from alby.utils import archive_data
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import json
import requests
from django.conf import settings
from alby.models import CadProduct
from alby.utils import dict_vlaue_int
from alby.serializers import ParametricProductSerializer, ParametricProductDxf
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from wsgiref.util import FileWrapper

from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED, HTTP_400_BAD_REQUEST


@require_http_methods(['POST', ])
@csrf_exempt
def get_cad_data(request, url='get-all'):
    req = dict_vlaue_int(json.loads(request.body))
    part_obj = get_object_or_404(CadProduct, id=req.pop('id'))
    # serialized_part_obj = ParametricProductDxf(part_obj)
    # serialized_data = serialized_part_obj.data
    res = requests.post(
        f'{settings.WEBCAD_URL}/{url}',
        json={
            'file': part_obj.part_file.name,
            'extra': req
        }).json()
    return JsonResponse(res)


@require_http_methods(['POST', ])
@csrf_exempt
def get_dxf(request):
    # print(json.loads(request.body))
    query = {k: v[0] for k, v in dict(request.POST).items()}
    req = dict_vlaue_int(query)
    part_obj = get_object_or_404(CadProduct, id=req.pop('id'))
    res = requests.post(
        f'{settings.WEBCAD_URL}/get-dxf',
        json={
            'file': part_obj.part_file.name,
            'extra': req
        }).json()
    if not res.get('dxf'):
        raise Exception('The Error occurred during dxf preparation')
    arch, arch_name = archive_data(res["dxf"])
    response = HttpResponse(FileWrapper(arch), content_type='application/zip')
    response["Content-Disposition"] = "attachment; filename=a"
    return response
