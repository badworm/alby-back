from django.urls import path
from connector.views import get_cad_data, get_dxf

app_name = 'connector'

urlpatterns = [
    path('cadfile-info/', get_cad_data, name='cad-info'),
    path('get-dxf/', get_dxf, name='cad-concrete-info'),
]
