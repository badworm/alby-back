import json
import os.path

from alby.tasks import run_nesting
from alby.models import Cart, CartItem, get_partid_file_by_prod

from django.core import serializers as dj_serializer
from django.utils.crypto import get_random_string
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db import models
from django.http.response import Http404, HttpResponseRedirect
from django.utils.cache import add_never_cache_headers
from django.utils.translation import get_language_from_request
from django.shortcuts import get_object_or_404

from dxfgen.utils import clean_extra_field
from dxfgen.controllers.models import get_component_by_dims

from nesting.serializers import NestRequestSerializer
from nesting.models import NestingResult, NestingDetail
from nesting.serializers import NestingDetailSerializer

from rest_framework.decorators import api_view
from rest_framework import generics
from rest_framework import serializers

from s3sync.backends.storage_backends import UsersPrivateStorage
from s3sync.backends.connect import s3_io_get_file

from shop.conf import app_settings
from shop.models.product import ProductModel
from shop.rest.money import JSONRenderer
from shop.rest.renderers import CMSPageRenderer




@csrf_exempt
@api_view(['POST'])
def part_nesting(request):
    nest_instance = NestingResult()
    serialized_material = NestRequestSerializer(data=request.data, many=True)
    if not serialized_material.is_valid():
        raise serializers.ValidationError('Some data are corrupt')
    cart_id = Cart.objects.get(customer_id=request.user.id)
    cart_items = CartItem.objects.filter(cart_id=cart_id, cart_type='parametric')
    nest_request = dj_serializer.serialize('json', cart_items, fields=('extra', 'quantity', 'product'))
    nest_request = json.loads(nest_request)
    for item in nest_request:
        cad_id = get_partid_file_by_prod(item['fields']['product'])
        prefs = clean_extra_field(item['fields']['extra']['settings'])
        dims = clean_extra_field(item['fields']['extra'])
        item['fields']['part_id'] = get_component_by_dims(cad_id, dims).id
        item['fields']['extra']['settings'] = prefs
    task_id = get_random_string(8)
    res = run_nesting(nest_request, request.user.id, serialized_material.data, task_id)
    if res:
        nest_instance.task_id = task_id
        nest_instance.user_id = request.user.id
        nest_instance.save()
        print(nest_instance.id)
        return HttpResponse(task_id)
    else:
        return HttpResponse(Http404)

@csrf_exempt
@api_view(['GET'])
def nesting_result(request, nest_id):
    return HttpResponse(nest_id)


class NestingDetailtView(generics.RetrieveAPIView):

    renderer_classes = (CMSPageRenderer, JSONRenderer)
    lookup_field = 'task_id'
    product_model = NestingResult
    serializer_class = NestingDetailSerializer
    queryset = NestingResult.objects.all()


@api_view(['GET'])
def download_nesting_dxf(request, nest_id, nest_detail_id):
    nesting_item = get_object_or_404(NestingDetail, id=nest_detail_id, nesting_id_id=nest_id)
    s3instance = UsersPrivateStorage()
    nest_file = s3_io_get_file(s3instance, nesting_item.files[0])
    response = HttpResponse(content_type='application/dxf')
    response['Content-Disposition'] = f'attachment; filename={nest_file["name"]}'
    response.write(nest_file['io'])
    return response

