from rest_framework import serializers
from .models import NestingDetail, NestingResult


class NestRequestSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=25)
    id = serializers.IntegerField(min_value=1, max_value=999999)
    txtr = serializers.BooleanField()
    width = serializers.IntegerField(min_value=1, max_value=5000)
    height = serializers.IntegerField(min_value=1, max_value=5000)
    thick = serializers.IntegerField(min_value=1, max_value=5000)
    prt_gap = serializers.IntegerField(min_value=0, max_value=1000)
    edge_gap = serializers.IntegerField(min_value=0, max_value=1000)
    quantity = serializers.IntegerField(min_value=1, max_value=99)


class NestingDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = NestingResult
        fields = '__all__'
