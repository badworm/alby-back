from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.urls import path
from .views import nesting_result
from menus.menu_pool import menu_pool
from cms.cms_menus import SoftRootCutter


@apphook_pool.register  # register the application
class NestingResultApp(CMSApp):

    app_name = "nesting-app"  # must match the application namespace
    name = "Nesting result app"

    def get_urls(self, page=None, language=None, **kwargs):
        return ['nesting.urls']
