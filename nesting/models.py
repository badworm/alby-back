from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms.models import model_to_dict


def get_nesting_detail_imgs(nest_id: int):
    nest_results = NestingDetail.objects.filter(nesting_id=nest_id)



def models_to_dict_val_as_list(objs):
    """
    convert form ORM result to dict with values as list
    :param objs: objs = SomeMode.objects.filter(id=1)
    :return: {'id': [12, 11, 10, 9, 8], 'nesting_id': [13, 13, 13, 13, 13], 'sheet_name': ...}
    """
    def _objs_model_to_list(objs):
        # return from models object- list on dicts
        lst = []
        for item in objs:
            lst.append(model_to_dict(item))
        return lst
    dicts = _objs_model_to_list(objs)
    result_dict = {}
    for item in dicts:
        for k, v in item.items():
            result_dict.setdefault(k, []).append(v)
    return result_dict


class NestingResult(models.Model):

    TASK_STATUS = (('Created', 'created'), ('Pending', 'pending'), ('Done', 'done'), ('Canceled', 'canceled'),
                   ('Failed', 'failed'))

    user_id = models.PositiveIntegerField(blank=True)
    task_id = models.CharField(max_length=50, blank=True, unique=True)
    status = models.CharField(max_length=10, blank=True, choices=TASK_STATUS, default='created')
    done = models.BooleanField(verbose_name=_('Does job success'), default=False)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    extra = models.JSONField(verbose_name=_('Extra task\'s data'), blank=True, null=True)

    def __str__(self):
        return f'Task id: {self.task_id}, by {self.user_id}'


class NestingDetail(models.Model):
    nesting_id = models.ForeignKey('NestingResult', on_delete=models.CASCADE, unique=False)
    sheet_name = models.CharField(verbose_name=_('Sheet name'), max_length=100)
    sheet_length = models.FloatField(verbose_name=_('Sheet length'))
    sheet_width = models.FloatField(verbose_name=_('Sheet width'))
    total_part_area = models.FloatField(verbose_name=_('Total parts area'))
    parts_in_nest = models.SmallIntegerField(verbose_name=_('Parts in nest'))
    nest_number = models.SmallIntegerField(verbose_name=_('Nest number'), help_text=_('Nest number in current nest'))
    nest_quantity = models.SmallIntegerField(verbose_name=_('Nest quantity'), help_text=_('Nest quantity'))
    sheet_utilisation = models.FloatField(verbose_name=_('Sheet utilisation'), help_text=_('Percent sheet utilisation'))
    nest_efficiency = models.FloatField(verbose_name=_('Nest efficiency'), help_text=_('Nest efficiency'), max_length=5)
    nest_length = models.FloatField(verbose_name=_('Nest length'), help_text=_('Nest length'), max_length=5)
    part_name = models.CharField(verbose_name=_('Part name'), max_length=40, blank=True)
    part_length = models.SmallIntegerField(verbose_name=_('Part length'), null=True)
    part_width = models.SmallIntegerField(verbose_name=_('Part width'), null=True)
    part_qty_in_nest = models.SmallIntegerField(verbose_name=_('Qty in nest'),
                                                help_text=_('Quantity in current nest'), null=True)
    part_prod_by_nest = models.SmallIntegerField(verbose_name=_('Prod by nest'),
                                                 help_text=_('Qty produced by current nest'), null=True)
    part_total_job = models.SmallIntegerField(verbose_name=_('Part qty in job'),
                                              help_text=_('Qty produced by job'), null=True)
    part_priority = models.SmallIntegerField(verbose_name=_('Parts priority'),
                                             help_text=_('Priority which part is the first'), null=True)
    files = models.JSONField(verbose_name=_('Nesting result files'), max_length=999)

    @property
    def sheet_area(self):
        return self.sheet_width*self.sheet_length

    @property
    def part_area(self):
        return self.part_width*self.part_length

    @property
    def sheet_util_percent(self):
        return f'{self.sheet_utilisation*100}%'

    @property
    def part_util_percent(self):
        return f'{self.part_util_percent*100}%'

    def __str__(self):
        return f'Nesting id: {self.nesting_id}'
