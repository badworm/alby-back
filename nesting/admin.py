from django.contrib import admin
from nesting.models import *


admin.site.register(NestingResult)

admin.site.register(NestingDetail)
