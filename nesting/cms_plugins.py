from alby.models.cart import Cart

from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.plugin_base import TransparentWrapper
from cmsplugin_cascade.plugin_base import CascadePluginBase

from django.core.exceptions import ObjectDoesNotExist
from django.template import TemplateDoesNotExist
from django.template.loader import select_template, get_template
from django.utils.translation import ugettext_lazy as _
from django.utils.html import mark_safe
from django.shortcuts import get_object_or_404
from django.forms.models import model_to_dict

from filer.models import File

from nesting.models import NestingDetail, NestingResult, models_to_dict_val_as_list

from shop.cascade.extensions import ShopExtendableMixin, LeftRightExtensionMixin


@plugin_pool.register_plugin  # register the plugin
class NestingReslutPlugin(LeftRightExtensionMixin, TransparentWrapper, CascadePluginBase):
    name = _("Nesting result")
    require_parent = True
    parent_classes = ['BootstrapColumnPlugin']
    cache = False
    allow_children = True
    model_mixins = (ShopExtendableMixin,)

    def get_render_template(self, context, instance, placeholder):
        render_template = instance.glossary.get('render_template')
        if render_template:
            return get_template(render_template)
        render_type = instance.glossary.get('render_type')
        try:
            return select_template([
                'nesting/{}.html'.format(render_type),
            ])
        except TemplateDoesNotExist:
            return get_template('nesting/result-table.html')

    def render(self, context, instance, placeholder):
        try:
            nest_instance = NestingDetail.objects.filter(nesting_id=context['data']['id'])
            context['results'] = [model_to_dict(i) for i in nest_instance]
        except (KeyError, ObjectDoesNotExist):
            context['nesting_ico'] = File.objects.filter(original_filename='nesting-ico.png').first()
            context['nest_list'] = NestingResult.objects.filter(user_id=context['user'].id)

        return self.super(NestingReslutPlugin, self).render(context, instance, placeholder)
