from django.urls import path
from .views import part_nesting, NestingDetailtView, download_nesting_dxf


urlpatterns = [
    path('start/', part_nesting, name='start_nesting'),
    path('<str:task_id>', NestingDetailtView.as_view(), name='nesting_result'),
    path('<int:nest_id>/<int:nest_detail_id>', download_nesting_dxf, name='download_nesting'),
]
