from django.apps import AppConfig


class NestingConfig(AppConfig):
    name = 'nesting'
