from django import template
from s3sync.backends.storage_backends import PrivateMediaStorage
register = template.Library()


@register.simple_tag(name='percent')
def percent(unit):
    return int(unit * 100)


@register.simple_tag(name='s3_url')
def s3_url(file_path):
    a = PrivateMediaStorage()
    a.location = a.private_media
    if a.exists(file_path):
        return a.url(file_path).split('?')[0]
