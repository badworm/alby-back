--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_emailaddress; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.account_emailaddress (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    verified boolean NOT NULL,
    "primary" boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.account_emailaddress OWNER TO serg;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.account_emailaddress_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailaddress_id_seq OWNER TO serg;

--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.account_emailaddress_id_seq OWNED BY public.account_emailaddress.id;


--
-- Name: account_emailconfirmation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.account_emailconfirmation (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    sent timestamp with time zone,
    key character varying(64) NOT NULL,
    email_address_id integer NOT NULL
);


ALTER TABLE public.account_emailconfirmation OWNER TO serg;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.account_emailconfirmation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_emailconfirmation_id_seq OWNER TO serg;

--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.account_emailconfirmation_id_seq OWNED BY public.account_emailconfirmation.id;


--
-- Name: alby_billingaddress; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_billingaddress (
    id integer NOT NULL,
    priority smallint NOT NULL,
    name character varying(1024) NOT NULL,
    address1 character varying(1024) NOT NULL,
    address2 character varying(1024),
    zip_code character varying(12) NOT NULL,
    city character varying(1024) NOT NULL,
    customer_id integer NOT NULL
);


ALTER TABLE public.alby_billingaddress OWNER TO serg;

--
-- Name: alby_billingaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_billingaddress_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_billingaddress_id_seq OWNER TO serg;

--
-- Name: alby_billingaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_billingaddress_id_seq OWNED BY public.alby_billingaddress.id;


--
-- Name: alby_cart; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_cart (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    extra jsonb NOT NULL,
    billing_address_id integer,
    customer_id integer NOT NULL,
    shipping_address_id integer
);


ALTER TABLE public.alby_cart OWNER TO serg;

--
-- Name: alby_cart_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_cart_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_cart_id_seq OWNER TO serg;

--
-- Name: alby_cart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_cart_id_seq OWNED BY public.alby_cart.id;


--
-- Name: alby_cartitem; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_cartitem (
    id integer NOT NULL,
    product_code character varying(255),
    updated_at timestamp with time zone NOT NULL,
    extra jsonb NOT NULL,
    quantity integer NOT NULL,
    cart_id integer NOT NULL,
    product_id integer NOT NULL,
    CONSTRAINT alby_cartitem_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.alby_cartitem OWNER TO serg;

--
-- Name: alby_cartitem_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_cartitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_cartitem_id_seq OWNER TO serg;

--
-- Name: alby_cartitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_cartitem_id_seq OWNED BY public.alby_cartitem.id;


--
-- Name: alby_commodity; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_commodity (
    product_ptr_id integer NOT NULL,
    unit_price numeric(30,3) NOT NULL,
    placeholder_id integer,
    product_code_id integer NOT NULL
);


ALTER TABLE public.alby_commodity OWNER TO serg;

--
-- Name: alby_commodity_translation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_commodity_translation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    description text NOT NULL,
    caption text NOT NULL,
    master_id integer
);


ALTER TABLE public.alby_commodity_translation OWNER TO serg;

--
-- Name: alby_commodity_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_commodity_translation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_commodity_translation_id_seq OWNER TO serg;

--
-- Name: alby_commodity_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_commodity_translation_id_seq OWNED BY public.alby_commodity_translation.id;


--
-- Name: alby_commodityinventory; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_commodityinventory (
    id integer NOT NULL,
    earliest timestamp with time zone NOT NULL,
    latest timestamp with time zone NOT NULL,
    quantity integer NOT NULL,
    product_id integer NOT NULL,
    CONSTRAINT alby_commodityinventory_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.alby_commodityinventory OWNER TO serg;

--
-- Name: alby_commodityinventory_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_commodityinventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_commodityinventory_id_seq OWNER TO serg;

--
-- Name: alby_commodityinventory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_commodityinventory_id_seq OWNED BY public.alby_commodityinventory.id;


--
-- Name: alby_customer; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_customer (
    user_id integer NOT NULL,
    recognized smallint NOT NULL,
    last_access timestamp with time zone NOT NULL,
    extra jsonb NOT NULL,
    phone character varying(150),
    number integer,
    CONSTRAINT alby_customer_number_check CHECK ((number >= 0)),
    CONSTRAINT alby_customer_recognized_check CHECK ((recognized >= 0))
);


ALTER TABLE public.alby_customer OWNER TO serg;

--
-- Name: alby_delivery; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_delivery (
    id integer NOT NULL,
    shipping_id character varying(255),
    fulfilled_at timestamp with time zone,
    shipped_at timestamp with time zone,
    shipping_method character varying(50) NOT NULL,
    order_id integer NOT NULL
);


ALTER TABLE public.alby_delivery OWNER TO serg;

--
-- Name: alby_delivery_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_delivery_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_delivery_id_seq OWNER TO serg;

--
-- Name: alby_delivery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_delivery_id_seq OWNED BY public.alby_delivery.id;


--
-- Name: alby_deliveryitem; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_deliveryitem (
    id integer NOT NULL,
    quantity integer NOT NULL,
    delivery_id integer NOT NULL,
    item_id integer NOT NULL
);


ALTER TABLE public.alby_deliveryitem OWNER TO serg;

--
-- Name: alby_deliveryitem_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_deliveryitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_deliveryitem_id_seq OWNER TO serg;

--
-- Name: alby_deliveryitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_deliveryitem_id_seq OWNED BY public.alby_deliveryitem.id;


--
-- Name: alby_discount; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_discount (
    id integer NOT NULL,
    discount_name character varying(100) NOT NULL,
    discont_scheme text NOT NULL
);


ALTER TABLE public.alby_discount OWNER TO serg;

--
-- Name: alby_discount_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_discount_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_discount_id_seq OWNER TO serg;

--
-- Name: alby_discount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_discount_id_seq OWNED BY public.alby_discount.id;


--
-- Name: alby_fabric; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_fabric (
    product_ptr_id integer NOT NULL,
    fabric_name character varying(150) NOT NULL,
    unit_price numeric(30,2) NOT NULL,
    fabric_type character varying(15) NOT NULL,
    composition character varying(255) NOT NULL,
    care character varying(255) NOT NULL,
    product_code_id integer NOT NULL
);


ALTER TABLE public.alby_fabric OWNER TO serg;

--
-- Name: alby_fabric_translation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_fabric_translation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    description text NOT NULL,
    caption text NOT NULL,
    master_id integer
);


ALTER TABLE public.alby_fabric_translation OWNER TO serg;

--
-- Name: alby_fabric_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_fabric_translation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_fabric_translation_id_seq OWNER TO serg;

--
-- Name: alby_fabric_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_fabric_translation_id_seq OWNED BY public.alby_fabric_translation.id;


--
-- Name: alby_lamel; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_lamel (
    product_ptr_id integer NOT NULL,
    unit_price numeric(30,3) NOT NULL,
    lamel_width character varying(6) NOT NULL,
    length character varying(25) NOT NULL,
    depth character varying(25) NOT NULL,
    weight character varying(6) NOT NULL,
    is_lamel boolean NOT NULL,
    weight_by_hand boolean NOT NULL,
    discont_scheme_id integer,
    product_code_id integer NOT NULL
);


ALTER TABLE public.alby_lamel OWNER TO serg;

--
-- Name: alby_lamel_translation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_lamel_translation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    description text NOT NULL,
    caption text NOT NULL,
    master_id integer
);


ALTER TABLE public.alby_lamel_translation OWNER TO serg;

--
-- Name: alby_lamel_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_lamel_translation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_lamel_translation_id_seq OWNER TO serg;

--
-- Name: alby_lamel_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_lamel_translation_id_seq OWNED BY public.alby_lamel_translation.id;


--
-- Name: alby_lamelinventory; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_lamelinventory (
    id integer NOT NULL,
    earliest timestamp with time zone NOT NULL,
    latest timestamp with time zone NOT NULL,
    quantity integer NOT NULL,
    product_id integer NOT NULL,
    CONSTRAINT alby_lamelinventory_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.alby_lamelinventory OWNER TO serg;

--
-- Name: alby_lamelinventory_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_lamelinventory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_lamelinventory_id_seq OWNER TO serg;

--
-- Name: alby_lamelinventory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_lamelinventory_id_seq OWNED BY public.alby_lamelinventory.id;


--
-- Name: alby_order; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_order (
    id integer NOT NULL,
    status character varying(50) NOT NULL,
    currency character varying(7) NOT NULL,
    _subtotal numeric(30,2) NOT NULL,
    _total numeric(30,2) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    extra jsonb NOT NULL,
    stored_request jsonb NOT NULL,
    number integer,
    shipping_address_text text,
    billing_address_text text,
    token character varying(40),
    customer_id integer NOT NULL,
    CONSTRAINT alby_order_number_check CHECK ((number >= 0))
);


ALTER TABLE public.alby_order OWNER TO serg;

--
-- Name: alby_order_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_order_id_seq OWNER TO serg;

--
-- Name: alby_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_order_id_seq OWNED BY public.alby_order.id;


--
-- Name: alby_orderitem; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_orderitem (
    id integer NOT NULL,
    product_name character varying(255),
    product_code character varying(255),
    _unit_price numeric(30,2),
    _line_total numeric(30,2),
    extra jsonb NOT NULL,
    quantity integer NOT NULL,
    canceled boolean NOT NULL,
    order_id integer NOT NULL,
    product_id integer,
    CONSTRAINT alby_orderitem_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.alby_orderitem OWNER TO serg;

--
-- Name: alby_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_orderitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_orderitem_id_seq OWNER TO serg;

--
-- Name: alby_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_orderitem_id_seq OWNED BY public.alby_orderitem.id;


--
-- Name: alby_orderpayment; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_orderpayment (
    id integer NOT NULL,
    amount numeric(30,2) NOT NULL,
    transaction_id character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    payment_method character varying(50) NOT NULL,
    order_id integer NOT NULL
);


ALTER TABLE public.alby_orderpayment OWNER TO serg;

--
-- Name: alby_orderpayment_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_orderpayment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_orderpayment_id_seq OWNER TO serg;

--
-- Name: alby_orderpayment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_orderpayment_id_seq OWNED BY public.alby_orderpayment.id;


--
-- Name: alby_product; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_product (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    active boolean NOT NULL,
    product_name character varying(255) NOT NULL,
    slug character varying(50) NOT NULL,
    "order" integer NOT NULL,
    polymorphic_ctype_id integer,
    product_title character varying(255) NOT NULL,
    CONSTRAINT alby_product_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.alby_product OWNER TO serg;

--
-- Name: alby_product_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_product_id_seq OWNER TO serg;

--
-- Name: alby_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_product_id_seq OWNED BY public.alby_product.id;


--
-- Name: alby_productimage; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_productimage (
    id integer NOT NULL,
    "order" smallint NOT NULL,
    image_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.alby_productimage OWNER TO serg;

--
-- Name: alby_productimage_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_productimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_productimage_id_seq OWNER TO serg;

--
-- Name: alby_productimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_productimage_id_seq OWNED BY public.alby_productimage.id;


--
-- Name: alby_productlist; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_productlist (
    id integer NOT NULL,
    product_code character varying(255) NOT NULL,
    product_model character varying(255) NOT NULL
);


ALTER TABLE public.alby_productlist OWNER TO serg;

--
-- Name: alby_productlist_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_productlist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_productlist_id_seq OWNER TO serg;

--
-- Name: alby_productlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_productlist_id_seq OWNED BY public.alby_productlist.id;


--
-- Name: alby_productpage; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_productpage (
    id integer NOT NULL,
    page_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.alby_productpage OWNER TO serg;

--
-- Name: alby_productpage_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_productpage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_productpage_id_seq OWNER TO serg;

--
-- Name: alby_productpage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_productpage_id_seq OWNED BY public.alby_productpage.id;


--
-- Name: alby_producttranslation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_producttranslation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    master_id integer
);


ALTER TABLE public.alby_producttranslation OWNER TO serg;

--
-- Name: alby_producttranslation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_producttranslation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_producttranslation_id_seq OWNER TO serg;

--
-- Name: alby_producttranslation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_producttranslation_id_seq OWNED BY public.alby_producttranslation.id;


--
-- Name: alby_shippingaddress; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_shippingaddress (
    id integer NOT NULL,
    priority smallint NOT NULL,
    name character varying(1024) NOT NULL,
    address1 character varying(1024) NOT NULL,
    address2 character varying(1024),
    zip_code character varying(12) NOT NULL,
    city character varying(1024) NOT NULL,
    customer_id integer NOT NULL
);


ALTER TABLE public.alby_shippingaddress OWNER TO serg;

--
-- Name: alby_shippingaddress_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_shippingaddress_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_shippingaddress_id_seq OWNER TO serg;

--
-- Name: alby_shippingaddress_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_shippingaddress_id_seq OWNED BY public.alby_shippingaddress.id;


--
-- Name: alby_sofamodel; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_sofamodel (
    product_ptr_id integer NOT NULL,
    sofa_type character varying(9) NOT NULL
);


ALTER TABLE public.alby_sofamodel OWNER TO serg;

--
-- Name: alby_sofamodel_translation; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_sofamodel_translation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    description text NOT NULL,
    caption text NOT NULL,
    master_id integer
);


ALTER TABLE public.alby_sofamodel_translation OWNER TO serg;

--
-- Name: alby_sofamodel_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_sofamodel_translation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_sofamodel_translation_id_seq OWNER TO serg;

--
-- Name: alby_sofamodel_translation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_sofamodel_translation_id_seq OWNED BY public.alby_sofamodel_translation.id;


--
-- Name: alby_sofavariant; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_sofavariant (
    id integer NOT NULL,
    unit_price numeric(30,2) NOT NULL,
    product_code_id integer NOT NULL,
    fabric_id integer NOT NULL,
    product_model_id integer NOT NULL
);


ALTER TABLE public.alby_sofavariant OWNER TO serg;

--
-- Name: alby_sofavariant_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_sofavariant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_sofavariant_id_seq OWNER TO serg;

--
-- Name: alby_sofavariant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_sofavariant_id_seq OWNED BY public.alby_sofavariant.id;


--
-- Name: alby_variantimage; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.alby_variantimage (
    id integer NOT NULL,
    "order" smallint NOT NULL,
    image_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.alby_variantimage OWNER TO serg;

--
-- Name: alby_variantimage_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.alby_variantimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alby_variantimage_id_seq OWNER TO serg;

--
-- Name: alby_variantimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.alby_variantimage_id_seq OWNED BY public.alby_variantimage.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO serg;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO serg;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO serg;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO serg;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO serg;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO serg;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    phone character varying(150),
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254),
    email_phone character varying(150),
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO serg;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO serg;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO serg;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO serg;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO serg;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO serg;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO serg;

--
-- Name: cms_aliaspluginmodel; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_aliaspluginmodel (
    cmsplugin_ptr_id integer NOT NULL,
    plugin_id integer,
    alias_placeholder_id integer
);


ALTER TABLE public.cms_aliaspluginmodel OWNER TO serg;

--
-- Name: cms_cmsplugin; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_cmsplugin (
    id integer NOT NULL,
    "position" smallint NOT NULL,
    language character varying(15) NOT NULL,
    plugin_type character varying(50) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    parent_id integer,
    placeholder_id integer,
    depth integer NOT NULL,
    numchild integer NOT NULL,
    path character varying(255) NOT NULL,
    CONSTRAINT cms_cmsplugin_depth_check CHECK ((depth >= 0)),
    CONSTRAINT cms_cmsplugin_numchild_check CHECK ((numchild >= 0)),
    CONSTRAINT cms_cmsplugin_position_check CHECK (("position" >= 0))
);


ALTER TABLE public.cms_cmsplugin OWNER TO serg;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_cmsplugin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_cmsplugin_id_seq OWNER TO serg;

--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_cmsplugin_id_seq OWNED BY public.cms_cmsplugin.id;


--
-- Name: cms_globalpagepermission; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_globalpagepermission (
    id integer NOT NULL,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    can_recover_page boolean NOT NULL,
    group_id integer,
    user_id integer
);


ALTER TABLE public.cms_globalpagepermission OWNER TO serg;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_globalpagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_globalpagepermission_id_seq OWNER TO serg;

--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_globalpagepermission_id_seq OWNED BY public.cms_globalpagepermission.id;


--
-- Name: cms_globalpagepermission_sites; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_globalpagepermission_sites (
    id integer NOT NULL,
    globalpagepermission_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.cms_globalpagepermission_sites OWNER TO serg;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_globalpagepermission_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_globalpagepermission_sites_id_seq OWNER TO serg;

--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_globalpagepermission_sites_id_seq OWNED BY public.cms_globalpagepermission_sites.id;


--
-- Name: cms_page; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_page (
    id integer NOT NULL,
    created_by character varying(255) NOT NULL,
    changed_by character varying(255) NOT NULL,
    creation_date timestamp with time zone NOT NULL,
    changed_date timestamp with time zone NOT NULL,
    publication_date timestamp with time zone,
    publication_end_date timestamp with time zone,
    in_navigation boolean NOT NULL,
    soft_root boolean NOT NULL,
    reverse_id character varying(40),
    navigation_extenders character varying(80),
    template character varying(100) NOT NULL,
    login_required boolean NOT NULL,
    limit_visibility_in_menu smallint,
    is_home boolean NOT NULL,
    application_urls character varying(200),
    application_namespace character varying(200),
    publisher_is_draft boolean NOT NULL,
    languages character varying(255),
    xframe_options integer NOT NULL,
    publisher_public_id integer,
    is_page_type boolean NOT NULL,
    node_id integer NOT NULL
);


ALTER TABLE public.cms_page OWNER TO serg;

--
-- Name: cms_page_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_page_id_seq OWNER TO serg;

--
-- Name: cms_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_page_id_seq OWNED BY public.cms_page.id;


--
-- Name: cms_page_placeholders; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_page_placeholders (
    id integer NOT NULL,
    page_id integer NOT NULL,
    placeholder_id integer NOT NULL
);


ALTER TABLE public.cms_page_placeholders OWNER TO serg;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_page_placeholders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_page_placeholders_id_seq OWNER TO serg;

--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_page_placeholders_id_seq OWNED BY public.cms_page_placeholders.id;


--
-- Name: cms_pagepermission; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_pagepermission (
    id integer NOT NULL,
    can_change boolean NOT NULL,
    can_add boolean NOT NULL,
    can_delete boolean NOT NULL,
    can_change_advanced_settings boolean NOT NULL,
    can_publish boolean NOT NULL,
    can_change_permissions boolean NOT NULL,
    can_move_page boolean NOT NULL,
    can_view boolean NOT NULL,
    grant_on integer NOT NULL,
    group_id integer,
    page_id integer,
    user_id integer
);


ALTER TABLE public.cms_pagepermission OWNER TO serg;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_pagepermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_pagepermission_id_seq OWNER TO serg;

--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_pagepermission_id_seq OWNED BY public.cms_pagepermission.id;


--
-- Name: cms_pageuser; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_pageuser (
    user_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE public.cms_pageuser OWNER TO serg;

--
-- Name: cms_pageusergroup; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_pageusergroup (
    group_ptr_id integer NOT NULL,
    created_by_id integer NOT NULL
);


ALTER TABLE public.cms_pageusergroup OWNER TO serg;

--
-- Name: cms_placeholder; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_placeholder (
    id integer NOT NULL,
    slot character varying(255) NOT NULL,
    default_width smallint,
    CONSTRAINT cms_placeholder_default_width_check CHECK ((default_width >= 0))
);


ALTER TABLE public.cms_placeholder OWNER TO serg;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_placeholder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_placeholder_id_seq OWNER TO serg;

--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_placeholder_id_seq OWNED BY public.cms_placeholder.id;


--
-- Name: cms_placeholderreference; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_placeholderreference (
    cmsplugin_ptr_id integer NOT NULL,
    name character varying(255) NOT NULL,
    placeholder_ref_id integer
);


ALTER TABLE public.cms_placeholderreference OWNER TO serg;

--
-- Name: cms_staticplaceholder; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_staticplaceholder (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    dirty boolean NOT NULL,
    creation_method character varying(20) NOT NULL,
    draft_id integer,
    public_id integer,
    site_id integer
);


ALTER TABLE public.cms_staticplaceholder OWNER TO serg;

--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_staticplaceholder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_staticplaceholder_id_seq OWNER TO serg;

--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_staticplaceholder_id_seq OWNED BY public.cms_staticplaceholder.id;


--
-- Name: cms_title; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_title (
    id integer NOT NULL,
    language character varying(15) NOT NULL,
    title character varying(255) NOT NULL,
    page_title character varying(255),
    menu_title character varying(255),
    meta_description text,
    slug character varying(255) NOT NULL,
    path character varying(255) NOT NULL,
    has_url_overwrite boolean NOT NULL,
    redirect character varying(2048),
    creation_date timestamp with time zone NOT NULL,
    published boolean NOT NULL,
    publisher_is_draft boolean NOT NULL,
    publisher_state smallint NOT NULL,
    page_id integer NOT NULL,
    publisher_public_id integer
);


ALTER TABLE public.cms_title OWNER TO serg;

--
-- Name: cms_title_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_title_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_title_id_seq OWNER TO serg;

--
-- Name: cms_title_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_title_id_seq OWNED BY public.cms_title.id;


--
-- Name: cms_treenode; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_treenode (
    id integer NOT NULL,
    path character varying(255) NOT NULL,
    depth integer NOT NULL,
    numchild integer NOT NULL,
    parent_id integer,
    site_id integer NOT NULL,
    CONSTRAINT cms_treenode_depth_check CHECK ((depth >= 0)),
    CONSTRAINT cms_treenode_numchild_check CHECK ((numchild >= 0))
);


ALTER TABLE public.cms_treenode OWNER TO serg;

--
-- Name: cms_treenode_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_treenode_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_treenode_id_seq OWNER TO serg;

--
-- Name: cms_treenode_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_treenode_id_seq OWNED BY public.cms_treenode.id;


--
-- Name: cms_urlconfrevision; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_urlconfrevision (
    id integer NOT NULL,
    revision character varying(255) NOT NULL
);


ALTER TABLE public.cms_urlconfrevision OWNER TO serg;

--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_urlconfrevision_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_urlconfrevision_id_seq OWNER TO serg;

--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_urlconfrevision_id_seq OWNED BY public.cms_urlconfrevision.id;


--
-- Name: cms_usersettings; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cms_usersettings (
    id integer NOT NULL,
    language character varying(10) NOT NULL,
    clipboard_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.cms_usersettings OWNER TO serg;

--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cms_usersettings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cms_usersettings_id_seq OWNER TO serg;

--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cms_usersettings_id_seq OWNED BY public.cms_usersettings.id;


--
-- Name: cmsplugin_cascade_cascadeclipboard; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_cascadeclipboard (
    id integer NOT NULL,
    identifier character varying(50) NOT NULL,
    data text
);


ALTER TABLE public.cmsplugin_cascade_cascadeclipboard OWNER TO serg;

--
-- Name: cmsplugin_cascade_cascadeclipboard_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_cascadeclipboard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_cascadeclipboard_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_cascadeclipboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_cascadeclipboard_id_seq OWNED BY public.cmsplugin_cascade_cascadeclipboard.id;


--
-- Name: cmsplugin_cascade_element; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_element (
    cmsplugin_ptr_id integer NOT NULL,
    glossary text NOT NULL,
    shared_glossary_id integer
);


ALTER TABLE public.cmsplugin_cascade_element OWNER TO serg;

--
-- Name: cmsplugin_cascade_iconfont; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_iconfont (
    id integer NOT NULL,
    identifier character varying(50) NOT NULL,
    config_data text NOT NULL,
    font_folder character varying(100) NOT NULL,
    zip_file_id integer NOT NULL,
    is_default boolean NOT NULL
);


ALTER TABLE public.cmsplugin_cascade_iconfont OWNER TO serg;

--
-- Name: cmsplugin_cascade_iconfont_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_iconfont_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_iconfont_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_iconfont_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_iconfont_id_seq OWNED BY public.cmsplugin_cascade_iconfont.id;


--
-- Name: cmsplugin_cascade_inline; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_inline (
    id integer NOT NULL,
    glossary text NOT NULL,
    cascade_element_id integer NOT NULL
);


ALTER TABLE public.cmsplugin_cascade_inline OWNER TO serg;

--
-- Name: cmsplugin_cascade_inline_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_inline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_inline_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_inline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_inline_id_seq OWNED BY public.cmsplugin_cascade_inline.id;


--
-- Name: cmsplugin_cascade_page; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_page (
    id integer NOT NULL,
    settings text NOT NULL,
    glossary text NOT NULL,
    extended_object_id integer NOT NULL,
    public_extension_id integer,
    icon_font_id integer,
    menu_symbol character varying(32)
);


ALTER TABLE public.cmsplugin_cascade_page OWNER TO serg;

--
-- Name: cmsplugin_cascade_page_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_page_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_page_id_seq OWNED BY public.cmsplugin_cascade_page.id;


--
-- Name: cmsplugin_cascade_pluginextrafields; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_pluginextrafields (
    id integer NOT NULL,
    plugin_type character varying(50) NOT NULL,
    allow_id_tag boolean NOT NULL,
    css_classes text,
    inline_styles text,
    site_id integer NOT NULL
);


ALTER TABLE public.cmsplugin_cascade_pluginextrafields OWNER TO serg;

--
-- Name: cmsplugin_cascade_pluginextrafields_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_pluginextrafields_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_pluginextrafields_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_pluginextrafields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_pluginextrafields_id_seq OWNED BY public.cmsplugin_cascade_pluginextrafields.id;


--
-- Name: cmsplugin_cascade_sharedglossary; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_sharedglossary (
    id integer NOT NULL,
    plugin_type character varying(50) NOT NULL,
    identifier character varying(50) NOT NULL,
    glossary text
);


ALTER TABLE public.cmsplugin_cascade_sharedglossary OWNER TO serg;

--
-- Name: cmsplugin_cascade_sharedglossary_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_sharedglossary_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_sharedglossary_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_sharedglossary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_sharedglossary_id_seq OWNED BY public.cmsplugin_cascade_sharedglossary.id;


--
-- Name: cmsplugin_cascade_sortinline; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_sortinline (
    id integer NOT NULL,
    glossary text NOT NULL,
    "order" integer NOT NULL,
    cascade_element_id integer NOT NULL,
    CONSTRAINT cmsplugin_cascade_sortinline_order_check CHECK (("order" >= 0))
);


ALTER TABLE public.cmsplugin_cascade_sortinline OWNER TO serg;

--
-- Name: cmsplugin_cascade_sortinline_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_sortinline_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_sortinline_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_sortinline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_sortinline_id_seq OWNED BY public.cmsplugin_cascade_sortinline.id;


--
-- Name: cmsplugin_cascade_texteditorconfigfields; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.cmsplugin_cascade_texteditorconfigfields (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    element_type character varying(12) NOT NULL,
    css_classes character varying(250) NOT NULL
);


ALTER TABLE public.cmsplugin_cascade_texteditorconfigfields OWNER TO serg;

--
-- Name: cmsplugin_cascade_texteditorconfigfields_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.cmsplugin_cascade_texteditorconfigfields_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmsplugin_cascade_texteditorconfigfields_id_seq OWNER TO serg;

--
-- Name: cmsplugin_cascade_texteditorconfigfields_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.cmsplugin_cascade_texteditorconfigfields_id_seq OWNED BY public.cmsplugin_cascade_texteditorconfigfields.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO serg;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO serg;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO serg;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO serg;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO serg;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO serg;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO serg;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO serg;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO serg;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: djangocms_text_ckeditor_text; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.djangocms_text_ckeditor_text (
    cmsplugin_ptr_id integer NOT NULL,
    body text NOT NULL
);


ALTER TABLE public.djangocms_text_ckeditor_text OWNER TO serg;

--
-- Name: easy_thumbnails_source; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.easy_thumbnails_source (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL
);


ALTER TABLE public.easy_thumbnails_source OWNER TO serg;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.easy_thumbnails_source_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_source_id_seq OWNER TO serg;

--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.easy_thumbnails_source_id_seq OWNED BY public.easy_thumbnails_source.id;


--
-- Name: easy_thumbnails_thumbnail; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.easy_thumbnails_thumbnail (
    id integer NOT NULL,
    storage_hash character varying(40) NOT NULL,
    name character varying(255) NOT NULL,
    modified timestamp with time zone NOT NULL,
    source_id integer NOT NULL
);


ALTER TABLE public.easy_thumbnails_thumbnail OWNER TO serg;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.easy_thumbnails_thumbnail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnail_id_seq OWNER TO serg;

--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.easy_thumbnails_thumbnail_id_seq OWNED BY public.easy_thumbnails_thumbnail.id;


--
-- Name: easy_thumbnails_thumbnaildimensions; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.easy_thumbnails_thumbnaildimensions (
    id integer NOT NULL,
    thumbnail_id integer NOT NULL,
    width integer,
    height integer,
    CONSTRAINT easy_thumbnails_thumbnaildimensions_height_check CHECK ((height >= 0)),
    CONSTRAINT easy_thumbnails_thumbnaildimensions_width_check CHECK ((width >= 0))
);


ALTER TABLE public.easy_thumbnails_thumbnaildimensions OWNER TO serg;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.easy_thumbnails_thumbnaildimensions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.easy_thumbnails_thumbnaildimensions_id_seq OWNER TO serg;

--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.easy_thumbnails_thumbnaildimensions_id_seq OWNED BY public.easy_thumbnails_thumbnaildimensions.id;


--
-- Name: filer_clipboard; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_clipboard (
    id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.filer_clipboard OWNER TO serg;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_clipboard_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_clipboard_id_seq OWNER TO serg;

--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_clipboard_id_seq OWNED BY public.filer_clipboard.id;


--
-- Name: filer_clipboarditem; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_clipboarditem (
    id integer NOT NULL,
    clipboard_id integer NOT NULL,
    file_id integer NOT NULL
);


ALTER TABLE public.filer_clipboarditem OWNER TO serg;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_clipboarditem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_clipboarditem_id_seq OWNER TO serg;

--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_clipboarditem_id_seq OWNED BY public.filer_clipboarditem.id;


--
-- Name: filer_file; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_file (
    id integer NOT NULL,
    file character varying(255),
    _file_size bigint,
    sha1 character varying(40) NOT NULL,
    has_all_mandatory_data boolean NOT NULL,
    original_filename character varying(255),
    name character varying(255) NOT NULL,
    description text,
    uploaded_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    is_public boolean NOT NULL,
    folder_id integer,
    owner_id integer,
    polymorphic_ctype_id integer
);


ALTER TABLE public.filer_file OWNER TO serg;

--
-- Name: filer_file_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_file_id_seq OWNER TO serg;

--
-- Name: filer_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_file_id_seq OWNED BY public.filer_file.id;


--
-- Name: filer_folder; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_folder (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    uploaded_at timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    owner_id integer,
    parent_id integer,
    CONSTRAINT filer_folder_level_check CHECK ((level >= 0)),
    CONSTRAINT filer_folder_lft_check CHECK ((lft >= 0)),
    CONSTRAINT filer_folder_rght_check CHECK ((rght >= 0)),
    CONSTRAINT filer_folder_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.filer_folder OWNER TO serg;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_folder_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_folder_id_seq OWNER TO serg;

--
-- Name: filer_folder_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_folder_id_seq OWNED BY public.filer_folder.id;


--
-- Name: filer_folderpermission; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_folderpermission (
    id integer NOT NULL,
    type smallint NOT NULL,
    everybody boolean NOT NULL,
    can_edit smallint,
    can_read smallint,
    can_add_children smallint,
    folder_id integer,
    group_id integer,
    user_id integer
);


ALTER TABLE public.filer_folderpermission OWNER TO serg;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_folderpermission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_folderpermission_id_seq OWNER TO serg;

--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_folderpermission_id_seq OWNED BY public.filer_folderpermission.id;


--
-- Name: filer_image; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_image (
    file_ptr_id integer NOT NULL,
    _height integer,
    _width integer,
    date_taken timestamp with time zone,
    default_alt_text character varying(255),
    default_caption character varying(255),
    author character varying(255),
    must_always_publish_author_credit boolean NOT NULL,
    must_always_publish_copyright boolean NOT NULL,
    subject_location character varying(64) NOT NULL
);


ALTER TABLE public.filer_image OWNER TO serg;

--
-- Name: filer_thumbnailoption; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.filer_thumbnailoption (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    crop boolean NOT NULL,
    upscale boolean NOT NULL
);


ALTER TABLE public.filer_thumbnailoption OWNER TO serg;

--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.filer_thumbnailoption_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filer_thumbnailoption_id_seq OWNER TO serg;

--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.filer_thumbnailoption_id_seq OWNED BY public.filer_thumbnailoption.id;


--
-- Name: menus_cachekey; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.menus_cachekey (
    id integer NOT NULL,
    language character varying(255) NOT NULL,
    site integer NOT NULL,
    key character varying(255) NOT NULL,
    CONSTRAINT menus_cachekey_site_check CHECK ((site >= 0))
);


ALTER TABLE public.menus_cachekey OWNER TO serg;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.menus_cachekey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menus_cachekey_id_seq OWNER TO serg;

--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.menus_cachekey_id_seq OWNED BY public.menus_cachekey.id;


--
-- Name: post_office_attachment; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.post_office_attachment (
    id integer NOT NULL,
    file character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    mimetype character varying(255) NOT NULL,
    headers text
);


ALTER TABLE public.post_office_attachment OWNER TO serg;

--
-- Name: post_office_attachment_emails; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.post_office_attachment_emails (
    id integer NOT NULL,
    attachment_id integer NOT NULL,
    email_id integer NOT NULL
);


ALTER TABLE public.post_office_attachment_emails OWNER TO serg;

--
-- Name: post_office_attachment_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.post_office_attachment_emails_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_office_attachment_emails_id_seq OWNER TO serg;

--
-- Name: post_office_attachment_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.post_office_attachment_emails_id_seq OWNED BY public.post_office_attachment_emails.id;


--
-- Name: post_office_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.post_office_attachment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_office_attachment_id_seq OWNER TO serg;

--
-- Name: post_office_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.post_office_attachment_id_seq OWNED BY public.post_office_attachment.id;


--
-- Name: post_office_email; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.post_office_email (
    id integer NOT NULL,
    from_email character varying(254) NOT NULL,
    "to" text NOT NULL,
    cc text NOT NULL,
    bcc text NOT NULL,
    subject character varying(989) NOT NULL,
    message text NOT NULL,
    html_message text NOT NULL,
    status smallint,
    priority smallint,
    created timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    scheduled_time timestamp with time zone,
    headers text,
    context text,
    template_id integer,
    backend_alias character varying(64) NOT NULL,
    CONSTRAINT post_office_email_priority_check CHECK ((priority >= 0)),
    CONSTRAINT post_office_email_status_check CHECK ((status >= 0))
);


ALTER TABLE public.post_office_email OWNER TO serg;

--
-- Name: post_office_email_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.post_office_email_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_office_email_id_seq OWNER TO serg;

--
-- Name: post_office_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.post_office_email_id_seq OWNED BY public.post_office_email.id;


--
-- Name: post_office_emailtemplate; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.post_office_emailtemplate (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    subject character varying(255) NOT NULL,
    content text NOT NULL,
    html_content text NOT NULL,
    created timestamp with time zone NOT NULL,
    last_updated timestamp with time zone NOT NULL,
    default_template_id integer,
    language character varying(12) NOT NULL
);


ALTER TABLE public.post_office_emailtemplate OWNER TO serg;

--
-- Name: post_office_emailtemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.post_office_emailtemplate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_office_emailtemplate_id_seq OWNER TO serg;

--
-- Name: post_office_emailtemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.post_office_emailtemplate_id_seq OWNED BY public.post_office_emailtemplate.id;


--
-- Name: post_office_log; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.post_office_log (
    id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    status smallint NOT NULL,
    exception_type character varying(255) NOT NULL,
    message text NOT NULL,
    email_id integer NOT NULL,
    CONSTRAINT post_office_log_status_check CHECK ((status >= 0))
);


ALTER TABLE public.post_office_log OWNER TO serg;

--
-- Name: post_office_log_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.post_office_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.post_office_log_id_seq OWNER TO serg;

--
-- Name: post_office_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.post_office_log_id_seq OWNED BY public.post_office_log.id;


--
-- Name: shop_notification; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.shop_notification (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    transition_target character varying(50) NOT NULL,
    recipient_id integer,
    mail_template_id integer NOT NULL,
    notify smallint NOT NULL,
    CONSTRAINT shop_notification_notify_check CHECK ((notify >= 0))
);


ALTER TABLE public.shop_notification OWNER TO serg;

--
-- Name: shop_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.shop_notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shop_notification_id_seq OWNER TO serg;

--
-- Name: shop_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.shop_notification_id_seq OWNED BY public.shop_notification.id;


--
-- Name: shop_notificationattachment; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.shop_notificationattachment (
    id integer NOT NULL,
    attachment_id integer,
    notification_id integer NOT NULL
);


ALTER TABLE public.shop_notificationattachment OWNER TO serg;

--
-- Name: shop_notificationattachment_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.shop_notificationattachment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shop_notificationattachment_id_seq OWNER TO serg;

--
-- Name: shop_notificationattachment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.shop_notificationattachment_id_seq OWNED BY public.shop_notificationattachment.id;


--
-- Name: socialaccount_socialaccount; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.socialaccount_socialaccount (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    uid character varying(191) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialaccount OWNER TO serg;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.socialaccount_socialaccount_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialaccount_id_seq OWNER TO serg;

--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.socialaccount_socialaccount_id_seq OWNED BY public.socialaccount_socialaccount.id;


--
-- Name: socialaccount_socialapp; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.socialaccount_socialapp (
    id integer NOT NULL,
    provider character varying(30) NOT NULL,
    name character varying(40) NOT NULL,
    client_id character varying(191) NOT NULL,
    secret character varying(191) NOT NULL,
    key character varying(191) NOT NULL
);


ALTER TABLE public.socialaccount_socialapp OWNER TO serg;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.socialaccount_socialapp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_id_seq OWNER TO serg;

--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.socialaccount_socialapp_id_seq OWNED BY public.socialaccount_socialapp.id;


--
-- Name: socialaccount_socialapp_sites; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.socialaccount_socialapp_sites (
    id integer NOT NULL,
    socialapp_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialapp_sites OWNER TO serg;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.socialaccount_socialapp_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialapp_sites_id_seq OWNER TO serg;

--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.socialaccount_socialapp_sites_id_seq OWNED BY public.socialaccount_socialapp_sites.id;


--
-- Name: socialaccount_socialtoken; Type: TABLE; Schema: public; Owner: serg
--

CREATE TABLE public.socialaccount_socialtoken (
    id integer NOT NULL,
    token text NOT NULL,
    token_secret text NOT NULL,
    expires_at timestamp with time zone,
    account_id integer NOT NULL,
    app_id integer NOT NULL
);


ALTER TABLE public.socialaccount_socialtoken OWNER TO serg;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: serg
--

CREATE SEQUENCE public.socialaccount_socialtoken_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.socialaccount_socialtoken_id_seq OWNER TO serg;

--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serg
--

ALTER SEQUENCE public.socialaccount_socialtoken_id_seq OWNED BY public.socialaccount_socialtoken.id;


--
-- Name: account_emailaddress id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailaddress ALTER COLUMN id SET DEFAULT nextval('public.account_emailaddress_id_seq'::regclass);


--
-- Name: account_emailconfirmation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailconfirmation ALTER COLUMN id SET DEFAULT nextval('public.account_emailconfirmation_id_seq'::regclass);


--
-- Name: alby_billingaddress id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_billingaddress ALTER COLUMN id SET DEFAULT nextval('public.alby_billingaddress_id_seq'::regclass);


--
-- Name: alby_cart id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart ALTER COLUMN id SET DEFAULT nextval('public.alby_cart_id_seq'::regclass);


--
-- Name: alby_cartitem id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cartitem ALTER COLUMN id SET DEFAULT nextval('public.alby_cartitem_id_seq'::regclass);


--
-- Name: alby_commodity_translation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity_translation ALTER COLUMN id SET DEFAULT nextval('public.alby_commodity_translation_id_seq'::regclass);


--
-- Name: alby_commodityinventory id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodityinventory ALTER COLUMN id SET DEFAULT nextval('public.alby_commodityinventory_id_seq'::regclass);


--
-- Name: alby_delivery id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_delivery ALTER COLUMN id SET DEFAULT nextval('public.alby_delivery_id_seq'::regclass);


--
-- Name: alby_deliveryitem id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_deliveryitem ALTER COLUMN id SET DEFAULT nextval('public.alby_deliveryitem_id_seq'::regclass);


--
-- Name: alby_discount id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_discount ALTER COLUMN id SET DEFAULT nextval('public.alby_discount_id_seq'::regclass);


--
-- Name: alby_fabric_translation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric_translation ALTER COLUMN id SET DEFAULT nextval('public.alby_fabric_translation_id_seq'::regclass);


--
-- Name: alby_lamel_translation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel_translation ALTER COLUMN id SET DEFAULT nextval('public.alby_lamel_translation_id_seq'::regclass);


--
-- Name: alby_lamelinventory id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamelinventory ALTER COLUMN id SET DEFAULT nextval('public.alby_lamelinventory_id_seq'::regclass);


--
-- Name: alby_order id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_order ALTER COLUMN id SET DEFAULT nextval('public.alby_order_id_seq'::regclass);


--
-- Name: alby_orderitem id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderitem ALTER COLUMN id SET DEFAULT nextval('public.alby_orderitem_id_seq'::regclass);


--
-- Name: alby_orderpayment id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderpayment ALTER COLUMN id SET DEFAULT nextval('public.alby_orderpayment_id_seq'::regclass);


--
-- Name: alby_product id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_product ALTER COLUMN id SET DEFAULT nextval('public.alby_product_id_seq'::regclass);


--
-- Name: alby_productimage id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productimage ALTER COLUMN id SET DEFAULT nextval('public.alby_productimage_id_seq'::regclass);


--
-- Name: alby_productlist id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productlist ALTER COLUMN id SET DEFAULT nextval('public.alby_productlist_id_seq'::regclass);


--
-- Name: alby_productpage id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productpage ALTER COLUMN id SET DEFAULT nextval('public.alby_productpage_id_seq'::regclass);


--
-- Name: alby_producttranslation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_producttranslation ALTER COLUMN id SET DEFAULT nextval('public.alby_producttranslation_id_seq'::regclass);


--
-- Name: alby_shippingaddress id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_shippingaddress ALTER COLUMN id SET DEFAULT nextval('public.alby_shippingaddress_id_seq'::regclass);


--
-- Name: alby_sofamodel_translation id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel_translation ALTER COLUMN id SET DEFAULT nextval('public.alby_sofamodel_translation_id_seq'::regclass);


--
-- Name: alby_sofavariant id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofavariant ALTER COLUMN id SET DEFAULT nextval('public.alby_sofavariant_id_seq'::regclass);


--
-- Name: alby_variantimage id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_variantimage ALTER COLUMN id SET DEFAULT nextval('public.alby_variantimage_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: cms_cmsplugin id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_cmsplugin ALTER COLUMN id SET DEFAULT nextval('public.cms_cmsplugin_id_seq'::regclass);


--
-- Name: cms_globalpagepermission id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission ALTER COLUMN id SET DEFAULT nextval('public.cms_globalpagepermission_id_seq'::regclass);


--
-- Name: cms_globalpagepermission_sites id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission_sites ALTER COLUMN id SET DEFAULT nextval('public.cms_globalpagepermission_sites_id_seq'::regclass);


--
-- Name: cms_page id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page ALTER COLUMN id SET DEFAULT nextval('public.cms_page_id_seq'::regclass);


--
-- Name: cms_page_placeholders id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page_placeholders ALTER COLUMN id SET DEFAULT nextval('public.cms_page_placeholders_id_seq'::regclass);


--
-- Name: cms_pagepermission id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pagepermission ALTER COLUMN id SET DEFAULT nextval('public.cms_pagepermission_id_seq'::regclass);


--
-- Name: cms_placeholder id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_placeholder ALTER COLUMN id SET DEFAULT nextval('public.cms_placeholder_id_seq'::regclass);


--
-- Name: cms_staticplaceholder id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder ALTER COLUMN id SET DEFAULT nextval('public.cms_staticplaceholder_id_seq'::regclass);


--
-- Name: cms_title id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title ALTER COLUMN id SET DEFAULT nextval('public.cms_title_id_seq'::regclass);


--
-- Name: cms_treenode id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_treenode ALTER COLUMN id SET DEFAULT nextval('public.cms_treenode_id_seq'::regclass);


--
-- Name: cms_urlconfrevision id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_urlconfrevision ALTER COLUMN id SET DEFAULT nextval('public.cms_urlconfrevision_id_seq'::regclass);


--
-- Name: cms_usersettings id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_usersettings ALTER COLUMN id SET DEFAULT nextval('public.cms_usersettings_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_cascadeclipboard id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_cascadeclipboard ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_cascadeclipboard_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_iconfont id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_iconfont ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_iconfont_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_inline id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_inline ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_inline_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_page id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_page_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_pluginextrafields id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_pluginextrafields ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_pluginextrafields_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_sharedglossary id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sharedglossary ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_sharedglossary_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_sortinline id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sortinline ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_sortinline_id_seq'::regclass);


--
-- Name: cmsplugin_cascade_texteditorconfigfields id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_texteditorconfigfields ALTER COLUMN id SET DEFAULT nextval('public.cmsplugin_cascade_texteditorconfigfields_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: easy_thumbnails_source id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_source ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_source_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnail id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_thumbnail_id_seq'::regclass);


--
-- Name: easy_thumbnails_thumbnaildimensions id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions ALTER COLUMN id SET DEFAULT nextval('public.easy_thumbnails_thumbnaildimensions_id_seq'::regclass);


--
-- Name: filer_clipboard id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboard ALTER COLUMN id SET DEFAULT nextval('public.filer_clipboard_id_seq'::regclass);


--
-- Name: filer_clipboarditem id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboarditem ALTER COLUMN id SET DEFAULT nextval('public.filer_clipboarditem_id_seq'::regclass);


--
-- Name: filer_file id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_file ALTER COLUMN id SET DEFAULT nextval('public.filer_file_id_seq'::regclass);


--
-- Name: filer_folder id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folder ALTER COLUMN id SET DEFAULT nextval('public.filer_folder_id_seq'::regclass);


--
-- Name: filer_folderpermission id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folderpermission ALTER COLUMN id SET DEFAULT nextval('public.filer_folderpermission_id_seq'::regclass);


--
-- Name: filer_thumbnailoption id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_thumbnailoption ALTER COLUMN id SET DEFAULT nextval('public.filer_thumbnailoption_id_seq'::regclass);


--
-- Name: menus_cachekey id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.menus_cachekey ALTER COLUMN id SET DEFAULT nextval('public.menus_cachekey_id_seq'::regclass);


--
-- Name: post_office_attachment id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment ALTER COLUMN id SET DEFAULT nextval('public.post_office_attachment_id_seq'::regclass);


--
-- Name: post_office_attachment_emails id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment_emails ALTER COLUMN id SET DEFAULT nextval('public.post_office_attachment_emails_id_seq'::regclass);


--
-- Name: post_office_email id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_email ALTER COLUMN id SET DEFAULT nextval('public.post_office_email_id_seq'::regclass);


--
-- Name: post_office_emailtemplate id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_emailtemplate ALTER COLUMN id SET DEFAULT nextval('public.post_office_emailtemplate_id_seq'::regclass);


--
-- Name: post_office_log id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_log ALTER COLUMN id SET DEFAULT nextval('public.post_office_log_id_seq'::regclass);


--
-- Name: shop_notification id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notification ALTER COLUMN id SET DEFAULT nextval('public.shop_notification_id_seq'::regclass);


--
-- Name: shop_notificationattachment id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notificationattachment ALTER COLUMN id SET DEFAULT nextval('public.shop_notificationattachment_id_seq'::regclass);


--
-- Name: socialaccount_socialaccount id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialaccount ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialaccount_id_seq'::regclass);


--
-- Name: socialaccount_socialapp id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_id_seq'::regclass);


--
-- Name: socialaccount_socialapp_sites id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialapp_sites_id_seq'::regclass);


--
-- Name: socialaccount_socialtoken id; Type: DEFAULT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialtoken ALTER COLUMN id SET DEFAULT nextval('public.socialaccount_socialtoken_id_seq'::regclass);


--
-- Data for Name: account_emailaddress; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.account_emailaddress (id, email, verified, "primary", user_id) FROM stdin;
\.


--
-- Data for Name: account_emailconfirmation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.account_emailconfirmation (id, created, sent, key, email_address_id) FROM stdin;
\.


--
-- Data for Name: alby_billingaddress; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_billingaddress (id, priority, name, address1, address2, zip_code, city, customer_id) FROM stdin;
\.


--
-- Data for Name: alby_cart; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_cart (id, created_at, updated_at, extra, billing_address_id, customer_id, shipping_address_id) FROM stdin;
\.


--
-- Data for Name: alby_cartitem; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_cartitem (id, product_code, updated_at, extra, quantity, cart_id, product_id) FROM stdin;
\.


--
-- Data for Name: alby_commodity; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_commodity (product_ptr_id, unit_price, placeholder_id, product_code_id) FROM stdin;
\.


--
-- Data for Name: alby_commodity_translation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_commodity_translation (id, language_code, description, caption, master_id) FROM stdin;
\.


--
-- Data for Name: alby_commodityinventory; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_commodityinventory (id, earliest, latest, quantity, product_id) FROM stdin;
\.


--
-- Data for Name: alby_customer; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_customer (user_id, recognized, last_access, extra, phone, number) FROM stdin;
\.


--
-- Data for Name: alby_delivery; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_delivery (id, shipping_id, fulfilled_at, shipped_at, shipping_method, order_id) FROM stdin;
\.


--
-- Data for Name: alby_deliveryitem; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_deliveryitem (id, quantity, delivery_id, item_id) FROM stdin;
\.


--
-- Data for Name: alby_discount; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_discount (id, discount_name, discont_scheme) FROM stdin;
\.


--
-- Data for Name: alby_fabric; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_fabric (product_ptr_id, fabric_name, unit_price, fabric_type, composition, care, product_code_id) FROM stdin;
\.


--
-- Data for Name: alby_fabric_translation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_fabric_translation (id, language_code, description, caption, master_id) FROM stdin;
\.


--
-- Data for Name: alby_lamel; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_lamel (product_ptr_id, unit_price, lamel_width, length, depth, weight, is_lamel, weight_by_hand, discont_scheme_id, product_code_id) FROM stdin;
\.


--
-- Data for Name: alby_lamel_translation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_lamel_translation (id, language_code, description, caption, master_id) FROM stdin;
\.


--
-- Data for Name: alby_lamelinventory; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_lamelinventory (id, earliest, latest, quantity, product_id) FROM stdin;
\.


--
-- Data for Name: alby_order; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_order (id, status, currency, _subtotal, _total, created_at, updated_at, extra, stored_request, number, shipping_address_text, billing_address_text, token, customer_id) FROM stdin;
\.


--
-- Data for Name: alby_orderitem; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_orderitem (id, product_name, product_code, _unit_price, _line_total, extra, quantity, canceled, order_id, product_id) FROM stdin;
\.


--
-- Data for Name: alby_orderpayment; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_orderpayment (id, amount, transaction_id, created_at, payment_method, order_id) FROM stdin;
\.


--
-- Data for Name: alby_product; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_product (id, created_at, updated_at, active, product_name, slug, "order", polymorphic_ctype_id, product_title) FROM stdin;
\.


--
-- Data for Name: alby_productimage; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_productimage (id, "order", image_id, product_id) FROM stdin;
\.


--
-- Data for Name: alby_productlist; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_productlist (id, product_code, product_model) FROM stdin;
\.


--
-- Data for Name: alby_productpage; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_productpage (id, page_id, product_id) FROM stdin;
\.


--
-- Data for Name: alby_producttranslation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_producttranslation (id, language_code, master_id) FROM stdin;
\.


--
-- Data for Name: alby_shippingaddress; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_shippingaddress (id, priority, name, address1, address2, zip_code, city, customer_id) FROM stdin;
\.


--
-- Data for Name: alby_sofamodel; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_sofamodel (product_ptr_id, sofa_type) FROM stdin;
\.


--
-- Data for Name: alby_sofamodel_translation; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_sofamodel_translation (id, language_code, description, caption, master_id) FROM stdin;
\.


--
-- Data for Name: alby_sofavariant; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_sofavariant (id, unit_price, product_code_id, fabric_id, product_model_id) FROM stdin;
\.


--
-- Data for Name: alby_variantimage; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.alby_variantimage (id, "order", image_id, product_id) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_user (id, password, last_login, is_superuser, username, phone, first_name, last_name, email, email_phone, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
\.


--
-- Data for Name: cms_aliaspluginmodel; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_aliaspluginmodel (cmsplugin_ptr_id, plugin_id, alias_placeholder_id) FROM stdin;
\.


--
-- Data for Name: cms_cmsplugin; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_cmsplugin (id, "position", language, plugin_type, creation_date, changed_date, parent_id, placeholder_id, depth, numchild, path) FROM stdin;
\.


--
-- Data for Name: cms_globalpagepermission; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_globalpagepermission (id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, can_recover_page, group_id, user_id) FROM stdin;
\.


--
-- Data for Name: cms_globalpagepermission_sites; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_globalpagepermission_sites (id, globalpagepermission_id, site_id) FROM stdin;
\.


--
-- Data for Name: cms_page; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_page (id, created_by, changed_by, creation_date, changed_date, publication_date, publication_end_date, in_navigation, soft_root, reverse_id, navigation_extenders, template, login_required, limit_visibility_in_menu, is_home, application_urls, application_namespace, publisher_is_draft, languages, xframe_options, publisher_public_id, is_page_type, node_id) FROM stdin;
\.


--
-- Data for Name: cms_page_placeholders; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_page_placeholders (id, page_id, placeholder_id) FROM stdin;
\.


--
-- Data for Name: cms_pagepermission; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_pagepermission (id, can_change, can_add, can_delete, can_change_advanced_settings, can_publish, can_change_permissions, can_move_page, can_view, grant_on, group_id, page_id, user_id) FROM stdin;
\.


--
-- Data for Name: cms_pageuser; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_pageuser (user_ptr_id, created_by_id) FROM stdin;
\.


--
-- Data for Name: cms_pageusergroup; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_pageusergroup (group_ptr_id, created_by_id) FROM stdin;
\.


--
-- Data for Name: cms_placeholder; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_placeholder (id, slot, default_width) FROM stdin;
\.


--
-- Data for Name: cms_placeholderreference; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_placeholderreference (cmsplugin_ptr_id, name, placeholder_ref_id) FROM stdin;
\.


--
-- Data for Name: cms_staticplaceholder; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_staticplaceholder (id, name, code, dirty, creation_method, draft_id, public_id, site_id) FROM stdin;
\.


--
-- Data for Name: cms_title; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_title (id, language, title, page_title, menu_title, meta_description, slug, path, has_url_overwrite, redirect, creation_date, published, publisher_is_draft, publisher_state, page_id, publisher_public_id) FROM stdin;
\.


--
-- Data for Name: cms_treenode; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_treenode (id, path, depth, numchild, parent_id, site_id) FROM stdin;
\.


--
-- Data for Name: cms_urlconfrevision; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_urlconfrevision (id, revision) FROM stdin;
\.


--
-- Data for Name: cms_usersettings; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cms_usersettings (id, language, clipboard_id, user_id) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_cascadeclipboard; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_cascadeclipboard (id, identifier, data) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_element; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_element (cmsplugin_ptr_id, glossary, shared_glossary_id) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_iconfont; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_iconfont (id, identifier, config_data, font_folder, zip_file_id, is_default) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_inline; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_inline (id, glossary, cascade_element_id) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_page; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_page (id, settings, glossary, extended_object_id, public_extension_id, icon_font_id, menu_symbol) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_pluginextrafields; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_pluginextrafields (id, plugin_type, allow_id_tag, css_classes, inline_styles, site_id) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_sharedglossary; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_sharedglossary (id, plugin_type, identifier, glossary) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_sortinline; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_sortinline (id, glossary, "order", cascade_element_id) FROM stdin;
\.


--
-- Data for Name: cmsplugin_cascade_texteditorconfigfields; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.cmsplugin_cascade_texteditorconfigfields (id, name, element_type, css_classes) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.django_site (id, domain, name) FROM stdin;
\.


--
-- Data for Name: djangocms_text_ckeditor_text; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.djangocms_text_ckeditor_text (cmsplugin_ptr_id, body) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_source; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.easy_thumbnails_source (id, storage_hash, name, modified) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_thumbnail; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.easy_thumbnails_thumbnail (id, storage_hash, name, modified, source_id) FROM stdin;
\.


--
-- Data for Name: easy_thumbnails_thumbnaildimensions; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.easy_thumbnails_thumbnaildimensions (id, thumbnail_id, width, height) FROM stdin;
\.


--
-- Data for Name: filer_clipboard; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_clipboard (id, user_id) FROM stdin;
\.


--
-- Data for Name: filer_clipboarditem; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_clipboarditem (id, clipboard_id, file_id) FROM stdin;
\.


--
-- Data for Name: filer_file; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_file (id, file, _file_size, sha1, has_all_mandatory_data, original_filename, name, description, uploaded_at, modified_at, is_public, folder_id, owner_id, polymorphic_ctype_id) FROM stdin;
\.


--
-- Data for Name: filer_folder; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_folder (id, name, uploaded_at, created_at, modified_at, lft, rght, tree_id, level, owner_id, parent_id) FROM stdin;
\.


--
-- Data for Name: filer_folderpermission; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_folderpermission (id, type, everybody, can_edit, can_read, can_add_children, folder_id, group_id, user_id) FROM stdin;
\.


--
-- Data for Name: filer_image; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_image (file_ptr_id, _height, _width, date_taken, default_alt_text, default_caption, author, must_always_publish_author_credit, must_always_publish_copyright, subject_location) FROM stdin;
\.


--
-- Data for Name: filer_thumbnailoption; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.filer_thumbnailoption (id, name, width, height, crop, upscale) FROM stdin;
\.


--
-- Data for Name: menus_cachekey; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.menus_cachekey (id, language, site, key) FROM stdin;
\.


--
-- Data for Name: post_office_attachment; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.post_office_attachment (id, file, name, mimetype, headers) FROM stdin;
\.


--
-- Data for Name: post_office_attachment_emails; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.post_office_attachment_emails (id, attachment_id, email_id) FROM stdin;
\.


--
-- Data for Name: post_office_email; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.post_office_email (id, from_email, "to", cc, bcc, subject, message, html_message, status, priority, created, last_updated, scheduled_time, headers, context, template_id, backend_alias) FROM stdin;
\.


--
-- Data for Name: post_office_emailtemplate; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.post_office_emailtemplate (id, name, description, subject, content, html_content, created, last_updated, default_template_id, language) FROM stdin;
\.


--
-- Data for Name: post_office_log; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.post_office_log (id, date, status, exception_type, message, email_id) FROM stdin;
\.


--
-- Data for Name: shop_notification; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.shop_notification (id, name, transition_target, recipient_id, mail_template_id, notify) FROM stdin;
\.


--
-- Data for Name: shop_notificationattachment; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.shop_notificationattachment (id, attachment_id, notification_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
\.


--
-- Data for Name: socialaccount_socialtoken; Type: TABLE DATA; Schema: public; Owner: serg
--

COPY public.socialaccount_socialtoken (id, token, token_secret, expires_at, account_id, app_id) FROM stdin;
\.


--
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 1, false);


--
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- Name: alby_billingaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_billingaddress_id_seq', 1, false);


--
-- Name: alby_cart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_cart_id_seq', 24, true);


--
-- Name: alby_cartitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_cartitem_id_seq', 142, true);


--
-- Name: alby_commodity_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_commodity_translation_id_seq', 1, false);


--
-- Name: alby_commodityinventory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_commodityinventory_id_seq', 1, false);


--
-- Name: alby_delivery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_delivery_id_seq', 1, false);


--
-- Name: alby_deliveryitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_deliveryitem_id_seq', 1, false);


--
-- Name: alby_discount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_discount_id_seq', 1, true);


--
-- Name: alby_fabric_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_fabric_translation_id_seq', 5, true);


--
-- Name: alby_lamel_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_lamel_translation_id_seq', 12, true);


--
-- Name: alby_lamelinventory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_lamelinventory_id_seq', 12, true);


--
-- Name: alby_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_order_id_seq', 1, false);


--
-- Name: alby_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_orderitem_id_seq', 1, false);


--
-- Name: alby_orderpayment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_orderpayment_id_seq', 1, false);


--
-- Name: alby_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_product_id_seq', 19, true);


--
-- Name: alby_productimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_productimage_id_seq', 33, true);


--
-- Name: alby_productlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_productlist_id_seq', 97, true);


--
-- Name: alby_productpage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_productpage_id_seq', 32, true);


--
-- Name: alby_producttranslation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_producttranslation_id_seq', 19, true);


--
-- Name: alby_shippingaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_shippingaddress_id_seq', 4, true);


--
-- Name: alby_sofamodel_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_sofamodel_translation_id_seq', 2, true);


--
-- Name: alby_sofavariant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_sofavariant_id_seq', 18, true);


--
-- Name: alby_variantimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.alby_variantimage_id_seq', 25, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 584, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 24, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: cms_cmsplugin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_cmsplugin_id_seq', 2464, true);


--
-- Name: cms_globalpagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_globalpagepermission_id_seq', 1, false);


--
-- Name: cms_globalpagepermission_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_globalpagepermission_sites_id_seq', 1, false);


--
-- Name: cms_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_page_id_seq', 81, true);


--
-- Name: cms_page_placeholders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_page_placeholders_id_seq', 160, true);


--
-- Name: cms_pagepermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_pagepermission_id_seq', 1, false);


--
-- Name: cms_placeholder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_placeholder_id_seq', 173, true);


--
-- Name: cms_staticplaceholder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_staticplaceholder_id_seq', 2, true);


--
-- Name: cms_title_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_title_id_seq', 81, true);


--
-- Name: cms_treenode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_treenode_id_seq', 42, true);


--
-- Name: cms_urlconfrevision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_urlconfrevision_id_seq', 1, false);


--
-- Name: cms_usersettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cms_usersettings_id_seq', 1, true);


--
-- Name: cmsplugin_cascade_cascadeclipboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_cascadeclipboard_id_seq', 46, true);


--
-- Name: cmsplugin_cascade_iconfont_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_iconfont_id_seq', 1, true);


--
-- Name: cmsplugin_cascade_inline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_inline_id_seq', 40, true);


--
-- Name: cmsplugin_cascade_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_page_id_seq', 13, true);


--
-- Name: cmsplugin_cascade_pluginextrafields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_pluginextrafields_id_seq', 1, true);


--
-- Name: cmsplugin_cascade_sharedglossary_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_sharedglossary_id_seq', 1, false);


--
-- Name: cmsplugin_cascade_sortinline_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_sortinline_id_seq', 100, true);


--
-- Name: cmsplugin_cascade_texteditorconfigfields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.cmsplugin_cascade_texteditorconfigfields_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 553, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 151, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 181, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Name: easy_thumbnails_source_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.easy_thumbnails_source_id_seq', 99, true);


--
-- Name: easy_thumbnails_thumbnail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.easy_thumbnails_thumbnail_id_seq', 676, true);


--
-- Name: easy_thumbnails_thumbnaildimensions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.easy_thumbnails_thumbnaildimensions_id_seq', 1, false);


--
-- Name: filer_clipboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_clipboard_id_seq', 1, true);


--
-- Name: filer_clipboarditem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_clipboarditem_id_seq', 1, false);


--
-- Name: filer_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_file_id_seq', 68, true);


--
-- Name: filer_folder_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_folder_id_seq', 11, true);


--
-- Name: filer_folderpermission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_folderpermission_id_seq', 1, false);


--
-- Name: filer_thumbnailoption_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.filer_thumbnailoption_id_seq', 1, false);


--
-- Name: menus_cachekey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.menus_cachekey_id_seq', 286, true);


--
-- Name: post_office_attachment_emails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.post_office_attachment_emails_id_seq', 1, false);


--
-- Name: post_office_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.post_office_attachment_id_seq', 1, false);


--
-- Name: post_office_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.post_office_email_id_seq', 1, false);


--
-- Name: post_office_emailtemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.post_office_emailtemplate_id_seq', 2, true);


--
-- Name: post_office_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.post_office_log_id_seq', 1, false);


--
-- Name: shop_notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.shop_notification_id_seq', 2, true);


--
-- Name: shop_notificationattachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.shop_notificationattachment_id_seq', 1, false);


--
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 1, false);


--
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 1, false);


--
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 1, false);


--
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serg
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 1, false);


--
-- Name: account_emailaddress account_emailaddress_email_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_email_key UNIQUE (email);


--
-- Name: account_emailaddress account_emailaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_pkey PRIMARY KEY (id);


--
-- Name: account_emailconfirmation account_emailconfirmation_key_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_key_key UNIQUE (key);


--
-- Name: account_emailconfirmation account_emailconfirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirmation_pkey PRIMARY KEY (id);


--
-- Name: alby_billingaddress alby_billingaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_billingaddress
    ADD CONSTRAINT alby_billingaddress_pkey PRIMARY KEY (id);


--
-- Name: alby_cart alby_cart_customer_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart
    ADD CONSTRAINT alby_cart_customer_id_key UNIQUE (customer_id);


--
-- Name: alby_cart alby_cart_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart
    ADD CONSTRAINT alby_cart_pkey PRIMARY KEY (id);


--
-- Name: alby_cartitem alby_cartitem_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cartitem
    ADD CONSTRAINT alby_cartitem_pkey PRIMARY KEY (id);


--
-- Name: alby_commodity alby_commodity_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity
    ADD CONSTRAINT alby_commodity_pkey PRIMARY KEY (product_ptr_id);


--
-- Name: alby_commodity_translation alby_commodity_translati_language_code_master_id_9d3b7309_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity_translation
    ADD CONSTRAINT alby_commodity_translati_language_code_master_id_9d3b7309_uniq UNIQUE (language_code, master_id);


--
-- Name: alby_commodity_translation alby_commodity_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity_translation
    ADD CONSTRAINT alby_commodity_translation_pkey PRIMARY KEY (id);


--
-- Name: alby_commodityinventory alby_commodityinventory_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodityinventory
    ADD CONSTRAINT alby_commodityinventory_pkey PRIMARY KEY (id);


--
-- Name: alby_customer alby_customer_number_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_customer
    ADD CONSTRAINT alby_customer_number_key UNIQUE (number);


--
-- Name: alby_customer alby_customer_phone_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_customer
    ADD CONSTRAINT alby_customer_phone_key UNIQUE (phone);


--
-- Name: alby_customer alby_customer_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_customer
    ADD CONSTRAINT alby_customer_pkey PRIMARY KEY (user_id);


--
-- Name: alby_delivery alby_delivery_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_delivery
    ADD CONSTRAINT alby_delivery_pkey PRIMARY KEY (id);


--
-- Name: alby_delivery alby_delivery_shipping_method_shipping_id_227edbc8_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_delivery
    ADD CONSTRAINT alby_delivery_shipping_method_shipping_id_227edbc8_uniq UNIQUE (shipping_method, shipping_id);


--
-- Name: alby_deliveryitem alby_deliveryitem_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_deliveryitem
    ADD CONSTRAINT alby_deliveryitem_pkey PRIMARY KEY (id);


--
-- Name: alby_discount alby_discount_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_discount
    ADD CONSTRAINT alby_discount_pkey PRIMARY KEY (id);


--
-- Name: alby_fabric alby_fabric_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric
    ADD CONSTRAINT alby_fabric_pkey PRIMARY KEY (product_ptr_id);


--
-- Name: alby_fabric_translation alby_fabric_translation_language_code_master_id_10807fbb_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric_translation
    ADD CONSTRAINT alby_fabric_translation_language_code_master_id_10807fbb_uniq UNIQUE (language_code, master_id);


--
-- Name: alby_fabric_translation alby_fabric_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric_translation
    ADD CONSTRAINT alby_fabric_translation_pkey PRIMARY KEY (id);


--
-- Name: alby_lamel alby_lamel_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel
    ADD CONSTRAINT alby_lamel_pkey PRIMARY KEY (product_ptr_id);


--
-- Name: alby_lamel_translation alby_lamel_translation_language_code_master_id_74c33402_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel_translation
    ADD CONSTRAINT alby_lamel_translation_language_code_master_id_74c33402_uniq UNIQUE (language_code, master_id);


--
-- Name: alby_lamel_translation alby_lamel_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel_translation
    ADD CONSTRAINT alby_lamel_translation_pkey PRIMARY KEY (id);


--
-- Name: alby_lamelinventory alby_lamelinventory_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamelinventory
    ADD CONSTRAINT alby_lamelinventory_pkey PRIMARY KEY (id);


--
-- Name: alby_order alby_order_number_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_order
    ADD CONSTRAINT alby_order_number_key UNIQUE (number);


--
-- Name: alby_order alby_order_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_order
    ADD CONSTRAINT alby_order_pkey PRIMARY KEY (id);


--
-- Name: alby_orderitem alby_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderitem
    ADD CONSTRAINT alby_orderitem_pkey PRIMARY KEY (id);


--
-- Name: alby_orderpayment alby_orderpayment_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderpayment
    ADD CONSTRAINT alby_orderpayment_pkey PRIMARY KEY (id);


--
-- Name: alby_product alby_product_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_product
    ADD CONSTRAINT alby_product_pkey PRIMARY KEY (id);


--
-- Name: alby_product alby_product_slug_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_product
    ADD CONSTRAINT alby_product_slug_key UNIQUE (slug);


--
-- Name: alby_productimage alby_productimage_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productimage
    ADD CONSTRAINT alby_productimage_pkey PRIMARY KEY (id);


--
-- Name: alby_productlist alby_productlist_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productlist
    ADD CONSTRAINT alby_productlist_pkey PRIMARY KEY (id);


--
-- Name: alby_productlist alby_productlist_product_code_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productlist
    ADD CONSTRAINT alby_productlist_product_code_key UNIQUE (product_code);


--
-- Name: alby_productpage alby_productpage_page_id_product_id_eab4c507_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productpage
    ADD CONSTRAINT alby_productpage_page_id_product_id_eab4c507_uniq UNIQUE (page_id, product_id);


--
-- Name: alby_productpage alby_productpage_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productpage
    ADD CONSTRAINT alby_productpage_pkey PRIMARY KEY (id);


--
-- Name: alby_producttranslation alby_producttranslation_language_code_master_id_89ac8963_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_producttranslation
    ADD CONSTRAINT alby_producttranslation_language_code_master_id_89ac8963_uniq UNIQUE (language_code, master_id);


--
-- Name: alby_producttranslation alby_producttranslation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_producttranslation
    ADD CONSTRAINT alby_producttranslation_pkey PRIMARY KEY (id);


--
-- Name: alby_shippingaddress alby_shippingaddress_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_shippingaddress
    ADD CONSTRAINT alby_shippingaddress_pkey PRIMARY KEY (id);


--
-- Name: alby_sofamodel alby_sofamodel_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel
    ADD CONSTRAINT alby_sofamodel_pkey PRIMARY KEY (product_ptr_id);


--
-- Name: alby_sofamodel_translation alby_sofamodel_translati_language_code_master_id_bb47e41f_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel_translation
    ADD CONSTRAINT alby_sofamodel_translati_language_code_master_id_bb47e41f_uniq UNIQUE (language_code, master_id);


--
-- Name: alby_sofamodel_translation alby_sofamodel_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel_translation
    ADD CONSTRAINT alby_sofamodel_translation_pkey PRIMARY KEY (id);


--
-- Name: alby_sofavariant alby_sofavariant_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofavariant
    ADD CONSTRAINT alby_sofavariant_pkey PRIMARY KEY (id);


--
-- Name: alby_variantimage alby_variantimage_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_variantimage
    ADD CONSTRAINT alby_variantimage_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_email_1c89df09_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_1c89df09_uniq UNIQUE (email);


--
-- Name: auth_user auth_user_email_phone_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_phone_key UNIQUE (email_phone);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_phone_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_phone_key UNIQUE (phone);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cms_cmsplugin cms_cmsplugin_path_4917bb44_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_path_4917bb44_uniq UNIQUE (path);


--
-- Name: cms_cmsplugin cms_cmsplugin_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermission_globalpagepermission_id__db684f41_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermission_globalpagepermission_id__db684f41_uniq UNIQUE (globalpagepermission_id, site_id);


--
-- Name: cms_globalpagepermission cms_globalpagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermission_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermission_sites_pkey PRIMARY KEY (id);


--
-- Name: cms_page cms_page_node_id_publisher_is_draft_c1293d6a_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page
    ADD CONSTRAINT cms_page_node_id_publisher_is_draft_c1293d6a_uniq UNIQUE (node_id, publisher_is_draft);


--
-- Name: cms_page cms_page_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page
    ADD CONSTRAINT cms_page_pkey PRIMARY KEY (id);


--
-- Name: cms_page_placeholders cms_page_placeholders_page_id_placeholder_id_ab7fbfb8_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_page_id_placeholder_id_ab7fbfb8_uniq UNIQUE (page_id, placeholder_id);


--
-- Name: cms_page_placeholders cms_page_placeholders_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_pkey PRIMARY KEY (id);


--
-- Name: cms_page cms_page_publisher_public_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page
    ADD CONSTRAINT cms_page_publisher_public_id_key UNIQUE (publisher_public_id);


--
-- Name: cms_pagepermission cms_pagepermission_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_pkey PRIMARY KEY (id);


--
-- Name: cms_pageuser cms_pageuser_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageuser
    ADD CONSTRAINT cms_pageuser_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: cms_pageusergroup cms_pageusergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_pkey PRIMARY KEY (group_ptr_id);


--
-- Name: cms_placeholder cms_placeholder_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_placeholder
    ADD CONSTRAINT cms_placeholder_pkey PRIMARY KEY (id);


--
-- Name: cms_placeholderreference cms_placeholderreference_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_placeholderreference
    ADD CONSTRAINT cms_placeholderreference_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cms_staticplaceholder cms_staticplaceholder_code_site_id_21ba079c_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_code_site_id_21ba079c_uniq UNIQUE (code, site_id);


--
-- Name: cms_staticplaceholder cms_staticplaceholder_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_pkey PRIMARY KEY (id);


--
-- Name: cms_title cms_title_language_page_id_61aaf084_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title
    ADD CONSTRAINT cms_title_language_page_id_61aaf084_uniq UNIQUE (language, page_id);


--
-- Name: cms_title cms_title_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title
    ADD CONSTRAINT cms_title_pkey PRIMARY KEY (id);


--
-- Name: cms_title cms_title_publisher_public_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title
    ADD CONSTRAINT cms_title_publisher_public_id_key UNIQUE (publisher_public_id);


--
-- Name: cms_treenode cms_treenode_path_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_treenode
    ADD CONSTRAINT cms_treenode_path_key UNIQUE (path);


--
-- Name: cms_treenode cms_treenode_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_treenode
    ADD CONSTRAINT cms_treenode_pkey PRIMARY KEY (id);


--
-- Name: cms_urlconfrevision cms_urlconfrevision_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_urlconfrevision
    ADD CONSTRAINT cms_urlconfrevision_pkey PRIMARY KEY (id);


--
-- Name: cms_usersettings cms_usersettings_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_usersettings
    ADD CONSTRAINT cms_usersettings_pkey PRIMARY KEY (id);


--
-- Name: cms_usersettings cms_usersettings_user_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_usersettings
    ADD CONSTRAINT cms_usersettings_user_id_key UNIQUE (user_id);


--
-- Name: cmsplugin_cascade_cascadeclipboard cmsplugin_cascade_cascadeclipboard_identifier_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_cascadeclipboard
    ADD CONSTRAINT cmsplugin_cascade_cascadeclipboard_identifier_key UNIQUE (identifier);


--
-- Name: cmsplugin_cascade_cascadeclipboard cmsplugin_cascade_cascadeclipboard_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_cascadeclipboard
    ADD CONSTRAINT cmsplugin_cascade_cascadeclipboard_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_element cmsplugin_cascade_element_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_element
    ADD CONSTRAINT cmsplugin_cascade_element_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: cmsplugin_cascade_iconfont cmsplugin_cascade_iconfont_identifier_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_iconfont
    ADD CONSTRAINT cmsplugin_cascade_iconfont_identifier_key UNIQUE (identifier);


--
-- Name: cmsplugin_cascade_iconfont cmsplugin_cascade_iconfont_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_iconfont
    ADD CONSTRAINT cmsplugin_cascade_iconfont_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_inline cmsplugin_cascade_inline_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_inline
    ADD CONSTRAINT cmsplugin_cascade_inline_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_page_extended_object_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_page_extended_object_id_key UNIQUE (extended_object_id);


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_page_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_page_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_page_public_extension_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_page_public_extension_id_key UNIQUE (public_extension_id);


--
-- Name: cmsplugin_cascade_pluginextrafields cmsplugin_cascade_plugin_plugin_type_site_id_b5146694_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_pluginextrafields
    ADD CONSTRAINT cmsplugin_cascade_plugin_plugin_type_site_id_b5146694_uniq UNIQUE (plugin_type, site_id);


--
-- Name: cmsplugin_cascade_pluginextrafields cmsplugin_cascade_pluginextrafields_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_pluginextrafields
    ADD CONSTRAINT cmsplugin_cascade_pluginextrafields_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_sharedglossary cmsplugin_cascade_shared_plugin_type_identifier_997f0236_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sharedglossary
    ADD CONSTRAINT cmsplugin_cascade_shared_plugin_type_identifier_997f0236_uniq UNIQUE (plugin_type, identifier);


--
-- Name: cmsplugin_cascade_sharedglossary cmsplugin_cascade_sharedglossary_identifier_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sharedglossary
    ADD CONSTRAINT cmsplugin_cascade_sharedglossary_identifier_key UNIQUE (identifier);


--
-- Name: cmsplugin_cascade_sharedglossary cmsplugin_cascade_sharedglossary_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sharedglossary
    ADD CONSTRAINT cmsplugin_cascade_sharedglossary_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_sortinline cmsplugin_cascade_sortinline_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sortinline
    ADD CONSTRAINT cmsplugin_cascade_sortinline_pkey PRIMARY KEY (id);


--
-- Name: cmsplugin_cascade_texteditorconfigfields cmsplugin_cascade_texteditorconfigfields_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_texteditorconfigfields
    ADD CONSTRAINT cmsplugin_cascade_texteditorconfigfields_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: djangocms_text_ckeditor_text djangocms_text_ckeditor_text_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.djangocms_text_ckeditor_text
    ADD CONSTRAINT djangocms_text_ckeditor_text_pkey PRIMARY KEY (cmsplugin_ptr_id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_source easy_thumbnails_source_storage_hash_name_481ce32d_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_source
    ADD CONSTRAINT easy_thumbnails_source_storage_hash_name_481ce32d_uniq UNIQUE (storage_hash, name);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnai_storage_hash_name_source_fb375270_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnai_storage_hash_name_source_fb375270_uniq UNIQUE (storage_hash, name, source_id);


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thumbnail_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thumbnail_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_pkey PRIMARY KEY (id);


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thumbnaildimensions_thumbnail_id_key; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thumbnaildimensions_thumbnail_id_key UNIQUE (thumbnail_id);


--
-- Name: filer_clipboard filer_clipboard_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboard
    ADD CONSTRAINT filer_clipboard_pkey PRIMARY KEY (id);


--
-- Name: filer_clipboarditem filer_clipboarditem_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_pkey PRIMARY KEY (id);


--
-- Name: filer_file filer_file_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_file
    ADD CONSTRAINT filer_file_pkey PRIMARY KEY (id);


--
-- Name: filer_folder filer_folder_parent_id_name_bc773258_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folder
    ADD CONSTRAINT filer_folder_parent_id_name_bc773258_uniq UNIQUE (parent_id, name);


--
-- Name: filer_folder filer_folder_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folder
    ADD CONSTRAINT filer_folder_pkey PRIMARY KEY (id);


--
-- Name: filer_folderpermission filer_folderpermission_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_pkey PRIMARY KEY (id);


--
-- Name: filer_image filer_image_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_image
    ADD CONSTRAINT filer_image_pkey PRIMARY KEY (file_ptr_id);


--
-- Name: filer_thumbnailoption filer_thumbnailoption_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_thumbnailoption
    ADD CONSTRAINT filer_thumbnailoption_pkey PRIMARY KEY (id);


--
-- Name: menus_cachekey menus_cachekey_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.menus_cachekey
    ADD CONSTRAINT menus_cachekey_pkey PRIMARY KEY (id);


--
-- Name: post_office_attachment_emails post_office_attachment_e_attachment_id_email_id_8e046917_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment_emails
    ADD CONSTRAINT post_office_attachment_e_attachment_id_email_id_8e046917_uniq UNIQUE (attachment_id, email_id);


--
-- Name: post_office_attachment_emails post_office_attachment_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment_emails
    ADD CONSTRAINT post_office_attachment_emails_pkey PRIMARY KEY (id);


--
-- Name: post_office_attachment post_office_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment
    ADD CONSTRAINT post_office_attachment_pkey PRIMARY KEY (id);


--
-- Name: post_office_email post_office_email_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_email
    ADD CONSTRAINT post_office_email_pkey PRIMARY KEY (id);


--
-- Name: post_office_emailtemplate post_office_emailtemplat_name_language_default_te_4023e3e4_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_emailtemplate
    ADD CONSTRAINT post_office_emailtemplat_name_language_default_te_4023e3e4_uniq UNIQUE (name, language, default_template_id);


--
-- Name: post_office_emailtemplate post_office_emailtemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_emailtemplate
    ADD CONSTRAINT post_office_emailtemplate_pkey PRIMARY KEY (id);


--
-- Name: post_office_log post_office_log_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_log
    ADD CONSTRAINT post_office_log_pkey PRIMARY KEY (id);


--
-- Name: shop_notification shop_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notification
    ADD CONSTRAINT shop_notification_pkey PRIMARY KEY (id);


--
-- Name: shop_notificationattachment shop_notificationattachment_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notificationattachment
    ADD CONSTRAINT shop_notificationattachment_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_provider_uid_fc810c6e_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_provider_uid_fc810c6e_uniq UNIQUE (provider, uid);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp__socialapp_id_site_id_71a9a768_uniq UNIQUE (socialapp_id, site_id);


--
-- Name: socialaccount_socialapp socialaccount_socialapp_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp
    ADD CONSTRAINT socialaccount_socialapp_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialapp_sites socialaccount_socialapp_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_socialapp_sites_pkey PRIMARY KEY (id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq UNIQUE (app_id, account_id);


--
-- Name: socialaccount_socialtoken socialaccount_socialtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_socialtoken_pkey PRIMARY KEY (id);


--
-- Name: account_emailaddress_email_03be32b2_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX account_emailaddress_email_03be32b2_like ON public.account_emailaddress USING btree (email varchar_pattern_ops);


--
-- Name: account_emailaddress_user_id_2c513194; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX account_emailaddress_user_id_2c513194 ON public.account_emailaddress USING btree (user_id);


--
-- Name: account_emailconfirmation_email_address_id_5b7f8c58; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX account_emailconfirmation_email_address_id_5b7f8c58 ON public.account_emailconfirmation USING btree (email_address_id);


--
-- Name: account_emailconfirmation_key_f43612bd_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX account_emailconfirmation_key_f43612bd_like ON public.account_emailconfirmation USING btree (key varchar_pattern_ops);


--
-- Name: alby_billingaddress_customer_id_cf0174f1; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_billingaddress_customer_id_cf0174f1 ON public.alby_billingaddress USING btree (customer_id);


--
-- Name: alby_billingaddress_priority_49af0bc0; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_billingaddress_priority_49af0bc0 ON public.alby_billingaddress USING btree (priority);


--
-- Name: alby_cart_billing_address_id_73dec424; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_cart_billing_address_id_73dec424 ON public.alby_cart USING btree (billing_address_id);


--
-- Name: alby_cart_shipping_address_id_7ed543be; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_cart_shipping_address_id_7ed543be ON public.alby_cart USING btree (shipping_address_id);


--
-- Name: alby_cartitem_cart_id_7c441df8; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_cartitem_cart_id_7c441df8 ON public.alby_cartitem USING btree (cart_id);


--
-- Name: alby_cartitem_product_id_fb0ffe80; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_cartitem_product_id_fb0ffe80 ON public.alby_cartitem USING btree (product_id);


--
-- Name: alby_commodity_placeholder_id_eab39765; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodity_placeholder_id_eab39765 ON public.alby_commodity USING btree (placeholder_id);


--
-- Name: alby_commodity_product_code_id_262e5fc0; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodity_product_code_id_262e5fc0 ON public.alby_commodity USING btree (product_code_id);


--
-- Name: alby_commodity_translation_language_code_5e7e82de; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodity_translation_language_code_5e7e82de ON public.alby_commodity_translation USING btree (language_code);


--
-- Name: alby_commodity_translation_language_code_5e7e82de_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodity_translation_language_code_5e7e82de_like ON public.alby_commodity_translation USING btree (language_code varchar_pattern_ops);


--
-- Name: alby_commodity_translation_master_id_db0f61db; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodity_translation_master_id_db0f61db ON public.alby_commodity_translation USING btree (master_id);


--
-- Name: alby_commodityinventory_earliest_eb9ade39; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodityinventory_earliest_eb9ade39 ON public.alby_commodityinventory USING btree (earliest);


--
-- Name: alby_commodityinventory_latest_d76457ca; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodityinventory_latest_d76457ca ON public.alby_commodityinventory USING btree (latest);


--
-- Name: alby_commodityinventory_product_id_3435e842; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_commodityinventory_product_id_3435e842 ON public.alby_commodityinventory USING btree (product_id);


--
-- Name: alby_customer_phone_7603b872_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_customer_phone_7603b872_like ON public.alby_customer USING btree (phone varchar_pattern_ops);


--
-- Name: alby_delivery_order_id_413f1920; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_delivery_order_id_413f1920 ON public.alby_delivery USING btree (order_id);


--
-- Name: alby_deliveryitem_delivery_id_edb43ddc; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_deliveryitem_delivery_id_edb43ddc ON public.alby_deliveryitem USING btree (delivery_id);


--
-- Name: alby_deliveryitem_item_id_9f06ce8c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_deliveryitem_item_id_9f06ce8c ON public.alby_deliveryitem USING btree (item_id);


--
-- Name: alby_fabric_product_code_id_d6518426; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_fabric_product_code_id_d6518426 ON public.alby_fabric USING btree (product_code_id);


--
-- Name: alby_fabric_translation_language_code_9038115d; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_fabric_translation_language_code_9038115d ON public.alby_fabric_translation USING btree (language_code);


--
-- Name: alby_fabric_translation_language_code_9038115d_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_fabric_translation_language_code_9038115d_like ON public.alby_fabric_translation USING btree (language_code varchar_pattern_ops);


--
-- Name: alby_fabric_translation_master_id_7a6c7571; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_fabric_translation_master_id_7a6c7571 ON public.alby_fabric_translation USING btree (master_id);


--
-- Name: alby_lamel_discont_scheme_id_ed2f6283; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamel_discont_scheme_id_ed2f6283 ON public.alby_lamel USING btree (discont_scheme_id);


--
-- Name: alby_lamel_product_code_id_0c837519; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamel_product_code_id_0c837519 ON public.alby_lamel USING btree (product_code_id);


--
-- Name: alby_lamel_translation_language_code_244cea09; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamel_translation_language_code_244cea09 ON public.alby_lamel_translation USING btree (language_code);


--
-- Name: alby_lamel_translation_language_code_244cea09_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamel_translation_language_code_244cea09_like ON public.alby_lamel_translation USING btree (language_code varchar_pattern_ops);


--
-- Name: alby_lamel_translation_master_id_041fc106; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamel_translation_master_id_041fc106 ON public.alby_lamel_translation USING btree (master_id);


--
-- Name: alby_lamelinventory_earliest_e89a0932; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamelinventory_earliest_e89a0932 ON public.alby_lamelinventory USING btree (earliest);


--
-- Name: alby_lamelinventory_latest_5c81f51e; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamelinventory_latest_5c81f51e ON public.alby_lamelinventory USING btree (latest);


--
-- Name: alby_lamelinventory_product_id_98f39136; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_lamelinventory_product_id_98f39136 ON public.alby_lamelinventory USING btree (product_id);


--
-- Name: alby_order_customer_id_eb896ea6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_order_customer_id_eb896ea6 ON public.alby_order USING btree (customer_id);


--
-- Name: alby_orderitem_order_id_146a310d; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_orderitem_order_id_146a310d ON public.alby_orderitem USING btree (order_id);


--
-- Name: alby_orderitem_product_id_4d948465; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_orderitem_product_id_4d948465 ON public.alby_orderitem USING btree (product_id);


--
-- Name: alby_orderpayment_order_id_169b1020; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_orderpayment_order_id_169b1020 ON public.alby_orderpayment USING btree (order_id);


--
-- Name: alby_product_order_ebf8328d; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_product_order_ebf8328d ON public.alby_product USING btree ("order");


--
-- Name: alby_product_polymorphic_ctype_id_1aea35eb; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_product_polymorphic_ctype_id_1aea35eb ON public.alby_product USING btree (polymorphic_ctype_id);


--
-- Name: alby_product_slug_6ca1f226_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_product_slug_6ca1f226_like ON public.alby_product USING btree (slug varchar_pattern_ops);


--
-- Name: alby_productimage_image_id_0419f545; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_productimage_image_id_0419f545 ON public.alby_productimage USING btree (image_id);


--
-- Name: alby_productimage_product_id_d73c92c5; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_productimage_product_id_d73c92c5 ON public.alby_productimage USING btree (product_id);


--
-- Name: alby_productlist_product_code_c3c2d151_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_productlist_product_code_c3c2d151_like ON public.alby_productlist USING btree (product_code varchar_pattern_ops);


--
-- Name: alby_productpage_page_id_958bf826; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_productpage_page_id_958bf826 ON public.alby_productpage USING btree (page_id);


--
-- Name: alby_productpage_product_id_e2eb2b4a; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_productpage_product_id_e2eb2b4a ON public.alby_productpage USING btree (product_id);


--
-- Name: alby_producttranslation_language_code_aeb8da16; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_producttranslation_language_code_aeb8da16 ON public.alby_producttranslation USING btree (language_code);


--
-- Name: alby_producttranslation_language_code_aeb8da16_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_producttranslation_language_code_aeb8da16_like ON public.alby_producttranslation USING btree (language_code varchar_pattern_ops);


--
-- Name: alby_producttranslation_master_id_9aa75e48; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_producttranslation_master_id_9aa75e48 ON public.alby_producttranslation USING btree (master_id);


--
-- Name: alby_shippingaddress_customer_id_01de57c2; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_shippingaddress_customer_id_01de57c2 ON public.alby_shippingaddress USING btree (customer_id);


--
-- Name: alby_shippingaddress_priority_2f512af0; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_shippingaddress_priority_2f512af0 ON public.alby_shippingaddress USING btree (priority);


--
-- Name: alby_sofamodel_translation_language_code_aa6804d8; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofamodel_translation_language_code_aa6804d8 ON public.alby_sofamodel_translation USING btree (language_code);


--
-- Name: alby_sofamodel_translation_language_code_aa6804d8_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofamodel_translation_language_code_aa6804d8_like ON public.alby_sofamodel_translation USING btree (language_code varchar_pattern_ops);


--
-- Name: alby_sofamodel_translation_master_id_3de2a345; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofamodel_translation_master_id_3de2a345 ON public.alby_sofamodel_translation USING btree (master_id);


--
-- Name: alby_sofavariant_fabric_id_eea026e4; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofavariant_fabric_id_eea026e4 ON public.alby_sofavariant USING btree (fabric_id);


--
-- Name: alby_sofavariant_product_code_id_3ebfa900; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofavariant_product_code_id_3ebfa900 ON public.alby_sofavariant USING btree (product_code_id);


--
-- Name: alby_sofavariant_product_model_id_68bca617; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_sofavariant_product_model_id_68bca617 ON public.alby_sofavariant USING btree (product_model_id);


--
-- Name: alby_variantimage_image_id_8bf62496; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_variantimage_image_id_8bf62496 ON public.alby_variantimage USING btree (image_id);


--
-- Name: alby_variantimage_product_id_7f470d32; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX alby_variantimage_product_id_7f470d32 ON public.alby_variantimage USING btree (product_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_email_1c89df09_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_email_1c89df09_like ON public.auth_user USING btree (email varchar_pattern_ops);


--
-- Name: auth_user_email_phone_0ba40178_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_email_phone_0ba40178_like ON public.auth_user USING btree (email_phone varchar_pattern_ops);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_phone_552f73e3_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_phone_552f73e3_like ON public.auth_user USING btree (phone varchar_pattern_ops);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a ON public.cms_aliaspluginmodel USING btree (alias_placeholder_id);


--
-- Name: cms_aliaspluginmodel_plugin_id_9867676e; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_aliaspluginmodel_plugin_id_9867676e ON public.cms_aliaspluginmodel USING btree (plugin_id);


--
-- Name: cms_cmsplugin_language_bbea8a48; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_language_bbea8a48 ON public.cms_cmsplugin USING btree (language);


--
-- Name: cms_cmsplugin_language_bbea8a48_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_language_bbea8a48_like ON public.cms_cmsplugin USING btree (language varchar_pattern_ops);


--
-- Name: cms_cmsplugin_parent_id_fd3bd9dd; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_parent_id_fd3bd9dd ON public.cms_cmsplugin USING btree (parent_id);


--
-- Name: cms_cmsplugin_path_4917bb44_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_path_4917bb44_like ON public.cms_cmsplugin USING btree (path varchar_pattern_ops);


--
-- Name: cms_cmsplugin_placeholder_id_0bfa3b26; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_placeholder_id_0bfa3b26 ON public.cms_cmsplugin USING btree (placeholder_id);


--
-- Name: cms_cmsplugin_plugin_type_94e96ebf; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_plugin_type_94e96ebf ON public.cms_cmsplugin USING btree (plugin_type);


--
-- Name: cms_cmsplugin_plugin_type_94e96ebf_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_cmsplugin_plugin_type_94e96ebf_like ON public.cms_cmsplugin USING btree (plugin_type varchar_pattern_ops);


--
-- Name: cms_globalpagepermission_group_id_991b4733; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_globalpagepermission_group_id_991b4733 ON public.cms_globalpagepermission USING btree (group_id);


--
-- Name: cms_globalpagepermission_sites_globalpagepermission_id_46bd2681; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_globalpagepermission_sites_globalpagepermission_id_46bd2681 ON public.cms_globalpagepermission_sites USING btree (globalpagepermission_id);


--
-- Name: cms_globalpagepermission_sites_site_id_00460b53; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_globalpagepermission_sites_site_id_00460b53 ON public.cms_globalpagepermission_sites USING btree (site_id);


--
-- Name: cms_globalpagepermission_user_id_a227cee1; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_globalpagepermission_user_id_a227cee1 ON public.cms_globalpagepermission USING btree (user_id);


--
-- Name: cms_page_application_urls_9ef47497; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_application_urls_9ef47497 ON public.cms_page USING btree (application_urls);


--
-- Name: cms_page_application_urls_9ef47497_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_application_urls_9ef47497_like ON public.cms_page USING btree (application_urls varchar_pattern_ops);


--
-- Name: cms_page_in_navigation_01c24279; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_in_navigation_01c24279 ON public.cms_page USING btree (in_navigation);


--
-- Name: cms_page_is_home_edadca07; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_is_home_edadca07 ON public.cms_page USING btree (is_home);


--
-- Name: cms_page_limit_visibility_in_menu_30db6aa6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_limit_visibility_in_menu_30db6aa6 ON public.cms_page USING btree (limit_visibility_in_menu);


--
-- Name: cms_page_navigation_extenders_c24af8dd; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_navigation_extenders_c24af8dd ON public.cms_page USING btree (navigation_extenders);


--
-- Name: cms_page_navigation_extenders_c24af8dd_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_navigation_extenders_c24af8dd_like ON public.cms_page USING btree (navigation_extenders varchar_pattern_ops);


--
-- Name: cms_page_node_id_c87b85a9; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_node_id_c87b85a9 ON public.cms_page USING btree (node_id);


--
-- Name: cms_page_placeholders_page_id_f2ce8dec; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_placeholders_page_id_f2ce8dec ON public.cms_page_placeholders USING btree (page_id);


--
-- Name: cms_page_placeholders_placeholder_id_6b120886; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_placeholders_placeholder_id_6b120886 ON public.cms_page_placeholders USING btree (placeholder_id);


--
-- Name: cms_page_publication_date_684fabf7; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_publication_date_684fabf7 ON public.cms_page USING btree (publication_date);


--
-- Name: cms_page_publication_end_date_12a0c46a; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_publication_end_date_12a0c46a ON public.cms_page USING btree (publication_end_date);


--
-- Name: cms_page_publisher_is_draft_141cba60; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_publisher_is_draft_141cba60 ON public.cms_page USING btree (publisher_is_draft);


--
-- Name: cms_page_reverse_id_ffc9ede2; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_reverse_id_ffc9ede2 ON public.cms_page USING btree (reverse_id);


--
-- Name: cms_page_reverse_id_ffc9ede2_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_reverse_id_ffc9ede2_like ON public.cms_page USING btree (reverse_id varchar_pattern_ops);


--
-- Name: cms_page_soft_root_51efccbe; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_page_soft_root_51efccbe ON public.cms_page USING btree (soft_root);


--
-- Name: cms_pagepermission_group_id_af5af193; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_pagepermission_group_id_af5af193 ON public.cms_pagepermission USING btree (group_id);


--
-- Name: cms_pagepermission_page_id_0ae9a163; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_pagepermission_page_id_0ae9a163 ON public.cms_pagepermission USING btree (page_id);


--
-- Name: cms_pagepermission_user_id_0c7d2b3c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_pagepermission_user_id_0c7d2b3c ON public.cms_pagepermission USING btree (user_id);


--
-- Name: cms_pageuser_created_by_id_8e9fbf83; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_pageuser_created_by_id_8e9fbf83 ON public.cms_pageuser USING btree (created_by_id);


--
-- Name: cms_pageusergroup_created_by_id_7d57fa39; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_pageusergroup_created_by_id_7d57fa39 ON public.cms_pageusergroup USING btree (created_by_id);


--
-- Name: cms_placeholder_slot_0bc04d21; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_placeholder_slot_0bc04d21 ON public.cms_placeholder USING btree (slot);


--
-- Name: cms_placeholder_slot_0bc04d21_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_placeholder_slot_0bc04d21_like ON public.cms_placeholder USING btree (slot varchar_pattern_ops);


--
-- Name: cms_placeholderreference_placeholder_ref_id_244759b1; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_placeholderreference_placeholder_ref_id_244759b1 ON public.cms_placeholderreference USING btree (placeholder_ref_id);


--
-- Name: cms_staticplaceholder_draft_id_5aee407b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_staticplaceholder_draft_id_5aee407b ON public.cms_staticplaceholder USING btree (draft_id);


--
-- Name: cms_staticplaceholder_public_id_876aaa66; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_staticplaceholder_public_id_876aaa66 ON public.cms_staticplaceholder USING btree (public_id);


--
-- Name: cms_staticplaceholder_site_id_dc6a85b6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_staticplaceholder_site_id_dc6a85b6 ON public.cms_staticplaceholder USING btree (site_id);


--
-- Name: cms_title_has_url_overwrite_ecf27bb9; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_has_url_overwrite_ecf27bb9 ON public.cms_title USING btree (has_url_overwrite);


--
-- Name: cms_title_language_50a0dfa1; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_language_50a0dfa1 ON public.cms_title USING btree (language);


--
-- Name: cms_title_language_50a0dfa1_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_language_50a0dfa1_like ON public.cms_title USING btree (language varchar_pattern_ops);


--
-- Name: cms_title_page_id_5fade2a3; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_page_id_5fade2a3 ON public.cms_title USING btree (page_id);


--
-- Name: cms_title_path_c484314c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_path_c484314c ON public.cms_title USING btree (path);


--
-- Name: cms_title_path_c484314c_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_path_c484314c_like ON public.cms_title USING btree (path varchar_pattern_ops);


--
-- Name: cms_title_publisher_is_draft_95874c88; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_publisher_is_draft_95874c88 ON public.cms_title USING btree (publisher_is_draft);


--
-- Name: cms_title_publisher_state_9a952b0f; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_publisher_state_9a952b0f ON public.cms_title USING btree (publisher_state);


--
-- Name: cms_title_slug_4947d146; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_slug_4947d146 ON public.cms_title USING btree (slug);


--
-- Name: cms_title_slug_4947d146_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_title_slug_4947d146_like ON public.cms_title USING btree (slug varchar_pattern_ops);


--
-- Name: cms_treenode_parent_id_59bb02c4; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_treenode_parent_id_59bb02c4 ON public.cms_treenode USING btree (parent_id);


--
-- Name: cms_treenode_path_6eb22885_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_treenode_path_6eb22885_like ON public.cms_treenode USING btree (path varchar_pattern_ops);


--
-- Name: cms_treenode_site_id_d3f46985; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_treenode_site_id_d3f46985 ON public.cms_treenode USING btree (site_id);


--
-- Name: cms_usersettings_clipboard_id_3ae17c19; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cms_usersettings_clipboard_id_3ae17c19 ON public.cms_usersettings USING btree (clipboard_id);


--
-- Name: cmsplugin_cascade_cascadeclipboard_identifier_6621d067_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_cascadeclipboard_identifier_6621d067_like ON public.cmsplugin_cascade_cascadeclipboard USING btree (identifier varchar_pattern_ops);


--
-- Name: cmsplugin_cascade_element_shared_glossary_id_607dc805; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_element_shared_glossary_id_607dc805 ON public.cmsplugin_cascade_element USING btree (shared_glossary_id);


--
-- Name: cmsplugin_cascade_iconfont_identifier_2c2df4a9_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_iconfont_identifier_2c2df4a9_like ON public.cmsplugin_cascade_iconfont USING btree (identifier varchar_pattern_ops);


--
-- Name: cmsplugin_cascade_iconfont_zip_file_id_7da0a199; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_iconfont_zip_file_id_7da0a199 ON public.cmsplugin_cascade_iconfont USING btree (zip_file_id);


--
-- Name: cmsplugin_cascade_inline_cascade_element_id_bef4d1d2; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_inline_cascade_element_id_bef4d1d2 ON public.cmsplugin_cascade_inline USING btree (cascade_element_id);


--
-- Name: cmsplugin_cascade_page_icon_font_id_8624d66d; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_page_icon_font_id_8624d66d ON public.cmsplugin_cascade_page USING btree (icon_font_id);


--
-- Name: cmsplugin_cascade_pluginextrafields_plugin_type_05dc46f5; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_pluginextrafields_plugin_type_05dc46f5 ON public.cmsplugin_cascade_pluginextrafields USING btree (plugin_type);


--
-- Name: cmsplugin_cascade_pluginextrafields_plugin_type_05dc46f5_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_pluginextrafields_plugin_type_05dc46f5_like ON public.cmsplugin_cascade_pluginextrafields USING btree (plugin_type varchar_pattern_ops);


--
-- Name: cmsplugin_cascade_pluginextrafields_site_id_0afc685b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_pluginextrafields_site_id_0afc685b ON public.cmsplugin_cascade_pluginextrafields USING btree (site_id);


--
-- Name: cmsplugin_cascade_sharedglossary_identifier_3dbbc4d6_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_sharedglossary_identifier_3dbbc4d6_like ON public.cmsplugin_cascade_sharedglossary USING btree (identifier varchar_pattern_ops);


--
-- Name: cmsplugin_cascade_sharedglossary_plugin_type_98408398; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_sharedglossary_plugin_type_98408398 ON public.cmsplugin_cascade_sharedglossary USING btree (plugin_type);


--
-- Name: cmsplugin_cascade_sharedglossary_plugin_type_98408398_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_sharedglossary_plugin_type_98408398_like ON public.cmsplugin_cascade_sharedglossary USING btree (plugin_type varchar_pattern_ops);


--
-- Name: cmsplugin_cascade_sortinline_cascade_element_id_15bf5242; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_sortinline_cascade_element_id_15bf5242 ON public.cmsplugin_cascade_sortinline USING btree (cascade_element_id);


--
-- Name: cmsplugin_cascade_sortinline_order_77dc03a6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX cmsplugin_cascade_sortinline_order_77dc03a6 ON public.cmsplugin_cascade_sortinline USING btree ("order");


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_name_5fe0edc6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6 ON public.easy_thumbnails_source USING btree (name);


--
-- Name: easy_thumbnails_source_name_5fe0edc6_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_source_name_5fe0edc6_like ON public.easy_thumbnails_source USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9 ON public.easy_thumbnails_source USING btree (storage_hash);


--
-- Name: easy_thumbnails_source_storage_hash_946cbcc9_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_source_storage_hash_946cbcc9_like ON public.easy_thumbnails_source USING btree (storage_hash varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31 ON public.easy_thumbnails_thumbnail USING btree (name);


--
-- Name: easy_thumbnails_thumbnail_name_b5882c31_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_thumbnail_name_b5882c31_like ON public.easy_thumbnails_thumbnail USING btree (name varchar_pattern_ops);


--
-- Name: easy_thumbnails_thumbnail_source_id_5b57bc77; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_thumbnail_source_id_5b57bc77 ON public.easy_thumbnails_thumbnail USING btree (source_id);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49 ON public.easy_thumbnails_thumbnail USING btree (storage_hash);


--
-- Name: easy_thumbnails_thumbnail_storage_hash_f1435f49_like; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX easy_thumbnails_thumbnail_storage_hash_f1435f49_like ON public.easy_thumbnails_thumbnail USING btree (storage_hash varchar_pattern_ops);


--
-- Name: filer_clipboard_user_id_b52ff0bc; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_clipboard_user_id_b52ff0bc ON public.filer_clipboard USING btree (user_id);


--
-- Name: filer_clipboarditem_clipboard_id_7a76518b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_clipboarditem_clipboard_id_7a76518b ON public.filer_clipboarditem USING btree (clipboard_id);


--
-- Name: filer_clipboarditem_file_id_06196f80; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_clipboarditem_file_id_06196f80 ON public.filer_clipboarditem USING btree (file_id);


--
-- Name: filer_file_folder_id_af803bbb; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_file_folder_id_af803bbb ON public.filer_file USING btree (folder_id);


--
-- Name: filer_file_owner_id_b9e32671; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_file_owner_id_b9e32671 ON public.filer_file USING btree (owner_id);


--
-- Name: filer_file_polymorphic_ctype_id_f44903c1; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_file_polymorphic_ctype_id_f44903c1 ON public.filer_file USING btree (polymorphic_ctype_id);


--
-- Name: filer_folder_owner_id_be530fb4; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folder_owner_id_be530fb4 ON public.filer_folder USING btree (owner_id);


--
-- Name: filer_folder_parent_id_308aecda; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folder_parent_id_308aecda ON public.filer_folder USING btree (parent_id);


--
-- Name: filer_folder_tree_id_b016223c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folder_tree_id_b016223c ON public.filer_folder USING btree (tree_id);


--
-- Name: filer_folder_tree_id_lft_088ce52b_idx; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folder_tree_id_lft_088ce52b_idx ON public.filer_folder USING btree (tree_id, lft);


--
-- Name: filer_folderpermission_folder_id_5d02f1da; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folderpermission_folder_id_5d02f1da ON public.filer_folderpermission USING btree (folder_id);


--
-- Name: filer_folderpermission_group_id_8901bafa; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folderpermission_group_id_8901bafa ON public.filer_folderpermission USING btree (group_id);


--
-- Name: filer_folderpermission_user_id_7673d4b6; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX filer_folderpermission_user_id_7673d4b6 ON public.filer_folderpermission USING btree (user_id);


--
-- Name: post_office_attachment_emails_attachment_id_6136fd9a; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_attachment_emails_attachment_id_6136fd9a ON public.post_office_attachment_emails USING btree (attachment_id);


--
-- Name: post_office_attachment_emails_email_id_96875fd9; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_attachment_emails_email_id_96875fd9 ON public.post_office_attachment_emails USING btree (email_id);


--
-- Name: post_office_email_created_1306952f; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_email_created_1306952f ON public.post_office_email USING btree (created);


--
-- Name: post_office_email_last_updated_0ffcec35; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_email_last_updated_0ffcec35 ON public.post_office_email USING btree (last_updated);


--
-- Name: post_office_email_scheduled_time_3869ebec; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_email_scheduled_time_3869ebec ON public.post_office_email USING btree (scheduled_time);


--
-- Name: post_office_email_status_013a896c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_email_status_013a896c ON public.post_office_email USING btree (status);


--
-- Name: post_office_email_template_id_417da7da; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_email_template_id_417da7da ON public.post_office_email USING btree (template_id);


--
-- Name: post_office_emailtemplate_default_template_id_2ac2f889; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_emailtemplate_default_template_id_2ac2f889 ON public.post_office_emailtemplate USING btree (default_template_id);


--
-- Name: post_office_log_email_id_d42c8808; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX post_office_log_email_id_d42c8808 ON public.post_office_log USING btree (email_id);


--
-- Name: shop_notification_mail_template_id_bb050f73; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX shop_notification_mail_template_id_bb050f73 ON public.shop_notification USING btree (mail_template_id);


--
-- Name: shop_notification_recipient_id_81184845; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX shop_notification_recipient_id_81184845 ON public.shop_notification USING btree (recipient_id);


--
-- Name: shop_notificationattachment_attachment_id_da92621b; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX shop_notificationattachment_attachment_id_da92621b ON public.shop_notificationattachment USING btree (attachment_id);


--
-- Name: shop_notificationattachment_notification_id_d30cc951; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX shop_notificationattachment_notification_id_d30cc951 ON public.shop_notificationattachment USING btree (notification_id);


--
-- Name: socialaccount_socialaccount_user_id_8146e70c; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX socialaccount_socialaccount_user_id_8146e70c ON public.socialaccount_socialaccount USING btree (user_id);


--
-- Name: socialaccount_socialapp_sites_site_id_2579dee5; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX socialaccount_socialapp_sites_site_id_2579dee5 ON public.socialaccount_socialapp_sites USING btree (site_id);


--
-- Name: socialaccount_socialapp_sites_socialapp_id_97fb6e7d; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX socialaccount_socialapp_sites_socialapp_id_97fb6e7d ON public.socialaccount_socialapp_sites USING btree (socialapp_id);


--
-- Name: socialaccount_socialtoken_account_id_951f210e; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX socialaccount_socialtoken_account_id_951f210e ON public.socialaccount_socialtoken USING btree (account_id);


--
-- Name: socialaccount_socialtoken_app_id_636a42d7; Type: INDEX; Schema: public; Owner: serg
--

CREATE INDEX socialaccount_socialtoken_app_id_636a42d7 ON public.socialaccount_socialtoken USING btree (app_id);


--
-- Name: account_emailaddress account_emailaddress_user_id_2c513194_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailaddress
    ADD CONSTRAINT account_emailaddress_user_id_2c513194_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: account_emailconfirmation account_emailconfirm_email_address_id_5b7f8c58_fk_account_e; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.account_emailconfirmation
    ADD CONSTRAINT account_emailconfirm_email_address_id_5b7f8c58_fk_account_e FOREIGN KEY (email_address_id) REFERENCES public.account_emailaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_billingaddress alby_billingaddress_customer_id_cf0174f1_fk_alby_cust; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_billingaddress
    ADD CONSTRAINT alby_billingaddress_customer_id_cf0174f1_fk_alby_cust FOREIGN KEY (customer_id) REFERENCES public.alby_customer(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_cart alby_cart_billing_address_id_73dec424_fk_alby_billingaddress_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart
    ADD CONSTRAINT alby_cart_billing_address_id_73dec424_fk_alby_billingaddress_id FOREIGN KEY (billing_address_id) REFERENCES public.alby_billingaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_cart alby_cart_customer_id_ef0446f5_fk_alby_customer_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart
    ADD CONSTRAINT alby_cart_customer_id_ef0446f5_fk_alby_customer_user_id FOREIGN KEY (customer_id) REFERENCES public.alby_customer(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_cart alby_cart_shipping_address_id_7ed543be_fk_alby_ship; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cart
    ADD CONSTRAINT alby_cart_shipping_address_id_7ed543be_fk_alby_ship FOREIGN KEY (shipping_address_id) REFERENCES public.alby_shippingaddress(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_cartitem alby_cartitem_cart_id_7c441df8_fk_alby_cart_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cartitem
    ADD CONSTRAINT alby_cartitem_cart_id_7c441df8_fk_alby_cart_id FOREIGN KEY (cart_id) REFERENCES public.alby_cart(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_cartitem alby_cartitem_product_id_fb0ffe80_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_cartitem
    ADD CONSTRAINT alby_cartitem_product_id_fb0ffe80_fk_alby_product_id FOREIGN KEY (product_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_commodity alby_commodity_placeholder_id_eab39765_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity
    ADD CONSTRAINT alby_commodity_placeholder_id_eab39765_fk_cms_placeholder_id FOREIGN KEY (placeholder_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_commodity alby_commodity_product_code_id_262e5fc0_fk_alby_productlist_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity
    ADD CONSTRAINT alby_commodity_product_code_id_262e5fc0_fk_alby_productlist_id FOREIGN KEY (product_code_id) REFERENCES public.alby_productlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_commodity alby_commodity_product_ptr_id_06151ec6_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity
    ADD CONSTRAINT alby_commodity_product_ptr_id_06151ec6_fk_alby_product_id FOREIGN KEY (product_ptr_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_commodity_translation alby_commodity_trans_master_id_db0f61db_fk_alby_comm; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodity_translation
    ADD CONSTRAINT alby_commodity_trans_master_id_db0f61db_fk_alby_comm FOREIGN KEY (master_id) REFERENCES public.alby_commodity(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_commodityinventory alby_commodityinvent_product_id_3435e842_fk_alby_comm; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_commodityinventory
    ADD CONSTRAINT alby_commodityinvent_product_id_3435e842_fk_alby_comm FOREIGN KEY (product_id) REFERENCES public.alby_commodity(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_customer alby_customer_user_id_4a7d98a5_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_customer
    ADD CONSTRAINT alby_customer_user_id_4a7d98a5_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_delivery alby_delivery_order_id_413f1920_fk_alby_order_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_delivery
    ADD CONSTRAINT alby_delivery_order_id_413f1920_fk_alby_order_id FOREIGN KEY (order_id) REFERENCES public.alby_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_deliveryitem alby_deliveryitem_delivery_id_edb43ddc_fk_alby_delivery_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_deliveryitem
    ADD CONSTRAINT alby_deliveryitem_delivery_id_edb43ddc_fk_alby_delivery_id FOREIGN KEY (delivery_id) REFERENCES public.alby_delivery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_deliveryitem alby_deliveryitem_item_id_9f06ce8c_fk_alby_orderitem_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_deliveryitem
    ADD CONSTRAINT alby_deliveryitem_item_id_9f06ce8c_fk_alby_orderitem_id FOREIGN KEY (item_id) REFERENCES public.alby_orderitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_fabric alby_fabric_product_code_id_d6518426_fk_alby_productlist_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric
    ADD CONSTRAINT alby_fabric_product_code_id_d6518426_fk_alby_productlist_id FOREIGN KEY (product_code_id) REFERENCES public.alby_productlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_fabric alby_fabric_product_ptr_id_f55963b3_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric
    ADD CONSTRAINT alby_fabric_product_ptr_id_f55963b3_fk_alby_product_id FOREIGN KEY (product_ptr_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_fabric_translation alby_fabric_translat_master_id_7a6c7571_fk_alby_fabr; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_fabric_translation
    ADD CONSTRAINT alby_fabric_translat_master_id_7a6c7571_fk_alby_fabr FOREIGN KEY (master_id) REFERENCES public.alby_fabric(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_lamel alby_lamel_discont_scheme_id_ed2f6283_fk_alby_discount_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel
    ADD CONSTRAINT alby_lamel_discont_scheme_id_ed2f6283_fk_alby_discount_id FOREIGN KEY (discont_scheme_id) REFERENCES public.alby_discount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_lamel alby_lamel_product_code_id_0c837519_fk_alby_productlist_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel
    ADD CONSTRAINT alby_lamel_product_code_id_0c837519_fk_alby_productlist_id FOREIGN KEY (product_code_id) REFERENCES public.alby_productlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_lamel alby_lamel_product_ptr_id_6eb3e10a_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel
    ADD CONSTRAINT alby_lamel_product_ptr_id_6eb3e10a_fk_alby_product_id FOREIGN KEY (product_ptr_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_lamel_translation alby_lamel_translati_master_id_041fc106_fk_alby_lame; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamel_translation
    ADD CONSTRAINT alby_lamel_translati_master_id_041fc106_fk_alby_lame FOREIGN KEY (master_id) REFERENCES public.alby_lamel(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_lamelinventory alby_lamelinventory_product_id_98f39136_fk_alby_lame; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_lamelinventory
    ADD CONSTRAINT alby_lamelinventory_product_id_98f39136_fk_alby_lame FOREIGN KEY (product_id) REFERENCES public.alby_lamel(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_order alby_order_customer_id_eb896ea6_fk_alby_customer_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_order
    ADD CONSTRAINT alby_order_customer_id_eb896ea6_fk_alby_customer_user_id FOREIGN KEY (customer_id) REFERENCES public.alby_customer(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_orderitem alby_orderitem_order_id_146a310d_fk_alby_order_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderitem
    ADD CONSTRAINT alby_orderitem_order_id_146a310d_fk_alby_order_id FOREIGN KEY (order_id) REFERENCES public.alby_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_orderitem alby_orderitem_product_id_4d948465_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderitem
    ADD CONSTRAINT alby_orderitem_product_id_4d948465_fk_alby_product_id FOREIGN KEY (product_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_orderpayment alby_orderpayment_order_id_169b1020_fk_alby_order_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_orderpayment
    ADD CONSTRAINT alby_orderpayment_order_id_169b1020_fk_alby_order_id FOREIGN KEY (order_id) REFERENCES public.alby_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_product alby_product_polymorphic_ctype_id_1aea35eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_product
    ADD CONSTRAINT alby_product_polymorphic_ctype_id_1aea35eb_fk_django_co FOREIGN KEY (polymorphic_ctype_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_productimage alby_productimage_image_id_0419f545_fk_filer_image_file_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productimage
    ADD CONSTRAINT alby_productimage_image_id_0419f545_fk_filer_image_file_ptr_id FOREIGN KEY (image_id) REFERENCES public.filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_productimage alby_productimage_product_id_d73c92c5_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productimage
    ADD CONSTRAINT alby_productimage_product_id_d73c92c5_fk_alby_product_id FOREIGN KEY (product_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_productpage alby_productpage_page_id_958bf826_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productpage
    ADD CONSTRAINT alby_productpage_page_id_958bf826_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_productpage alby_productpage_product_id_e2eb2b4a_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_productpage
    ADD CONSTRAINT alby_productpage_product_id_e2eb2b4a_fk_alby_product_id FOREIGN KEY (product_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_producttranslation alby_producttranslation_master_id_9aa75e48_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_producttranslation
    ADD CONSTRAINT alby_producttranslation_master_id_9aa75e48_fk_alby_product_id FOREIGN KEY (master_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_shippingaddress alby_shippingaddress_customer_id_01de57c2_fk_alby_cust; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_shippingaddress
    ADD CONSTRAINT alby_shippingaddress_customer_id_01de57c2_fk_alby_cust FOREIGN KEY (customer_id) REFERENCES public.alby_customer(user_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_sofamodel alby_sofamodel_product_ptr_id_41a9d935_fk_alby_product_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel
    ADD CONSTRAINT alby_sofamodel_product_ptr_id_41a9d935_fk_alby_product_id FOREIGN KEY (product_ptr_id) REFERENCES public.alby_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_sofamodel_translation alby_sofamodel_trans_master_id_3de2a345_fk_alby_sofa; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofamodel_translation
    ADD CONSTRAINT alby_sofamodel_trans_master_id_3de2a345_fk_alby_sofa FOREIGN KEY (master_id) REFERENCES public.alby_sofamodel(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_sofavariant alby_sofavariant_fabric_id_eea026e4_fk_alby_fabr; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofavariant
    ADD CONSTRAINT alby_sofavariant_fabric_id_eea026e4_fk_alby_fabr FOREIGN KEY (fabric_id) REFERENCES public.alby_fabric(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_sofavariant alby_sofavariant_product_code_id_3ebfa900_fk_alby_prod; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofavariant
    ADD CONSTRAINT alby_sofavariant_product_code_id_3ebfa900_fk_alby_prod FOREIGN KEY (product_code_id) REFERENCES public.alby_productlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_sofavariant alby_sofavariant_product_model_id_68bca617_fk_alby_sofa; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_sofavariant
    ADD CONSTRAINT alby_sofavariant_product_model_id_68bca617_fk_alby_sofa FOREIGN KEY (product_model_id) REFERENCES public.alby_sofamodel(product_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_variantimage alby_variantimage_image_id_8bf62496_fk_filer_image_file_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_variantimage
    ADD CONSTRAINT alby_variantimage_image_id_8bf62496_fk_filer_image_file_ptr_id FOREIGN KEY (image_id) REFERENCES public.filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: alby_variantimage alby_variantimage_product_id_7f470d32_fk_alby_sofavariant_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.alby_variantimage
    ADD CONSTRAINT alby_variantimage_product_id_7f470d32_fk_alby_sofavariant_id FOREIGN KEY (product_id) REFERENCES public.alby_sofavariant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_alias_placeholder_id_6d6e0c8a_fk_cms_place FOREIGN KEY (alias_placeholder_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_cmsplugin_ptr_id_f71dfd31_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_cmsplugin_ptr_id_f71dfd31_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_aliaspluginmodel cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_aliaspluginmodel
    ADD CONSTRAINT cms_aliaspluginmodel_plugin_id_9867676e_fk_cms_cmsplugin_id FOREIGN KEY (plugin_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_cmsplugin cms_cmsplugin_parent_id_fd3bd9dd_fk_cms_cmsplugin_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_parent_id_fd3bd9dd_fk_cms_cmsplugin_id FOREIGN KEY (parent_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_cmsplugin cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_cmsplugin
    ADD CONSTRAINT cms_cmsplugin_placeholder_id_0bfa3b26_fk_cms_placeholder_id FOREIGN KEY (placeholder_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermis_globalpagepermission_46bd2681_fk_cms_globa; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermis_globalpagepermission_46bd2681_fk_cms_globa FOREIGN KEY (globalpagepermission_id) REFERENCES public.cms_globalpagepermission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission_sites cms_globalpagepermis_site_id_00460b53_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission_sites
    ADD CONSTRAINT cms_globalpagepermis_site_id_00460b53_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission cms_globalpagepermission_group_id_991b4733_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_group_id_991b4733_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_globalpagepermission cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_globalpagepermission
    ADD CONSTRAINT cms_globalpagepermission_user_id_a227cee1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page cms_page_node_id_c87b85a9_fk_cms_treenode_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page
    ADD CONSTRAINT cms_page_node_id_c87b85a9_fk_cms_treenode_id FOREIGN KEY (node_id) REFERENCES public.cms_treenode(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page_placeholders cms_page_placeholder_placeholder_id_6b120886_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholder_placeholder_id_6b120886_fk_cms_place FOREIGN KEY (placeholder_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page_placeholders cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page_placeholders
    ADD CONSTRAINT cms_page_placeholders_page_id_f2ce8dec_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_page cms_page_publisher_public_id_d619fca0_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_page
    ADD CONSTRAINT cms_page_publisher_public_id_d619fca0_fk_cms_page_id FOREIGN KEY (publisher_public_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_group_id_af5af193_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_group_id_af5af193_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_page_id_0ae9a163_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_page_id_0ae9a163_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pagepermission cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pagepermission
    ADD CONSTRAINT cms_pagepermission_user_id_0c7d2b3c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageuser cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageuser
    ADD CONSTRAINT cms_pageuser_created_by_id_8e9fbf83_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageuser cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageuser
    ADD CONSTRAINT cms_pageuser_user_ptr_id_b3d65592_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageusergroup cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_created_by_id_7d57fa39_fk_auth_user_id FOREIGN KEY (created_by_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_pageusergroup cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_pageusergroup
    ADD CONSTRAINT cms_pageusergroup_group_ptr_id_34578d93_fk_auth_group_id FOREIGN KEY (group_ptr_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_placeholderreference cms_placeholderrefer_cmsplugin_ptr_id_6977ec85_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_placeholderreference
    ADD CONSTRAINT cms_placeholderrefer_cmsplugin_ptr_id_6977ec85_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_placeholderreference cms_placeholderrefer_placeholder_ref_id_244759b1_fk_cms_place; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_placeholderreference
    ADD CONSTRAINT cms_placeholderrefer_placeholder_ref_id_244759b1_fk_cms_place FOREIGN KEY (placeholder_ref_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_draft_id_5aee407b_fk_cms_placeholder_id FOREIGN KEY (draft_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_public_id_876aaa66_fk_cms_placeholder_id FOREIGN KEY (public_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_staticplaceholder cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_staticplaceholder
    ADD CONSTRAINT cms_staticplaceholder_site_id_dc6a85b6_fk_django_site_id FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_title cms_title_page_id_5fade2a3_fk_cms_page_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title
    ADD CONSTRAINT cms_title_page_id_5fade2a3_fk_cms_page_id FOREIGN KEY (page_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_title cms_title_publisher_public_id_003a2702_fk_cms_title_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_title
    ADD CONSTRAINT cms_title_publisher_public_id_003a2702_fk_cms_title_id FOREIGN KEY (publisher_public_id) REFERENCES public.cms_title(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_treenode cms_treenode_parent_id_59bb02c4_fk_cms_treenode_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_treenode
    ADD CONSTRAINT cms_treenode_parent_id_59bb02c4_fk_cms_treenode_id FOREIGN KEY (parent_id) REFERENCES public.cms_treenode(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_treenode cms_treenode_site_id_d3f46985_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_treenode
    ADD CONSTRAINT cms_treenode_site_id_d3f46985_fk_django_site_id FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_usersettings cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_usersettings
    ADD CONSTRAINT cms_usersettings_clipboard_id_3ae17c19_fk_cms_placeholder_id FOREIGN KEY (clipboard_id) REFERENCES public.cms_placeholder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cms_usersettings cms_usersettings_user_id_09633b2d_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cms_usersettings
    ADD CONSTRAINT cms_usersettings_user_id_09633b2d_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_element cmsplugin_cascade_el_cmsplugin_ptr_id_aaa25230_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_element
    ADD CONSTRAINT cmsplugin_cascade_el_cmsplugin_ptr_id_aaa25230_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_element cmsplugin_cascade_el_shared_glossary_id_607dc805_fk_cmsplugin; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_element
    ADD CONSTRAINT cmsplugin_cascade_el_shared_glossary_id_607dc805_fk_cmsplugin FOREIGN KEY (shared_glossary_id) REFERENCES public.cmsplugin_cascade_sharedglossary(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_iconfont cmsplugin_cascade_ic_zip_file_id_7da0a199_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_iconfont
    ADD CONSTRAINT cmsplugin_cascade_ic_zip_file_id_7da0a199_fk_filer_fil FOREIGN KEY (zip_file_id) REFERENCES public.filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_inline cmsplugin_cascade_in_cascade_element_id_bef4d1d2_fk_cmsplugin; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_inline
    ADD CONSTRAINT cmsplugin_cascade_in_cascade_element_id_bef4d1d2_fk_cmsplugin FOREIGN KEY (cascade_element_id) REFERENCES public.cmsplugin_cascade_element(cmsplugin_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_pa_extended_object_id_cd17c9f0_fk_cms_page_; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_pa_extended_object_id_cd17c9f0_fk_cms_page_ FOREIGN KEY (extended_object_id) REFERENCES public.cms_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_pa_icon_font_id_8624d66d_fk_cmsplugin; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_pa_icon_font_id_8624d66d_fk_cmsplugin FOREIGN KEY (icon_font_id) REFERENCES public.cmsplugin_cascade_iconfont(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_page cmsplugin_cascade_pa_public_extension_id_32f16696_fk_cmsplugin; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_page
    ADD CONSTRAINT cmsplugin_cascade_pa_public_extension_id_32f16696_fk_cmsplugin FOREIGN KEY (public_extension_id) REFERENCES public.cmsplugin_cascade_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_pluginextrafields cmsplugin_cascade_pl_site_id_0afc685b_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_pluginextrafields
    ADD CONSTRAINT cmsplugin_cascade_pl_site_id_0afc685b_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cmsplugin_cascade_sortinline cmsplugin_cascade_so_cascade_element_id_15bf5242_fk_cmsplugin; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.cmsplugin_cascade_sortinline
    ADD CONSTRAINT cmsplugin_cascade_so_cascade_element_id_15bf5242_fk_cmsplugin FOREIGN KEY (cascade_element_id) REFERENCES public.cmsplugin_cascade_element(cmsplugin_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: djangocms_text_ckeditor_text djangocms_text_ckedi_cmsplugin_ptr_id_946882c1_fk_cms_cmspl; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.djangocms_text_ckeditor_text
    ADD CONSTRAINT djangocms_text_ckedi_cmsplugin_ptr_id_946882c1_fk_cms_cmspl FOREIGN KEY (cmsplugin_ptr_id) REFERENCES public.cms_cmsplugin(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnail easy_thumbnails_thum_source_id_5b57bc77_fk_easy_thum; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnail
    ADD CONSTRAINT easy_thumbnails_thum_source_id_5b57bc77_fk_easy_thum FOREIGN KEY (source_id) REFERENCES public.easy_thumbnails_source(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: easy_thumbnails_thumbnaildimensions easy_thumbnails_thum_thumbnail_id_c3a0c549_fk_easy_thum; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.easy_thumbnails_thumbnaildimensions
    ADD CONSTRAINT easy_thumbnails_thum_thumbnail_id_c3a0c549_fk_easy_thum FOREIGN KEY (thumbnail_id) REFERENCES public.easy_thumbnails_thumbnail(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboard filer_clipboard_user_id_b52ff0bc_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboard
    ADD CONSTRAINT filer_clipboard_user_id_b52ff0bc_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboarditem filer_clipboarditem_clipboard_id_7a76518b_fk_filer_clipboard_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_clipboard_id_7a76518b_fk_filer_clipboard_id FOREIGN KEY (clipboard_id) REFERENCES public.filer_clipboard(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_clipboarditem filer_clipboarditem_file_id_06196f80_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_clipboarditem
    ADD CONSTRAINT filer_clipboarditem_file_id_06196f80_fk_filer_file_id FOREIGN KEY (file_id) REFERENCES public.filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_folder_id_af803bbb_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_file
    ADD CONSTRAINT filer_file_folder_id_af803bbb_fk_filer_folder_id FOREIGN KEY (folder_id) REFERENCES public.filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_owner_id_b9e32671_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_file
    ADD CONSTRAINT filer_file_owner_id_b9e32671_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_file filer_file_polymorphic_ctype_id_f44903c1_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_file
    ADD CONSTRAINT filer_file_polymorphic_ctype_id_f44903c1_fk_django_co FOREIGN KEY (polymorphic_ctype_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folder filer_folder_owner_id_be530fb4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folder
    ADD CONSTRAINT filer_folder_owner_id_be530fb4_fk_auth_user_id FOREIGN KEY (owner_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folder filer_folder_parent_id_308aecda_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folder
    ADD CONSTRAINT filer_folder_parent_id_308aecda_fk_filer_folder_id FOREIGN KEY (parent_id) REFERENCES public.filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_folder_id_5d02f1da_fk_filer_folder_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_folder_id_5d02f1da_fk_filer_folder_id FOREIGN KEY (folder_id) REFERENCES public.filer_folder(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_group_id_8901bafa_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_group_id_8901bafa_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_folderpermission filer_folderpermission_user_id_7673d4b6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_folderpermission
    ADD CONSTRAINT filer_folderpermission_user_id_7673d4b6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filer_image filer_image_file_ptr_id_3e21d4f0_fk_filer_file_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.filer_image
    ADD CONSTRAINT filer_image_file_ptr_id_3e21d4f0_fk_filer_file_id FOREIGN KEY (file_ptr_id) REFERENCES public.filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: post_office_attachment_emails post_office_attachme_attachment_id_6136fd9a_fk_post_offi; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment_emails
    ADD CONSTRAINT post_office_attachme_attachment_id_6136fd9a_fk_post_offi FOREIGN KEY (attachment_id) REFERENCES public.post_office_attachment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: post_office_attachment_emails post_office_attachme_email_id_96875fd9_fk_post_offi; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_attachment_emails
    ADD CONSTRAINT post_office_attachme_email_id_96875fd9_fk_post_offi FOREIGN KEY (email_id) REFERENCES public.post_office_email(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: post_office_email post_office_email_template_id_417da7da_fk_post_offi; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_email
    ADD CONSTRAINT post_office_email_template_id_417da7da_fk_post_offi FOREIGN KEY (template_id) REFERENCES public.post_office_emailtemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: post_office_emailtemplate post_office_emailtem_default_template_id_2ac2f889_fk_post_offi; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_emailtemplate
    ADD CONSTRAINT post_office_emailtem_default_template_id_2ac2f889_fk_post_offi FOREIGN KEY (default_template_id) REFERENCES public.post_office_emailtemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: post_office_log post_office_log_email_id_d42c8808_fk_post_office_email_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.post_office_log
    ADD CONSTRAINT post_office_log_email_id_d42c8808_fk_post_office_email_id FOREIGN KEY (email_id) REFERENCES public.post_office_email(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shop_notification shop_notification_mail_template_id_bb050f73_fk_post_offi; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notification
    ADD CONSTRAINT shop_notification_mail_template_id_bb050f73_fk_post_offi FOREIGN KEY (mail_template_id) REFERENCES public.post_office_emailtemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shop_notification shop_notification_recipient_id_81184845_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notification
    ADD CONSTRAINT shop_notification_recipient_id_81184845_fk_auth_user_id FOREIGN KEY (recipient_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shop_notificationattachment shop_notificationatt_attachment_id_da92621b_fk_filer_fil; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notificationattachment
    ADD CONSTRAINT shop_notificationatt_attachment_id_da92621b_fk_filer_fil FOREIGN KEY (attachment_id) REFERENCES public.filer_file(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: shop_notificationattachment shop_notificationatt_notification_id_d30cc951_fk_shop_noti; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.shop_notificationattachment
    ADD CONSTRAINT shop_notificationatt_notification_id_d30cc951_fk_shop_noti FOREIGN KEY (notification_id) REFERENCES public.shop_notification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_account_id_951f210e_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_account_id_951f210e_fk_socialacc FOREIGN KEY (account_id) REFERENCES public.socialaccount_socialaccount(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialtoken socialaccount_social_app_id_636a42d7_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialtoken
    ADD CONSTRAINT socialaccount_social_app_id_636a42d7_fk_socialacc FOREIGN KEY (app_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_site_id_2579dee5_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_site_id_2579dee5_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialapp_sites socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialapp_sites
    ADD CONSTRAINT socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc FOREIGN KEY (socialapp_id) REFERENCES public.socialaccount_socialapp(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: socialaccount_socialaccount socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: serg
--

ALTER TABLE ONLY public.socialaccount_socialaccount
    ADD CONSTRAINT socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

