from django import forms
from alby.models import ParametricModel
from django_json_widget.widgets import JSONEditorWidget
from parler.forms import TranslatableModelForm
from django.forms import Textarea


class ParametricModelAdminForm(TranslatableModelForm):
    class Meta:
        model = ParametricModel
        fields = ('alter_prop', 'caption', 'description')
        widgets = {
            'alter_prop': JSONEditorWidget(width='100%', height='300px'),
            'dxf_draws': Textarea(attrs={'rows': 1, 'cols': 40, 'style': 'height: 1em;'})
        }
