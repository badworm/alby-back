# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from parler.admin import TranslatableAdmin
from cms.admin.placeholderadmin import PlaceholderAdminMixin, FrontendEditableAdminMixin
from adminsortable2.admin import SortableAdminMixin
from shop.admin.product import CMSPageAsCategoryMixin, ProductImageInline, InvalidateProductCacheMixin
from polymorphic.admin import PolymorphicChildModelAdmin
from alby.models import Product
from alby.forms import ParametricModelAdminForm
from alby.models import CadProduct


@admin.register(CadProduct)
class CadProductAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, CMSPageAsCategoryMixin,
                      FrontendEditableAdminMixin, PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    base_model = Product
    form = ParametricModelAdminForm
    list_display = ['product_name', 'product_type', 'active']
    list_editable = ['active', ]
    fieldsets = (
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_title', 'product_code'),
                ('active', ),
                ('part_file', 'pdf_draws', 'threed_obj')
            ],
        }),
        (_("Properties"), {
            'fields': ['alter_prop', 'dxf_draws'],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
    )
    readonly_fields = ['product_code', ]
    filter_horizontal = ['cms_pages', ]
    prepopulated_fields = {'slug': ['product_name']}
    inlines = [ProductImageInline, ]
    save_as = True
