# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from parler.admin import TranslatableAdmin
from filer.models import ThumbnailOption
from cms.admin.placeholderadmin import PlaceholderAdminMixin, FrontendEditableAdminMixin
from alby.admin import customer
from alby.models.order import Order
from alby.admin.order import OrderAdmin as BaseOrderAdmin
from shop.admin.order import PrintInvoiceAdminMixin
# from shop.admin.delivery import DeliveryOrderAdminMixin
from adminsortable2.admin import SortableAdminMixin, PolymorphicSortableAdminMixin
from shop.admin.product import CMSPageAsCategoryMixin, ProductImageInline, InvalidateProductCacheMixin, \
    CMSPageFilter
from polymorphic.admin import (PolymorphicParentModelAdmin, PolymorphicChildModelAdmin,
                               PolymorphicChildModelFilter)
from alby.models import Product, SofaModel, SofaVariant
from alby.models import Discount, Fabric, VariantImage, ParametricModel
from nested_inline.admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline
from alby.forms import ParametricModelAdminForm
from dxfgen.models import FurnitureCadFile
from django import forms
from shop.models.related import ProductImageModel
from alby.models import CadProduct


admin.site.site_header = "ALBY Administration"
admin.site.unregister(ThumbnailOption)


@admin.register(Order)
# class OrderAdmin(BaseOrderAdmin, PrintInvoiceAdminMixin, DeliveryOrderAdminMixin):
class OrderAdmin(BaseOrderAdmin, PrintInvoiceAdminMixin):
    list_display = ['get_number', 'customer', 'status_name', 'get_total', 'created_at']


__all__ = ['customer']


class ProductImageInlineNested(admin.StackedInline):
    model = ProductImageModel
    inlines = []
    extra = 1


class VariantImageInline(NestedTabularInline):
    model = VariantImage
    fk_name = 'product'
    extra = 1


class SofaVariantForm(forms.ModelForm):
    unit_price = forms.DecimalField(required=False, decimal_places=2)

    def __init__(self, *args, **kwargs):
        super(SofaVariantForm, self).__init__(*args, **kwargs)
        self.fields['unit_price'].widget.attrs['min'] = 0.01

    def clean_unit_price(self):
        price = self.cleaned_data['unit_price']
        if price and price < 0.0099:
            raise forms.ValidationError("Price cannot be less than 0.01")
        return price

    class Meta:
        model = SofaVariant
        fields = '__all__'


class SofaVariantInLine(NestedStackedInline):
    model = SofaVariant
    inlines = [VariantImageInline, ]
    fk_name = 'product_model'
    readonly_fields = ['product_code', ]
    can_delete = True
    extra = 1
    save_as = True
    form = SofaVariantForm


@admin.register(Fabric)
class FabricAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, FrontendEditableAdminMixin,
                  CMSPageAsCategoryMixin, PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    base_model = Product
    fieldsets = (
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_title',),
                ('product_code', 'unit_price'),
                ('active',),
            ],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
        (_("Properties"), {
            'fields': ['composition', 'care', 'fabric_type'],
        }),
    )
    readonly_fields = ['product_code', ]
    filter_horizontal = ['cms_pages']
    inlines = [ProductImageInline, ]
    prepopulated_fields = {'slug': ['product_name']}
    save_as = True


@admin.register(SofaModel)
class SofaModelAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, FrontendEditableAdminMixin,
                     CMSPageAsCategoryMixin, PlaceholderAdminMixin, PolymorphicChildModelAdmin, NestedModelAdmin):
    base_model = Product
    fieldsets = (
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_title',),
            ],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
        (_("Properties"), {
            'fields': ['sofa_type'],
        }),
    )
    filter_horizontal = ['cms_pages']
    inlines = [ProductImageInlineNested, SofaVariantInLine]
    prepopulated_fields = {'slug': ['product_name']}
    save_as = True


admin.site.register(Discount)


@admin.register(Product)
class ProductAdmin(PolymorphicSortableAdminMixin, PolymorphicParentModelAdmin):
    base_model = Product
    child_models = [SofaModel, Fabric, ParametricModel, CadProduct]
    list_display = ['product_name', 'get_price', 'product_type', 'active']
    list_display_links = ['product_name']
    list_editable = ['active', ]
    search_fields = ['product_name']
    list_filter = [PolymorphicChildModelFilter, CMSPageFilter]
    list_per_page = 250
    list_max_show_all = 1000

    def get_price(self, obj):
        return str(obj.get_real_instance().get_price(None))

    get_price.short_description = _("Price starting at")


class FurnitureModelInline(admin.StackedInline):
    model = FurnitureCadFile
    extra = 1


@admin.register(ParametricModel)
class ParametricModelAdmin(InvalidateProductCacheMixin, SortableAdminMixin, TranslatableAdmin, CMSPageAsCategoryMixin,
                           FrontendEditableAdminMixin, PlaceholderAdminMixin, PolymorphicChildModelAdmin):
    base_model = Product
    form = ParametricModelAdminForm
    list_display = ['product_name', 'product_type', 'active']
    list_editable = ['active', ]
    fieldsets = (
        (None, {
            'fields': [
                ('product_name', 'slug'),
                ('product_title', 'product_code'),
                ('active', 'is_parametric'),
                ('product_cad',),
            ],
        }),
        (_("Properties"), {
            'fields': ['alter_prop', ],
        }),
        (_("Translatable Fields"), {
            'fields': ['caption', 'description'],
        }),
    )
    readonly_fields = ['product_code', ]
    filter_horizontal = ['cms_pages', 'product_cad']
    prepopulated_fields = {'slug': ['product_name']}
    inlines = [ProductImageInline, ]
    save_as = True
