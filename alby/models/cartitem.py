from alby.models import Customer
from django.utils.translation import ugettext_lazy as _
from django.db.models import CharField, PositiveIntegerField
from shop.models.cart import BaseCartItem
from shop.models.cart import CartItemManager, CartManager
from shop.modifiers.pool import cart_modifiers_pool
from dxfgen.models import TaskData
from django.core.exceptions import ObjectDoesNotExist


class FilteredCartItemManager(CartItemManager):
    """
    Customized model manager for our CartItem model.
    """

    def filter_cart_items(self, cart, request):
        """
        Use this method to fetch items for shopping from the cart. It rearranges the result set
        according to the defined modifiers.
        """
        cart_items = self.filter(cart=cart, quantity__gt=0).order_by('updated_at')
        # cart_items = self.filter(cart=cart, quantity__gt=0, cart_type='parametric').order_by('updated_at')
        for modifier in cart_modifiers_pool.get_all_modifiers():
            cart_items = modifier.arrange_cart_items(cart_items, request)
        return cart_items


class ParametricCartItemManager(FilteredCartItemManager):

    def get_or_create(self, **kwargs):
        """
        Create a unique cart item. If the same product exists already in the given cart,
        increase its quantity, if the product in the cart seems to be the same.
        """
        cart = kwargs.pop('cart')
        product = kwargs.pop('product')
        quantity = int(kwargs.pop('quantity', 1))
        extra = kwargs.pop('extra')
        extra['settings'] = product.product_cad.first().part_nest_settings
        for i in extra['settings']:
            extra['settings'][i].pop('dimens')
        cart_type = kwargs.pop('cart_type')


        # add a new item to the cart, or reuse an existing one, increasing the quantity
        watched = not quantity
        cart_item = product.is_in_cart(cart, watched=watched, cart_type=cart_type, extra=extra, **kwargs)
        if cart_item:
            if not watched:
                cart_item.quantity += quantity
            created = False
        else:
            cart_item = self.model(cart=cart, product=product, quantity=quantity, cart_type=cart_type, extra=extra, **kwargs)
            created = True
        return cart_item, created


class CartItem(BaseCartItem):

    CART_TYPE = (('cart', 'cart'), ('samples', 'samples'), ('parametric', 'parametric'), ('other', 'other'))

    cart_type = CharField(
        _('Cart type'),
        default='cart',
        max_length=11,
        choices=CART_TYPE,
        help_text=_("Choose cart type")
    )

    quantity = PositiveIntegerField()

    parameric_obj = ParametricCartItemManager()

    objects = FilteredCartItemManager()

    class Meta:
        app_label = 'alby'

    def delete(self, *args, **kwargs):
        try:
            a = TaskData.objects.get(cart_id=self.cart_id, cartitem_id=self.id)
            a.delete()
        except ObjectDoesNotExist:
            pass
        super().delete(*args, **kwargs)
