# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db.models import SET_DEFAULT, Sum
from shop.models.address import BaseShippingAddress, BaseBillingAddress
from shop.models.cart import BaseCart, CartManager
from shop import deferred
from shop.modifiers.pool import cart_modifiers_pool
from collections import OrderedDict
from alby.models.cartitem import CartItem


class Cart(BaseCart):
    """
    Default materialized model for BaseCart containing common fields
    """
    shipping_address = deferred.ForeignKey(
        BaseShippingAddress,
        on_delete=SET_DEFAULT,
        null=True,
        default=None,
        related_name='+',
    )

    billing_address = deferred.ForeignKey(
        BaseBillingAddress,
        on_delete=SET_DEFAULT,
        null=True,
        default=None,
        related_name='+',
    )

    def update(self, request, raise_exception=False):
        """
        This should be called after a cart item changed quantity, has been added or removed.

        It will loop over all items in the cart, and call all the configured cart modifiers.
        After this is done, it will compute and update the order's total and subtotal fields, along
        with any supplement added along the way by modifiers.

        Note that theses added fields are not stored - we actually want to
        reflect rebate and tax changes on the *cart* items, but we don't want
        that for the order items (since they are legally binding after the
        "purchase" button was pressed)
        """
        if not self._dirty:
            return

        if self._cached_cart_items:
            items = self._cached_cart_items
        else:
            items = CartItem.objects.filter_cart_items(self, request)

        # This calls all the pre_process_cart methods and the pre_process_cart_item for each item,
        # before processing the cart. This allows to prepare and collect data on the cart.
        for modifier in cart_modifiers_pool.get_all_modifiers():
            modifier.pre_process_cart(self, request, raise_exception)
            for item in items:
                modifier.pre_process_cart_item(self, item, request, raise_exception)

        self.extra_rows = OrderedDict()  # reset the dictionary
        self.subtotal = 0  # reset the subtotal
        for item in items:
            # item.update iterates over all cart modifiers and invokes method `process_cart_item`
            item.update(request)
            self.subtotal += item.line_total

        # Iterate over the registered modifiers, to process the cart's summary
        for modifier in cart_modifiers_pool.get_all_modifiers():
            for item in items:
                modifier.post_process_cart_item(self, item, request)
            modifier.process_cart(self, request)

        # This calls the post_process_cart method from cart modifiers, if any.
        # It allows for a last bit of processing on the "finished" cart, before
        # it is displayed
        for modifier in reversed(cart_modifiers_pool.get_all_modifiers()):
            modifier.post_process_cart(self, request)

        # Cache updated cart items
        self._cached_cart_items = items
        self._dirty = False

    @property
    def total_weight(self):
        """
        Returns the total weight of all items in the cart (for Shipping Services).
        """
        rez = 0
        for item in self.items.all():
            try:
                rez += float(item.product.weight) * item.quantity
            except:
                pass
        return float("{0:.2f}".format(rez))

    @property
    def num_items(self):
        """
        Returns the number of items in the cart.
        """
        # TODO: cart_type='parametric' SET to var
        items = self.items.filter(
            quantity__gt=0,
            cart_type='parametric',
        ).count()
        return items

    @property
    def total_quantity(self):
        """
        Returns the total quantity of all items in the cart.
        """
        items = self.items.filter(
            quantity__gt=0,
            cart_type='parametric',
        )
        aggr = items.aggregate(quantity=Sum('quantity'))
        return aggr['quantity'] or 0
        # if we would know, that self.items is already evaluated, then this might be faster:
        # return sum([ci.quantity for ci in self.items.all()])
