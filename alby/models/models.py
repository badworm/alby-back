# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from alby.models.cartitem import CartItem
from alby.models.address import BillingAddress, ShippingAddress
from alby.models.customer import Customer

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import FileExtensionValidator
from djangocms_text_ckeditor.fields import HTMLField
from dxfgen.models import FurnitureCadFile

from filer.fields import image

from polymorphic.query import PolymorphicQuerySet
from parler.managers import TranslatableManager, TranslatableQuerySet
from parler.models import TranslatableModelMixin, TranslatedFieldsModel, TranslatedFields
from parler.fields import TranslatedField

from s3sync.backends.storage_backends import PrivateMediaStorage
from shop.money.fields import MoneyField
from shop.models.product import BaseProduct, BaseProductManager, CMSPageReferenceMixin
from shop.models.defaults.mapping import ProductPage, ProductImage
from shop.money import Money
from uuid import uuid4


__all__ = ['CartItem', 'BillingAddress', 'ShippingAddress', 'Customer']


def get_upload_to(instance, filename):
    return '%s/%s' % (instance.product_code, filename)


def get_upload_to_random(instance, filename):
    return '%s/%s' % (uuid4().hex, filename)


class ProductQuerySet(TranslatableQuerySet, PolymorphicQuerySet):
    pass


class ProductManager(BaseProductManager, TranslatableManager):
    queryset_class = ProductQuerySet

    def get_queryset(self):
        qs = self.queryset_class(self.model, using=self._db)
        return qs.prefetch_related('translations')


class ProductList(models.Model):
    product_code = models.CharField(
        _("Product model"),
        max_length=255,
        unique=True,
    )
    product_model = models.CharField(
        _("Product model"),
        max_length=255,
        blank=True,
    )

    def is_unique_scu(self, scu):
        scu = str(scu)
        return ProductList.objects.filter(product_code=scu).count() == 0

    def set_num_scu(self, scu, n):
        scu = str(scu)
        while len(scu) <= int(n):
            scu = '0' + scu
        return scu

    def get_max_scu(self):
        max_scu = ProductList.objects.aggregate(models.Max('product_code'))
        try:
            return int(max_scu['product_code__max'])
        except:
            return 1

    def save(self, *args, **kwargs):
        if not self.product_code:
            max_scu = self.get_max_scu()
            while True:
                new_scu = max_scu + 1
                if self.is_unique_scu(new_scu):
                    self.product_code = self.set_num_scu(new_scu, 8)
                    break
        super(ProductList, self).save(*args, **kwargs)

    def __str__(self):
        return self.product_code


# @python_2_unicode_compatible
class Product(CMSPageReferenceMixin, TranslatableModelMixin, BaseProduct):
    """
    Base class to describe a polymorphic product. Here we declare common fields available in all of
    our different product types. These common fields are also used to build up the view displaying
    a list of all products.
    """
    product_name = models.CharField(
        _("Product Name"),
        max_length=255,
    )
    slug = models.SlugField(
        _("Slug"),
        unique=True,
    )
    product_title = models.CharField(
        _("Product Title (for SEO)"),
        max_length=255,
        blank=True,
    )

    caption = TranslatedField()

    # controlling the catalog
    order = models.PositiveIntegerField(
        _("Sort by"),
        db_index=True,
    )

    cms_pages = models.ManyToManyField(
        'cms.Page',
        through=ProductPage,
        help_text=_("Choose list view this product shall appear on."),
    )

    images = models.ManyToManyField(
        'filer.Image',
        through=ProductImage,
        blank=True
    )

    is_parametric = models.BooleanField(default=False)

    class Meta:
        ordering = ('order',)
        verbose_name = _("Product")
        verbose_name_plural = _("Products")

    objects = ProductManager()

    # filter expression used to lookup for a product item using the Select2 widget
    lookup_fields = ['product_name__icontains']

    def __str__(self):
        return self.product_name

    @property
    def sample_image(self):
        return self.images.first()

    def is_in_cart(self, cart, watched=False, cart_type='cart', extra=False, **kwargs):
        if extra:
            cart_item_qs = CartItem.objects.filter(cart=cart, product=self, cart_type=cart_type, extra__contains=extra)
        else:
            cart_item_qs = CartItem.objects.filter(cart=cart, product=self, cart_type=cart_type)
        return cart_item_qs.first()


class ProductTranslation(TranslatedFieldsModel):
    master = models.ForeignKey(
        Product,
        on_delete=models.CASCADE,
        related_name='translations',
        null=True,
    )
    caption = HTMLField(
        verbose_name=_("Caption"),
        blank=True,
        null=True,
        configuration='CKEDITOR_SETTINGS_CAPTION',
        help_text=_(
            "Short description used in the catalog's list view of products."),
    ),
    description = HTMLField(
        verbose_name=_("Description"),
        configuration='CKEDITOR_SETTINGS_DESCRIPTION',
        blank=True,
        help_text=_(
            "Full description used in the catalog's detail view of Smart Cards."),
    ),

    class Meta:
        unique_together = [('language_code', 'master')]


class Fabric(Product):
    fabric_name = models.CharField(
        _("Fabric name"),
        max_length=150,
        blank=True,
    )
    # common product fields
    unit_price = MoneyField(
        _("Price per meter"),
        decimal_places=2,
        help_text=_("Net price for this product by meter"),
    )
    # product properties
    FABRIC_TYPE = [
        ('leath', 'leather'),
        ('velv', 'velvet'),
        ('wool', 'wool'),
    ]
    fabric_type = models.CharField(
        _("Fabric type"),
        choices=FABRIC_TYPE,
        max_length=15,
    )

    product_code = models.ForeignKey(
        ProductList,
        on_delete=models.CASCADE,
        blank=True,
    )
    composition = models.CharField(
        _("Comosition of fabric"),
        max_length=255,
        unique=False,
    )
    care = models.CharField(
        _("Recommended wash care"),
        max_length=255,
        unique=False,
    )
    multilingual = TranslatedFields(
        description=HTMLField(
            verbose_name=_("Description"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
        caption=HTMLField(
            verbose_name=_("Caption"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
    )

    default_manager = ProductManager()

    class Meta:
        verbose_name = _("Fabric")
        verbose_name_plural = _("Fabrics")

    def get_price(self, request):
        return self.unit_price


class SofaModel(Product):
    sofa_type = models.CharField(
        _("Sofa Type"),
        choices=[(t, t) for t in (_('Straight'), _('Corner'), _('2 seat'), _('3 seat'), _('Sofabed'), _('Chair'))],
        max_length=9,
    )
    multilingual = TranslatedFields(
        description=HTMLField(
            verbose_name=_("Description"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
        caption=HTMLField(
            verbose_name=_("Caption"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
    )
    # other fields to map the specification sheet

    default_manager = ProductManager()

    lookup_fields = ('product_name__icontains',)

    def get_price(self, request):
        aggregate = self.variants.aggregate(models.Min('unit_price'))
        return Money(aggregate['unit_price__min'])

    def is_in_cart(self, cart, watched=False, **kwargs):
        try:
            product_code = kwargs['product_code']
        except KeyError:
            return
        cart_item_qs = CartItem.objects.filter(cart=cart, product=self)
        for cart_item in cart_item_qs:
            if cart_item.product_code == product_code:
                return cart_item

    def get_product_variant(self, **kwargs):
        try:
            product_code = kwargs.get('product_code')
            # added new model for the whole product list
            id_var = ProductList.objects.get(product_code=product_code).id
            return self.variants.get(product_code=id_var)
        except SofaVariant.DoesNotExist as e:
            raise SofaModel.DoesNotExist(e)


class SofaVariant(models.Model):
    class Meta:
        ordering = ('unit_price',)

    product_model = models.ForeignKey(
        SofaModel,
        related_name='variants',
        on_delete=models.CASCADE,
    )
    product_code = models.ForeignKey(
        ProductList,
        on_delete=models.CASCADE,
        blank=True,
    )
    images = models.ManyToManyField(
        'filer.Image',
        through='VariantImage',
    )

    unit_price = MoneyField(_("Unit price"), blank=True)

    fabric = models.ForeignKey(
        Fabric,
        on_delete=models.CASCADE,
        blank=True,
    )

    def get_availability(self, request, **kwargs):
        return True

    def __str__(self):
        return self.fabric.fabric_name

    def delete(self, using=None, keep_parents=False):
        ProductList.objects.filter(product_code=self.product_code).delete()
        super(SofaVariant, self).delete()


class VariantImage(models.Model):
    image = image.FilerImageField(on_delete=models.CASCADE)
    product = models.ForeignKey(
        SofaVariant,
        on_delete=models.CASCADE,
        blank=True,
    )
    order = models.SmallIntegerField(default=0)

    class Meta:
        abstract = False
        verbose_name = _("Product Image")
        verbose_name_plural = _("Product Images")
        ordering = ['order']


class ParametricModel(Product):

    def __init__(self, *args, **kwargs):
        self.is_parametric = True
        super().__init__(*args, **kwargs)

    product_code = models.ForeignKey(
        ProductList,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    unit_price = MoneyField(
        _("Unit price"),
        decimal_places=2,
        help_text=_("Net price for this product"),
    )

    alter_prop = models.JSONField(verbose_name=_('List of parametric properties'), blank=True, null=True)

    product_cad = models.ManyToManyField(FurnitureCadFile)

    multilingual = TranslatedFields(
        description=HTMLField(
            verbose_name=_("Description"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
        caption=HTMLField(
            verbose_name=_("Caption"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
    )

    default_manager = ProductManager()

    class Meta:
        verbose_name = _("Paeametric product")
        verbose_name_plural = _("Parametric products")

    def get_price(self, request):
        return self.unit_price

    def save(self, *args, **kwargs):
        if self.is_parametric is True and not self.unit_price:
            self.unit_price = 0.1
        super().save(*args, **kwargs)


class CadProduct(Product):

    def __init__(self, *args, **kwargs):
        self.is_parametric = True
        super().__init__(*args, **kwargs)

    product_code = models.ForeignKey(
        ProductList,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    unit_price = MoneyField(
        _("Unit price"),
        decimal_places=2,
        help_text=_("Net price for this product"),
    )

    alter_prop = models.JSONField(verbose_name=_('List of parametric properties'), blank=True, null=True)

    part_file = models.FileField(upload_to=get_upload_to_random, verbose_name='Cad File', validators=[FileExtensionValidator(
        {'FCStd', })], storage=PrivateMediaStorage())

    pdf_draws = models.FileField(upload_to=get_upload_to_random, verbose_name='Drafts File', validators=[FileExtensionValidator(
        {'pdf', })], storage=PrivateMediaStorage(), blank=True)

    dxf_draws = models.TextField(verbose_name='Dxf files', blank=True)
    # svg_imgs = models.TextField(verbose_name='Svg images', blank=True)

    threed_obj = models.FileField(upload_to=get_upload_to_random, verbose_name='3d File', validators=[FileExtensionValidator(
        {'x3d', 'obj', 'gltf', 'stl'})], storage=PrivateMediaStorage(), blank=True)

    multilingual = TranslatedFields(
        description=HTMLField(
            verbose_name=_("Description"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
        caption=HTMLField(
            verbose_name=_("Caption"),
            configuration='CKEDITOR_SETTINGS_DESCRIPTION',
            blank=True,
            help_text=_(
                "Full description used in the catalog's detail view of Smart Cards."),
        ),
    )

    default_manager = ProductManager()

    class Meta:
        verbose_name = _("Webcad product")
        verbose_name_plural = _("Webcad products")

    def get_price(self, request):
        return self.unit_price

    def save(self, *args, **kwargs):
        if not self.unit_price:
            self.unit_price = 0.1
        super().save(*args, **kwargs)


def get_partid_file_by_prod(product_id):
    return ParametricModel.objects.get(id=product_id).product_cad.first().part_id
