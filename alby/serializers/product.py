from alby.models import SofaModel, ParametricModel, CadProduct
from shop.serializers.bases import ProductSerializer
from rest_framework import serializers
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError


class SofaDetailSerializer(ProductSerializer):
    class Meta:
        model = SofaModel
        fields = ['id', 'product_name', 'product_url', 'product_model', 'price', 'media']


class CustomizedProductSerializer(serializers.ModelSerializer):
    # param_fields = serializers.SerializerMethodField()

    class Meta:
        model = CadProduct
        fields = ('product_name', 'product_title', 'alter_prop', 'images', 'caption', 'description')


class ParametricProductSerializer(serializers.ModelSerializer):
    # param_fields = serializers.SerializerMethodField()
    url = serializers.URLField(source='get_absolute_url', read_only=True)
    name = serializers.CharField(source='product_name')
    title = serializers.CharField(source='product_title')
    specs = serializers.CharField(source='caption')
    media = serializers.SerializerMethodField()

    class Meta:
        model = CadProduct
        fields = ('id', 'url', 'name', 'title', 'alter_prop', 'media', 'specs', 'description')

    def get_media(self, product):
        imgs = [{'src': str(i.url), 'alt': str(i.default_alt_text), 'type': 'image'} for i in product.images.all()]
        return {'full': imgs, 'thumbnails': self._thumbnails(product)}

    def _thumbnails(self, product):
        try:
            tmbs = []
            for item in product.images.all():
                thumb = get_thumbnailer(item)
                tmbs.append({
                    'src': thumb.get_thumbnail({'size': (80, 80)}).url,
                    'alt': str(item.default_alt_text)
                })
            return tmbs
        except (AttributeError, ValueError, InvalidImageFormatError):
            return [{'src': 'https://picsum.photos/80/80?random=1'}]


class ParametricProductDxf(serializers.ModelSerializer):
    dxf = serializers.CharField(source='dxf_draws')

    class Meta:
        model = CadProduct
        fields = ('dxf', )
