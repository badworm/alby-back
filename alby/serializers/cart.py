from shop.serializers.cart import CartSerializer, BaseItemSerializer, CartItems
from alby.models.cartitem import CartItem
from alby.models.cart import Cart
from shop.conf import app_settings
from alby.models import Product
from django.db.models import Subquery
from shop.modifiers.pool import cart_modifiers_pool
from rest_framework import serializers
from dxfgen.models import TaskData
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError


# class FilteredCartItemSerializer(serializers.ModelSerializer):
class FilteredCartItemSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(lookup_field='pk', view_name='cart-detail')
    title = serializers.SerializerMethodField()
    product_url = serializers.SerializerMethodField()
    src = serializers.SerializerMethodField()

    class Meta(BaseItemSerializer.Meta):
        model = CartItem
        exclude = ['cart', 'id']

    def create(self, validated_data):
        validated_data['cart'] = Cart.objects.get_or_create_from_request(self.context['request'])
        cart_item, _ = CartItem.parameric_obj.get_or_create(**validated_data)
        cart_item.save()
        return cart_item

    # summary = serializers.SerializerMethodField(
    #     help_text="Sub-serializer for fields to be shown in the product's summary.")

    def to_representation(self, cart_item):
        cart_item.update(self.context['request'])
        representation = super().to_representation(cart_item)
        return representation

    def validate_product(self, product):
        if not product.active:
            msg = "Product `{}` is inactive, and can not be added to the cart."
            raise serializers.ValidationError(msg.format(product))
        return product

    def get_title(self, product):
        return product.product.product_name

    def get_product_url(self, product):
        return product.product.get_absolute_url()

    def get_src(self, product):
        try:
            thumb = get_thumbnailer(product.product.images.first())
            return thumb.get_thumbnail({'size': (80, 80)}).url
        except (AttributeError, InvalidImageFormatError):
            return 'no image'

    # def get_summary(self, cart_item):
    #     serializer_class = app_settings.PRODUCT_SUMMARY_SERIALIZER
    #     serializer = serializer_class(cart_item.product, context=self.context,
    #                                   read_only=True, label=self.root.label)
    #     return serializer.data




class ParametricCartItemSerializer(FilteredCartItemSerializer):
    url = serializers.HyperlinkedIdentityField(lookup_field='pk', view_name='parametric-detail')
    item_status = serializers.SerializerMethodField()
    line_total = None
    unit_price = None
    extra_rows = None

    class Meta(BaseItemSerializer.Meta):
        model = CartItem
        exclude = ['cart', 'product_code', 'updated_at']

    # def __init__(self, *args, **kwargs):
    #     self.is_param = False
    #     # self.cart_type = 'cart'
    #     super().__init__(*args, **kwargs)

    def get_item_status(self, obj):
        status = TaskData.objects.filter(cartitem_id=obj.id)
        return status.first().task_status if len(status) else 'ok'


class CommonCartSerializer(CartSerializer):
    total_weight = serializers.FloatField()

    class Meta(CartSerializer.Meta):
        model = Cart
        fields = ['total_weight', ] + CartSerializer.Meta.fields

    def __init__(self, *args, **kwargs):
        self.is_param = False
        self.cart_type = 'cart'
        super().__init__(*args, **kwargs)

    def represent_items(self, cart):
        items = CartItem.objects.filter(
            cart=cart,
            quantity__gt=0,
            cart_type=self.cart_type,
        ).order_by('-updated_at')
        if self.with_items != CartItems.unsorted:
            for modifier in cart_modifiers_pool.get_all_modifiers():
                items = modifier.arrange_cart_items(items, self.context['request'])
        serializer = FilteredCartItemSerializer(items, context=self.context, label=self.label, many=True)
        return serializer.data


class ParametricCartSerializer(CommonCartSerializer):
    # materials = serializers.SerializerMethodField()

    class Meta(CartSerializer.Meta):
        model = Cart
        fields = ['num_items', 'id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_param = True
        self.cart_type = 'parametric'

    def represent_items(self, cart):
        items = CartItem.objects.filter(
            cart=cart,
            quantity__gt=0,
            cart_type=self.cart_type,
        ).order_by('-updated_at')
        if self.with_items != CartItems.unsorted:
            for modifier in cart_modifiers_pool.get_all_modifiers():
                items = modifier.arrange_cart_items(items, self.context['request'])
        serializer = ParametricCartItemSerializer(items, context=self.context, label=self.label, many=True)
        return serializer.data

    # def get_materials(self, product):
    #     self.materials = []
    #     raw_mat = RawMaterial.objects.all()
    #     for raw in raw_mat:
    #         self.materials.append({
    #             'name': raw.name,
    #             'id': raw.id,
    #             'txtr': raw.is_texture,
    #             'width': raw.width,
    #             'height': raw.height,
    #             'thick': raw.thick,
    #             'prt_gap': 10,
    #             'edge_gap': 10
    #         })
    #     return self.materials


class FilteredCartSerializer(CommonCartSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_param = False
