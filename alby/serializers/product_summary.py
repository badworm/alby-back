# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import serializers
from shop.serializers.bases import ProductSerializer
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError


class ProductSummarySerializer(ProductSerializer):
    """
    Default serializer to create a summary from our Product model. This summary then is used to
    render various list views, such as the catalog-, the cart-, and the list of ordered items.
    In case the Product model is polymorphic, this shall serialize the smallest common denominator
    of all product information.
    """
    name = serializers.CharField(source='product_name')
    url = serializers.URLField(source='get_absolute_url', read_only=True)
    thumbnail = serializers.SerializerMethodField(
        help_text="Returns a rendered HTML snippet containing a sample image among other elements",
    )

    class Meta(ProductSerializer.Meta):
        fields = ['id', 'name', 'url', 'price', 'thumbnail']

    def get_thumbnail(self, product):
        try:
            thumb = get_thumbnailer(product.images.all().first())
            return {"src": thumb.get_thumbnail({'size': (180, 180)}).url}
        except (AttributeError, ValueError, InvalidImageFormatError):
            return 'https://picsum.photos/150/150?random=1'

# a = Title.objects.filter(menu_title__isnull=False, published=True, publisher_is_draft=False).exclude(menu_title__exact='')
