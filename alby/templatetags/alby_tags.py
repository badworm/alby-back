import os
import uuid
from django import template
from django.conf import settings
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe
from shop.templatetags.shop_tags import CartIcon
from alby.models.cart import Cart
from shop.rest.money import JSONRenderer
from alby.serializers.cart import FilteredCartSerializer
from shop.serializers.cart import CartItems


register = template.Library()


@register.simple_tag(name='get_area')
def get_area(width, height):
    return round(width*height/1000000, 3)


@register.simple_tag(name='cache_bust')
def cache_bust():

    if settings.DEBUG:
        version = uuid.uuid1()
    else:
        version = os.environ.get('PROJECT_VERSION')
        if version is None:
            version = '1'

    return '__v__={version}'.format(version=version)

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

class AlbyCartIcon(CartIcon):

    def render(self, context):
        try:
            cart = Cart.objects.get_from_request(context['request'])
            serializer = FilteredCartSerializer(instance=cart, context=context, label='dropdown', with_items=self.with_items)
            cart_data = JSONRenderer().render(serializer.data)
        except Cart.DoesNotExist:
            cart_data = {'total_quantity': 0, 'num_items': 0}
        context.update({
            'cart_as_json': mark_safe(force_text(cart_data)),
            'has_dropdown': self.with_items != CartItems.without,
        })
        return self.get_template().render(context)


@register.tag
def cart_icon(parser, token):
    def raise_syntax_error():
        choices = '|'.join([item.name for item in CartItems])
        raise template.TemplateSyntaxError("Template tag '{}' takes one optional argument: {}".format(bits[0], choices))

    bits = token.split_contents()
    if len(bits) > 2:
        raise_syntax_error()
    if len(bits) == 2:
        try:
            with_items = CartItems(bits[1])
        except ValueError:
            raise_syntax_error()
    else:
        with_items = CartItems.without
    return AlbyCartIcon(with_items)
