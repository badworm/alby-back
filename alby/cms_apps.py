# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from cms.app_base import CMSApp

from cms.apphook_pool import apphook_pool
from cms.cms_menus import SoftRootCutter
from menus.menu_pool import menu_pool
from shop.cms_apphooks import CatalogListCMSApp, CatalogSearchApp, OrderApp, PasswordResetApp
from alby.serializers import ParametricProductSerializer, ParametricProductDxf
from alby.views import UpdateDataView
from dxfgen.views import ParametricDataView
from shop.rest.filters import CMSPagesFilterBackend
from dxfgen.serializers import PartListSerializer
from alby.views.products import ProductPage, ProductDxf
from alby.views.catalog import ProductListViewApi


class CatalogListApp(CatalogListCMSApp):
    def get_urls(self, page=None, language=None, **kwargs):
        from shop.views.catalog import AddToCartView, ProductRetrieveView, ProductListView
        from alby.serializers import RebateAddToCartSerializer, AddSofaToCartSerializer, UpdateDataSerialiser
        from shop.views.catalog import AddFilterContextMixin
        from shop.search.mixins import ProductSearchViewMixin

        # ProductListView = type('ProductSearchListView',
        #                        (AddFilterContextMixin, ProductListView), {})

        # ProductSearchView = type('ProductSearchListView', (ProductSearchViewMixin, ProductListView), {})

        filter_backends = [CMSPagesFilterBackend]
        # filter_backends.extend(api_settings.DEFAULT_FILTER_BACKENDS)

        return [
            url('parametric-list/parametric-data', ParametricDataView.as_view(
                serializer_class=PartListSerializer, )),
            url(r'^(?P<slug>[\w-]+)/add-to-cart', AddToCartView.as_view()),

            url(r'^(?P<slug>[\w-]+)/get-rebate', AddToCartView.as_view(
                serializer_class=RebateAddToCartSerializer,)),

            url(r'^(?P<slug>[\w-]+)/add-sofa-to-cart', AddToCartView.as_view(
                serializer_class=AddSofaToCartSerializer,)),

            url(r'^(?P<slug>[\w-]+)/update-small-data', UpdateDataView.as_view(
                serializer_class=UpdateDataSerialiser,)),

            url(r'^(?P<slug>[\w-]+)/?$', ProductPage.as_view(
                serializer_class=ParametricProductSerializer,
            )),

            url(r'^(?P<slug>[\w-]+)/dxf?$', ProductDxf.as_view(
                serializer_class=ParametricProductDxf,
            )),

            # url(r'^/divany/(?P<slug>[\w-]+)', ProductRetrieveView.as_view(
            #     use_modal_dialog=False,
            #     serializer_class=SofaDetailSerializer,)),

            # url(r'^(?P<slug>[\w-]+)/?$', ProductRetrieveView.as_view(
            #     use_modal_dialog=False,
            # )),

            url(r'^', ProductListViewApi.as_view(
                filter_backends=filter_backends, )),

            url(r'^', ProductListView.as_view(
                filter_backends=filter_backends, )),
            # url(r'^(?P<slug>[\w-]+)', ProductRetrieveView.as_view(
            #     use_modal_dialog=False,)),
            # url(r'^', ProductSearchView.as_view(
            #     redirect_to_lonely_product=True,
            # )),
        ]


apphook_pool.register(CatalogListApp)


# @apphook_pool.register
# class ParametricApphook(CMSApp):
#     app_name = "parametric"
#     name = "Parameter Application"
#
#     def get_urls(self, page=None, language=None, **kwargs):
#         return ["parametric.urls"]


from shop.cms_apphooks import CatalogSearchApp

apphook_pool.register(CatalogSearchApp)

apphook_pool.register(OrderApp)

apphook_pool.register(PasswordResetApp)


def _deregister_menu_pool_modifier(Modifier):
    index = None
    for k, modifier_class in enumerate(menu_pool.modifiers):
        if issubclass(modifier_class, Modifier):
            index = k
    if index is not None:
        # intentionally only modifying the list
        menu_pool.modifiers.pop(index)

_deregister_menu_pool_modifier(SoftRootCutter)
