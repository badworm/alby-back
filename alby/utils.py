from io import StringIO, BytesIO
import zipfile
from os import path


#TODO: clean dict without static vals
def dict_vlaue_int(data: dict) -> dict:
    try:
        return {key: int(data[key]) for key in data.keys() if ('productData' not in key) and ('dxf' not in key)}
    except ValueError:
        raise Exception('A dicts key is not int convertable')


def archive_data(files: dict):
    in_mem_arch = BytesIO()
    # with zipfile.ZipFile(f'{self.temp_folder}\\{puck_name}.zip', 'w', compression=zipfile.ZIP_LZMA) as zf:
    with zipfile.ZipFile(in_mem_arch, 'w', compression=zipfile.ZIP_LZMA) as zf:
        for dxf_data in files.keys():
            zf.writestr(dxf_data, files[dxf_data])
    in_mem_arch.seek(0)
    arch_name = 'a.zip'
    return in_mem_arch, arch_name



