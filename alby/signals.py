from django.db.models.signals import pre_save, post_save, m2m_changed
from django.dispatch import receiver
from django.conf import settings
from .models import ProductList, SofaVariant
from alby.models import CartItem, ParametricModel, CadProduct
from ast import literal_eval
import requests
from alby.settings import WEBCAD_URL
from requests.exceptions import RequestException
import json

# list_of_models = ('SofaVariant', 'Product', 'Commodity', 'Lamel', 'Fabric', 'ParametricModel')
#
#
# @receiver(post_save)
# def save_scu(sender, instance, created, **kwargs):
#     if sender.__name__ in list_of_models and created:
#         a = instance.id
#         saved_obj = ProductList.objects.create(product_model=sender.__name__)
#         instance.product_code_id = saved_obj.id


# @receiver(post_save, sender=CartItem)
# def run_task(instance, created=False, **kwargs):
#     if instance.cart_type == 'parametric':
#         print(instance.product_code)
#         print(instance.extra)


@receiver(post_save, sender=CadProduct)
def set_cad_data(instance, created=True, **kwargs):
    if instance.alter_prop:
        return True
    try:
        r = requests.post(f'{WEBCAD_URL}/get-all', json={'file': str(instance.part_file)})
        data = r.json()
        a = json.dumps(data.pop('dxf'))
        instance.dxf_draws = a
        instance.alter_prop = data
        instance.save()
    except RequestException:
        raise Exception('Some troubles with your requests')


def set_alter_prop(instance, action, created=False, **kwargs):
    if action == 'post_add':
        if not instance.alter_prop:
            try:
                if instance.product_cad.first().exposed_vars:
                    instance.alter_prop = literal_eval(instance.product_cad.first().exposed_vars)
                    instance.save()
                else:
                    print('empty exposed')
            except AttributeError:
                raise AttributeError("Selected cad file doesn't have exposed vars. Try re-save cad file.")


m2m_changed.connect(set_alter_prop, sender=ParametricModel.product_cad.through)
