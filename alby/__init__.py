from .tasks import app as celery_app

__all__ = ('celery_app',)

__version__ = '0.0.1'
default_app_config = 'alby.apps.alby'
