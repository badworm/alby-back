import os
from celery import Celery
from celery.result import AsyncResult

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'alby.settings')

app = Celery('alby')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


def get_task_status(task_id):
    res = AsyncResult(task_id, app=app)
    return res.status


def get_exposed_params_celery(part_id):
    task = app.send_task('edge.getvars', args=[part_id])
    return task.id


def generate_files_celery(part_id: str, req_data: dict, cart_item_id=0):
    task = app.send_task('edge.generatefiles', args=[part_id, req_data, cart_item_id])
    return task.id


def run_nesting(nest_data: str, user_id: int, sheet_pref: list, task_id: str):
    task = app.send_task('edge.start_nesting', args=[nest_data, user_id, sheet_pref, task_id])
    return task.id
