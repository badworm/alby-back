# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import re_path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.http import HttpResponse
from cms.sitemaps import CMSSitemap
from alby.sitemap import ProductSitemap
from alby.settings import prefix_default_language as is_prefix
from rest_framework import routers
from alby.views.cart import ParametricCartViewSet, CommonCartViewSet

app_name = 'alby'

router = routers.DefaultRouter()
router.register(r'parametric', ParametricCartViewSet, basename='parametric')
router.register(r'cart', CommonCartViewSet, basename='cart')

sitemaps = {'cmspages': CMSSitemap,
            'products': ProductSitemap}


def render_robots(request):
    permission = 'noindex' in settings.ROBOTS_META_TAGS and 'Disallow' or 'Allow'
    return HttpResponse(''
                        'User-Agent: *\n'
                        '%s: /\n'
                        'Disallow: /personal-pages/\n'
                        'Disallow: /cart/\n'
                        'Disallow: /register-customer/\n'
                        'Disallow: /request-password-reset/\n'
                        'Disallow: /search/\n'
                        'Sitemap: https://alby.by/sitemap.xml\n'
                        '' % permission, content_type='text/plain')

i18n_urls = (
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^', include('cms.urls')),
)
urlpatterns = [
    re_path(r'^robots\.txt$', render_robots),
    re_path(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='sitemap'),
    re_path(r'^shop/api/', include(router.urls)),
    re_path(r'^shop/', include('shop.urls')),
    re_path(r'^rest-auth/', include('rest_auth.urls')),
    re_path(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    # re_path('cad/', include('dxfgen.urls')),
    re_path('cms_api/', include('cms_api.urls')),
    re_path('cad/', include('connector.urls')),
    # re_path('api/', include('django_cms_headless_test.urls', namespace='headless_api')),
    # re_path('nesting/', include('nesting.urls')),
]

if settings.USE_I18N:
    urlpatterns.extend(i18n_patterns(*i18n_urls, prefix_default_language=is_prefix))
else:
    urlpatterns.extend(i18n_urls)
urlpatterns.extend(
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
