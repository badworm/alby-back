import os
from collections import OrderedDict

from cms.models.titlemodels import Title

from urllib.parse import urlsplit

from django.db import models
from django.http.response import Http404, HttpResponseRedirect, JsonResponse
from django.http.request import QueryDict
from django.shortcuts import get_object_or_404
from django.utils.cache import add_never_cache_headers
from django.utils.encoding import force_str
from django.utils.translation import get_language_from_request

from rest_framework import generics
from rest_framework import pagination
from rest_framework import status
from rest_framework import views
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param

from cms.views import details

from shop.conf import app_settings
from shop.models.product import ProductModel
from shop.rest.filters import CMSPagesFilterBackend
from shop.rest.money import JSONRenderer
from shop.rest.renderers import ShopTemplateHTMLRenderer, CMSPageRenderer
from shop.serializers.bases import ProductSerializer
from shop.serializers.defaults.catalog import AddToCartSerializer


class ProductListPagination(pagination.LimitOffsetPagination):

    default_limit = 16
    overlapping = 1

    def adjust_offset(self, url, page_offset):
        if url is None:
            return
        (scheme, netloc, path, query, fragment) = urlsplit(force_str(url))
        query_dict = QueryDict(query)
        try:
            offset = pagination._positive_int(
                query_dict[self.offset_query_param],
            )
        except (KeyError, ValueError):
            pass
        else:
            if offset > page_offset:
                url = replace_query_param(url, self.offset_query_param, max(0, offset - self.overlapping))
            elif offset < page_offset:
                url = replace_query_param(url, self.offset_query_param, offset + self.overlapping)
        return url

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('total', self.count),
            ('products', data)
        ]))

    # def get_html_context(self):
    #     context = super().get_html_context()
    #     page_offset = self.get_offset(self.request)
    #     context['previous_url'] = self.adjust_offset(context['previous_url'], page_offset)
    #     context['next_url'] = self.adjust_offset(context['next_url'], page_offset)
    #     for k, pl in enumerate(context['page_links']):
    #         url = self.adjust_offset(pl.url, page_offset)
    #         page_link = pagination.PageLink(url=url, number=pl.number, is_active=pl.is_active, is_break=pl.is_break)
    #         context['page_links'][k] = page_link
    #     return context


class ProductListViewApi(generics.ListAPIView):

    # renderer_classes = (CMSPageRenderer, JSONRenderer, BrowsableAPIRenderer)
    product_model = ProductModel
    serializer_class = app_settings.PRODUCT_SUMMARY_SERIALIZER
    limit_choices_to = models.Q()
    filter_class = None
    pagination_class = ProductListPagination
    redirect_to_lonely_product = False

    def get(self, request, *args, **kwargs):

        response = self.list(request, *args, **kwargs)
        # add_never_cache_headers(response)
        language = get_language_from_request(self.request)
        path = request.path.strip('/')
        title = Title.objects.filter(path=path, language=language).first() or 'Category'
        # print(response.data)
        response.data['id'] = title.id
        response.data['name'] = title.title
        response.data['title'] = title.page_title
        return JsonResponse(response.data, safe=False)

    def get_queryset(self):
        qs = self.product_model.objects.filter(self.limit_choices_to, active=True)
        # restrict queryset by language
        if hasattr(self.product_model, 'translations'):
            language = get_language_from_request(self.request)
            qs = qs.prefetch_related('translations').filter(translations__language_code=language)
        qs = qs.select_related('polymorphic_ctype')
        return qs
