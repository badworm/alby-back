import json

from alby.models import ProductList, ParametricModel, CadProduct
from alby.serializers import ParametricProductDxf
from alby.utils import archive_data

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from rest_auth.registration.views import SocialLoginView
from rest_framework.generics import RetrieveAPIView
from shop.views.catalog import AddToCartView
from django.shortcuts import get_object_or_404

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse

from rest_framework.response import Response
from rest_framework.status import HTTP_202_ACCEPTED, HTTP_400_BAD_REQUEST
from rest_framework.serializers import ModelSerializer

from wsgiref.util import FileWrapper


@method_decorator(csrf_exempt, name='dispatch')
class ProductPage(RetrieveAPIView):
    product_model = CadProduct
    lookup_field = 'slug'

    def get_queryset(self, **kwargs):
        field = self.kwargs['slug']
        return self.product_model.objects.get(slug=field)

    def get(self, request, *args, **kwargs):
        prod_detail = self.get_queryset(**kwargs)
        serializer = self.serializer_class(prod_detail)
        return Response(serializer.data, status=HTTP_202_ACCEPTED)


@method_decorator(csrf_exempt, name='dispatch')
class ProductDxf(ProductPage):

    def post(self, request, *args, **kwargs):
        instance = self.get_queryset(**kwargs)

        if dxf_draws := json.loads(instance.dxf_draws):
            arch, arch_name = archive_data(dxf_draws)
            response = HttpResponse(FileWrapper(arch), content_type='application/zip')
            response["Content-Disposition"] = "attachment; filename=a"
            return response
        return Response('', status=HTTP_400_BAD_REQUEST)
