from alby.serializers.cart import ParametricCartSerializer, FilteredCartSerializer
from alby.serializers.cart import ParametricCartItemSerializer, FilteredCartItemSerializer
from alby.models.cart import Cart
from alby.models.cartitem import CartItem

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework.response import Response
from rest_framework.decorators import action

from shop.views.cart import BaseViewSet
from shop.serializers.cart import CartItems


# class CommonCartViewSet(CartViewSet):

@method_decorator(csrf_exempt, name='dispatch')
class CommonCartViewSet(BaseViewSet):

    serializer_label = 'cart'
    serializer_class = FilteredCartSerializer
    item_serializer_class = FilteredCartItemSerializer

    def __init__(self, *args, **kwargs):
        self.cart_type = 'cart'
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        try:
            cart = Cart.objects.get_from_request(self.request)
            if self.kwargs.get(self.lookup_field):
                # we're interest only into a certain cart item
                return CartItem.objects.filter(cart=cart, cart_type=self.cart_type)
            return cart
        except Cart.DoesNotExist:
            return Cart()

    @action(detail=True, methods=['get'])
    def fetch(self, request):
        cart = self.get_queryset()
        context = self.get_serializer_context()
        serializer = self.serializer_class(cart, context=context, with_items=CartItems.without)
        return Response(serializer.data)

    @action(detail=False, methods=['get'], url_path='fetch-dropdown')
    def fetch_dropdown(self, request):
        cart = self.get_queryset()
        context = self.get_serializer_context()
        serializer = self.serializer_class(cart, context=context, label='dropdown', with_items=CartItems.unsorted)
        return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        cart = self.get_queryset()
        context = self.get_serializer_context()
        serializer = self.serializer_class(cart, context=context, label=self.serializer_label,
                                           with_items=self.with_items)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        """
        Handle changing the amount of the cart item referred by its primary key.
        """
        cart_item = self.get_object()
        context = self.get_serializer_context()
        item_serializer = self.item_serializer_class(
            cart_item,
            context=context,
            data=request.data,
            label=self.serializer_label,
        )
        item_serializer.is_valid(raise_exception=True)
        self.perform_update(item_serializer)
        cart_serializer = FilteredCartSerializer(cart_item.cart, context=context, label=self.cart_type)
        response_data = {
            'cart': cart_serializer.data,
            'cart_item': item_serializer.data,
        }
        return Response(data=response_data)


@method_decorator(csrf_exempt, name='dispatch')
class ParametricCartViewSet(CommonCartViewSet):
    serializer_label = 'parametric'
    serializer_class = ParametricCartSerializer
    item_serializer_class = ParametricCartItemSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cart_type = 'parametric'

    def update(self, request, *args, **kwargs):
        cart_item = self.get_object()
        context = self.get_serializer_context()
        item_serializer = self.item_serializer_class(
            cart_item,
            context=context,
            data=request.data,
            label=self.serializer_label,
        )
        item_serializer.is_valid(raise_exception=True)
        self.perform_update(item_serializer)
        response_data = {
            'cart_item': item_serializer.data,
        }
        return Response(data=response_data)

