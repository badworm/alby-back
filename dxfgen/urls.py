from django.urls import path
from .views import part_list, generate_csv_from_cart, download_drafts_zip, GetMaterials

app_name = 'dxfgen'

urlpatterns = [
    path('partlist/', part_list, name='partslist_csv'),
    path('drafts/<int:item_id>', download_drafts_zip, name='drafts_dxf'),
    path('generate-csv/', generate_csv_from_cart),
    path('materials-list/', GetMaterials.as_view())
]
