from alby.models import Product
from alby.models import get_partid_file_by_prod
from alby.models.cart import Cart, CartItem

from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404

from dxfgen.controllers.models import get_or_create_partslist
from dxfgen.models import TaskData, FurniturePartList, RawMaterial
from dxfgen.utils import nest_data_prep, clean_extra_field
from dxfgen.utils import write_parts_csv
from dxfgen.controllers.models import get_partlist
from dxfgen.utils import pack_files
from dxfgen.serializers import FurnitureComponentModelSerializer, RawMaterialSerializer

from rest_framework.decorators import api_view
from rest_framework.generics import RetrieveAPIView, ListAPIView
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.response import Response

from s3sync.backends.connect import s3_io_get_file
from s3sync.backends.storage_backends import PrivateMediaStorage


@method_decorator(csrf_exempt, name='part_list')
@api_view(['GET'])
def part_list(request):
    prod_objects = []
    part_data = []
    # cart_id = Cart.objects.get(customer_id=request.user.id)
    cart_id = Cart.objects.get_from_request(request)
    cart_items = CartItem.objects.filter(cart_id=cart_id, cart_type='parametric')
    if len(cart_items) == 0:
        raise ObjectDoesNotExist
    running_task_status = TaskData.objects.filter(cart_id=cart_id.id)
    running_tasks_id = [i.cartitem_id for i in running_task_status]
    for item in cart_items:
        if item.id not in running_tasks_id:
            prod_objects.append(item)
    for prod in prod_objects:
        part_id = get_partid_file_by_prod(prod.product_id)
        alter_params = clean_extra_field(prod.extra)
        part_obj = get_or_create_partslist(part_id, alter_params)
        part_data.append(part_obj)
    return write_parts_csv(part_data)


@api_view(['GET'])
def download_drafts_zip(request, item_id):
    response = HttpResponse(content_type='application/x-zip-compressed')
    response['Content-Disposition'] = 'attachment; filename="parts-list.zip"'
    # TODO: change request (two simultaneously)
    cart_id = Cart.objects.get(customer_id=request.user.id)
    cart_item = get_object_or_404(CartItem, cart_id=cart_id, id=item_id, cart_type='parametric', )
    part_obj = get_partlist(get_partid_file_by_prod(cart_item.product_id), clean_extra_field(cart_item.extra))
    tmp = list()
    s3instance = PrivateMediaStorage()
    for item in part_obj:
        tmp.append(s3_io_get_file(s3instance, item['file_path']))
    zip_file = pack_files(tmp)
    response.write(zip_file)
    return response


@method_decorator(csrf_exempt, name='dispatch')
class ParametricDataView(RetrieveAPIView):
    product_model = Product
    http_method_names = ['post', 'get']

    def get_context(self, request, **kwargs):
        try:
            data = self.request.data
            code = data.pop('product_id')
            part_id = get_partid_file_by_prod(code)
            alter_params = clean_extra_field(data.pop('alter_params'))
            part_obj = get_partlist(part_id, alter_params)
            return {'product': part_obj['part_files'], 'draft': part_obj['draft_files'], 'request': request}
        except (AttributeError, ValueError, ObjectDoesNotExist) as er:
            raise er

    def post(self, request, *args, **kwargs):
        context = self.get_context(request, **kwargs)
        if 'task_id' in context['product']:
            return JsonResponse(context['product'], safe=False)
        # mod_serializer = self.serializer_class(context['product'], many=True)
        # draft = FurnitureComponentModelSerializer(context['draft'])
        if context['product']:
            return JsonResponse({'product': context['product'], 'draft': context['draft']}, safe=False)
        return Response(context['product'].errors, status=HTTP_400_BAD_REQUEST)


@csrf_exempt
@api_view(['get'])
def generate_csv_from_cart(request):
    res_list = []
    cart_id = Cart.objects.get(customer_id=request.user.id)
    cart_items = CartItem.objects.filter(cart_id=cart_id, cart_type='parametric')
    for item in cart_items:
        if files_list := FurniturePartList.objects.filter(parent_id_id=item.part_id):
            res_list.append(nest_data_prep(files_list, item.quantity))
    return True


class GetMaterials(ListAPIView):
    serializer_class = RawMaterialSerializer
    # product_model = NestingResult
    queryset = RawMaterial.objects.all()
