import os.path
from os import mkdir, path, remove
import json
from django.utils.crypto import get_random_string
from _collections import OrderedDict
import csv
from io import BytesIO
import zipfile
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponse


def ordered_to_dict(ord_dict: OrderedDict) -> list:
    return json.loads(json.dumps(ord_dict))


def clean_extra_field(data):
    if isinstance(data, dict) and 'settings' in data:
        data.pop('settings')
    return data


def lin_to_win_path(path):
    return str(path).replace('/mnt/c/', 'c:\\').replace('/', '\\')


def win_to_lin_path(f_path):
    s = str(f_path).replace('\\', '/')
    s = os.path.join(s, '')
    return s


def win_c_to_lin_mnt(path):
    return str(path).replace('C:\\', '/mnt/c/').replace('\\', '/')


def keystoint(conv):
    k = {}
    for key, val in conv.items():
        try:
            k[key] = int(val)
        except ValueError:
            k[key] = val
    return k


def request_to_json(req_data):
    req_data = json.dumps(req_data)
    return json.loads(req_data, object_hook=keystoint)


def nest_data_prep(files_data, quantity):
    new_dict = OrderedDict()
    list_of_dict = []
    for i in files_data:
        new_dict['name'] = i.name
        new_dict['file_path'] = i.file_path
        new_dict['quantity'] = quantity
        new_dict['thick'] = i.alter_prop[0]
        list_of_dict.append(new_dict.copy())
    return list_of_dict


def pack_files(files: list):
    """
    :param files: files - list of dict in format [{'name: objectname, io: 10_data '} ...]
    :return: In memory zip archive
    """
    in_mem_arch = BytesIO()
    with zipfile.ZipFile(in_mem_arch, 'w', compression=zipfile.ZIP_LZMA) as zf:
        for file in files:
            zf.writestr(file['name'], file['io'])
    return in_mem_arch.getvalue()


def write_parts_csv(cart_data: list):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="parts-list.csv"'
    header_list = [_('Name'), _('Quantity'), _('Thick'), _('Width'), _('Height')]
    response.write("\xEF\xBB\xBF")
    csv_write = csv.DictWriter(response, fieldnames=header_list, delimiter=';')
    csv_write.writeheader()
    for parts in cart_data:
        for part in parts['part_files']:
            qty = part['alter_prop'][3] if len(part['alter_prop']) > 3 else '-'
            d1 = {_('Name'): part['name'], _('Quantity'): qty, _('Thick'): part['alter_prop'][0], _('Width'): part['alter_prop'][1], _('Height'): part['alter_prop'][2]}
            csv_write.writerow({**d1})
    return response


#
# """
# [
# 	[
# 		{
# 			"part_id": 46104728,
# 			"name": "niz",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\18-niz.dxf",
# 			"alter_prop": [
# 				18,
# 				564,
# 				578
# 			],
# 			"area": "0.326"
# 		},
# 		{
# 			"part_id": 12289376,
# 			"name": "dverca",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\18-dverca.dxf",
# 			"alter_prop": [
# 				18,
# 				597,
# 				743
# 			],
# 			"area": "0.4439"
# 		},
# 		{
# 			"part_id": 43495525,
# 			"name": "zadniaa-stenka-DVP",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\4-zadniaa-stenka-DVP.dxf",
# 			"alter_prop": [
# 				4,
# 				600,
# 				745
# 			],
# 			"area": "0.447"
# 		},
# 		{
# 			"part_id": 55915408,
# 			"name": "bok1",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\18-bok1.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.4738"
# 		},
# 		{
# 			"part_id": 33476626,
# 			"name": "bok2",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\18-bok2.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.4738"
# 		},
# 		{
# 			"part_id": 32854180,
# 			"name": "poperechnaia-planka",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\draft\\18-poperechnaia-planka.dxf",
# 			"alter_prop": [
# 				18,
# 				80,
# 				564
# 			],
# 			"area": "0.0451"
# 		}
# 	],
# 	[
# 		{
# 			"part_id": 46104728,
# 			"name": "niz",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\18-niz.dxf",
# 			"alter_prop": [
# 				18,
# 				428,
# 				564
# 			],
# 			"area": "0.2414"
# 		},
# 		{
# 			"part_id": 12289376,
# 			"name": "dverca",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\18-dverca.dxf",
# 			"alter_prop": [
# 				18,
# 				597,
# 				743
# 			],
# 			"area": "0.4439"
# 		},
# 		{
# 			"part_id": 43495525,
# 			"name": "zadniaa-stenka-DVP",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\4-zadniaa-stenka-DVP.dxf",
# 			"alter_prop": [
# 				4,
# 				600,
# 				745
# 			],
# 			"area": "0.447"
# 		},
# 		{
# 			"part_id": 55915408,
# 			"name": "bok1",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\18-bok1.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.3501"
# 		},
# 		{
# 			"part_id": 33476626,
# 			"name": "bok2",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\18-bok2.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.3501"
# 		},
# 		{
# 			"part_id": 32854180,
# 			"name": "poperechnaia-planka",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\119256312\\18-poperechnaia-planka.dxf",
# 			"alter_prop": [
# 				18,
# 				80,
# 				564
# 			],
# 			"area": "0.0451"
# 		}
# 	],
# 	[
# 		{
# 			"part_id": 46104728,
# 			"name": "niz",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\18-niz.dxf",
# 			"alter_prop": [
# 				18,
# 				478,
# 				564
# 			],
# 			"area": "0.2696"
# 		},
# 		{
# 			"part_id": 12289376,
# 			"name": "dverca",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\18-dverca.dxf",
# 			"alter_prop": [
# 				18,
# 				597,
# 				743
# 			],
# 			"area": "0.4439"
# 		},
# 		{
# 			"part_id": 43495525,
# 			"name": "zadniaa-stenka-DVP",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\4-zadniaa-stenka-DVP.dxf",
# 			"alter_prop": [
# 				4,
# 				600,
# 				745
# 			],
# 			"area": "0.447"
# 		},
# 		{
# 			"part_id": 55915408,
# 			"name": "bok1",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\18-bok1.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.3913"
# 		},
# 		{
# 			"part_id": 33476626,
# 			"name": "bok2",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\18-bok2.dxf",
# 			"alter_prop": [
# 				18,
# 				745,
# 				825
# 			],
# 			"area": "0.3913"
# 		},
# 		{
# 			"part_id": 32854180,
# 			"name": "poperechnaia-planka",
# 			"file_path": "C:\\Users\\siadn\\PycharmProjects\\alby.by\\workdir\\media\\files\\67295314\\248657625\\18-poperechnaia-planka.dxf",
# 			"alter_prop": [
# 				18,
# 				80,
# 				564
# 			],
# 			"area": "0.0451"
# 		}
# 	]
# ]
# """
