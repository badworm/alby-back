from rest_framework import serializers
from .models import FurnitureCadFile, FurnitureComponentModel, FurniturePartList, RawMaterial


class FurnitureModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FurnitureCadFile
        fields = '__all__'


def get_id_by_name(name, prods):
    for key, item in prods.items():
        if item['name'] == name:
            return key


class PartListSerializer(serializers.ModelSerializer):
    part_img = serializers.SerializerMethodField()
    # part_draft = serializers.SerializerMethodField()

    class Meta:
        model = FurniturePartList
        fields = ['part_id', 'part_img', 'name', 'file_path', 'alter_prop', 'area']

    def get_part_img(self, product):
        try:
            default_set = product.parent_id.component_file.part_nest_settings
            set_id = get_id_by_name(product.name, default_set)
            if set_id:
                # self.material = default_set[str(product.part_id)]['material']
                # TODO: S3 URL STTIC!!!
                img_url = f"https://alby-files.s3.eu-north-1.amazonaws.com/media/{default_set[set_id]['part_img']}"
                return img_url
            return None
        except (AttributeError, KeyError):
            # self.material = product['material']
            return ''

    # def get_part_draft(self, product):
    #     # print(product)
    #     if parts_draft := product.parent_id.draft_file:
    #         return parts_draft


class FurnitureComponentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FurnitureComponentModel
        fields = ['draft_file']


class RawMaterialSerializer(serializers.ModelSerializer):
    class Meta:
        model = RawMaterial
        fields = '__all__'
