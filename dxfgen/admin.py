from django.contrib import admin
from .models import *
from .forms import FurnModelAdminForm, FurnitureComponentAdminForm, FurniturePartListAdminForm


class ComponentModelInline(admin.TabularInline):
    model = FurnitureComponentModel
    form = FurnitureComponentAdminForm
    readonly_fields = ('files_path', 'draft_file', 'is_created')
    extra = 1

    def has_change_permission(self, request, obj=None):
        return False


class FurnitureModelFileAdmin(admin.ModelAdmin):
    form = FurnModelAdminForm
    exclude = ('variable_limit', )
    fields = (
        ('name', 'is_scalable'),
        ('part_file', 'draft_file'),
        ('part_id', 'exposed_vars'),
        # ('part_nest_settings', 'variables_limit'),
        ('part_nest_settings',),
    )
    readonly_fields = ('part_id', 'exposed_vars')
    inlines = [ComponentModelInline, ]


class FurniturePartListAdmin(admin.ModelAdmin):
    form = FurniturePartListAdminForm
    list_display = ('parent_id', 'name', 'area')
    fields = (
        ('parent_id', 'name', 'part_id'),
        ('area',),
        ('alter_prop',),
        'file_path',)

    def has_change_permission(self, request, obj=None):
        return False


admin.site.register(FurniturePartList, FurniturePartListAdmin)
admin.site.register(FurnitureCadFile, FurnitureModelFileAdmin)


class RawMaterialAdmin(admin.ModelAdmin):
    model = RawMaterial
    list_display = ('name', 'width', 'height', 'thick', 'is_texture')


admin.site.register(RawMaterial, RawMaterialAdmin)
