from django.forms import fields, widgets
from django.template import TemplateDoesNotExist
from django.template.loader import select_template, get_template
from django.utils.translation import ugettext_lazy as _
from django.utils.html import mark_safe
from entangled.forms import EntangledModelFormMixin
from cms.plugin_pool import plugin_pool
from cmsplugin_cascade.plugin_base import TransparentWrapper
from shop.cascade.extensions import ShopExtendableMixin, LeftRightExtensionMixin
from alby.models.cart import Cart
from alby.serializers.cart import ParametricCartSerializer
from cmsplugin_cascade.plugin_base import CascadePluginBase


class ParametricListPluginForm(EntangledModelFormMixin):
    CHOICES = [
        ('editable', _("Editable Cart")),
        ('summary', _("Cart Summary")),
    ]

    render_type = fields.ChoiceField(
        choices=CHOICES,
        widget=widgets.RadioSelect,
        label=_("Render as"),
        initial='editable',
        help_text=_("Shall the cart be editable or a static summary?"),
    )

    class Meta:
        entangled_fields = {'glossary': ['render_type']}


@plugin_pool.register_plugin  # register the plugin
class ParametricListPlugin(LeftRightExtensionMixin, TransparentWrapper, CascadePluginBase):
    name = _("Parametric list")
    require_parent = True
    parent_classes = ['BootstrapColumnPlugin']
    cache = False
    allow_children = True
    form = ParametricListPluginForm
    model_mixins = (ShopExtendableMixin,)

    @classmethod
    def get_identifier(cls, instance):
        render_type = instance.glossary.get('render_type')
        return mark_safe(dict(cls.form.CHOICES).get(render_type, ''))

    def get_render_template(self, context, instance, placeholder):
        render_template = instance.glossary.get('render_template')
        if render_template:
            return get_template(render_template)
        render_type = instance.glossary.get('render_type')
        try:
            return select_template([
                'parametric/list/{}.html'.format(render_type),
            ])
        except TemplateDoesNotExist:
            return get_template('dxfgen/list/editable.html')

    def render(self, context, instance, placeholder):
        try:
            cart = Cart.objects.get_from_request(context['request'])
            context['is_cart_filled'] = cart.items.exists()
            render_type = instance.glossary['render_type']
            if render_type == 'static':
                # update context for static cart with items to be endered as HTML
                cart_serializer = ParametricCartSerializer(cart, context=context, label='parametric', with_items=True)
                context['cart'] = cart_serializer.data
            elif render_type == 'summary':
                # update context for cart summary to be endered as HTML
                cart_serializer = ParametricCartSerializer(cart, context=context, label='parametric')
                context['cart'] = cart_serializer.data
        except (KeyError, Cart.DoesNotExist):
            pass
        return self.super(ParametricListPlugin, self).render(context, instance, placeholder)
