from django import forms
from .models import FurnitureCadFile, FurnitureComponentModel, FurniturePartList
from django_json_widget.widgets import JSONEditorWidget


class FurnModelAdminForm(forms.ModelForm):
    class Meta:
        model = FurnitureCadFile
        fields = ('part_nest_settings',)
        # fields = ('variables_limit', 'part_nest_settings',)
        widgets = {
            'part_nest_settings': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'})
        }


class FurniturePartListAdminForm(forms.ModelForm):
    class Meta:
        model = FurniturePartList
        fields = ('alter_prop',)
        # fields = ('variables_limit', 'part_nest_settings',)
        widgets = {
            'alter_prop': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'}),
            # 'variables_limit': JSONEditorWidget(width='100%', height='300px'),
        }


class FurnitureComponentAdminForm(forms.ModelForm):
    class Meta:
        model = FurnitureComponentModel
        fields = ('component_params',)
        widgets = {
            'component_params': JSONEditorWidget(width='100%', height='300px'),
        }
