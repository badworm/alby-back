from alby.tasks import generate_files_celery

from collections import OrderedDict

from dxfgen.models import FurnitureCadFile
from dxfgen.serializers import PartListSerializer
from django.core.exceptions import ObjectDoesNotExist


def get_component_by_dims(part_id: int, part_dims: dict):
    # return Furniturecomponentmodel object
    return FurnitureCadFile.objects.get(part_id=part_id).furniturecomponentmodel_set.get(
        component_params__contains=part_dims)


def get_partlist(part_id, part_data):
    try:
        obj_data = FurnitureCadFile.objects.get(part_id=part_id)
        part_obj = obj_data.furniturecomponentmodel_set.filter(
            component_params__contains=part_data)
        part_files = part_obj.first().furniturepartlist_set.all()
        mod_serializer = PartListSerializer(part_files, many=True)
        draft_file = part_obj.first().draft_file.url if part_obj.first().draft_file else ''
        return {'part_files': mod_serializer.data, 'draft_files': draft_file}
    except (ObjectDoesNotExist, AttributeError, ValueError):
        return False


def get_or_create_partslist(part_id, part_data):
    if part_files := get_partlist(part_id, part_data):
        return part_files
    else:
        task_id = generate_files_celery(part_id, part_data)
        return {'task_id': task_id}


def get_dimensions_by_dict(data):
    if type(data) is dict:
        return {'depth': data['depth'], 'width': data['width'], 'height': data['height']}
