from django.apps import AppConfig


class DxfgenConfig(AppConfig):
    name = 'dxfgen'

    def ready(self):
        import dxfgen.signals  # noqa
