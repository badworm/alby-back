from builtins import Exception


class EdgeError(Exception):

    def __init__(self, *args, **kwargs):
        pass

    @staticmethod
    def __new__(cls, *args, **kwargs):
        pass
