from alby.models.cart import CartItem
from alby.tasks import get_exposed_params_celery, generate_files_celery
from django.db.models import signals
from django.dispatch import receiver
from dxfgen.models import TaskData
from dxfgen.views import get_partid_file_by_prod, get_partlist
from .models import FurnitureCadFile
from .utils import clean_extra_field


@receiver(signals.post_save, sender=FurnitureCadFile)
def save_exposed(instance, **kwargs):
    if not instance.exposed_vars and instance.part_file:
        get_exposed_params_celery(instance.part_id)


@receiver(signals.post_save, sender=CartItem)
def save_task(instance, **kwargs):
    if instance.cart_type == 'parametric':
        data = clean_extra_field(instance.extra)
        part_id = get_partid_file_by_prod(instance.product_id)
        get_cart_item_instance = TaskData.objects.filter(cart_id=instance.cart_id, cartitem_id=instance.id)
        if len(get_cart_item_instance) >= 1:
            return
        if not get_partlist(part_id, data):
            task_instance = TaskData()
            task_instance.task_id = 'null'
            task_instance.cart_id = instance.cart_id
            task_instance.cartitem_id = instance.id
            task_instance.task_status = 'pending'
            task_instance.save()
            task_instance.task_id = generate_files_celery(part_id, instance.extra, instance.id)
            task_instance.save()


