import os.path
from django.db import models
from django.utils.crypto import get_random_string
from django.core.validators import FileExtensionValidator
from s3sync.backends.connect import s3_delete_folder
from django.utils.translation import ugettext_lazy as _
from s3sync.backends.storage_backends import PrivateMediaStorage


def get_upload_to(instance, filename):
    return '%s/%s' % (instance.part_id, filename)


class FurnitureCadFile(models.Model):
    name = models.CharField(max_length=100)
    part_id = models.IntegerField(unique=True, blank=True)
    part_file = models.FileField(upload_to=get_upload_to, verbose_name='Part File', validators=[FileExtensionValidator(
        {'par', 'psm', 'dft', 'asm'})], storage=PrivateMediaStorage())
    draft_file = models.FileField(upload_to=get_upload_to, verbose_name='Draft File', blank=True, null=True,
                                  validators=[FileExtensionValidator({'dft', })], storage=PrivateMediaStorage())
    is_scalable = models.BooleanField(verbose_name='Размер по масштабу', default=False, blank=True)
    exposed_vars = models.CharField(verbose_name='Внешние переменные', blank=True, max_length=350)
    variables_limit = models.JSONField(verbose_name='Лимиты размеров', blank=True, null=True)
    part_nest_settings = models.JSONField(verbose_name='Настройки нестинга', blank=True, max_length=600, null=True)

    class Meta:
        verbose_name = 'Файлы компонентов'
        verbose_name_plural = 'Файлы компонентов'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.part_id:
            self.part_id = get_random_string(8, '123456789')
        return super(FurnitureCadFile, self).save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        try:
            folder_path = os.path.basename(self.part_file.name)
            s3instance = PrivateMediaStorage()
            s3_delete_folder(s3instance, folder_path)
            if self.draft_file:
                draft_path = os.path.basename(self.draft_file.name)
                s3instance = PrivateMediaStorage()
                s3_delete_folder(s3instance, draft_path)
            return super(FurnitureCadFile, self).delete()
        except FileNotFoundError:
            return super(FurnitureCadFile, self).delete()


class FurnitureComponentModel(models.Model):
    component_file = models.ForeignKey('FurnitureCadFile', on_delete=models.PROTECT, verbose_name='Компоненты')
    files_path = models.CharField(verbose_name='Путь к файлам модели', max_length=250)
    draft_file = models.FileField(
        blank=True,
        null=True,
        verbose_name='Общий чертеж',
        storage=PrivateMediaStorage())
    component_params = models.JSONField(verbose_name='Размеры модели', blank=True, null=True, max_length=200)
    is_created = models.BooleanField(default=False)
    creating_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.component_file.name

    def delete(self, using=None, keep_parents=False):
        try:
            s3instance = PrivateMediaStorage()
            s3_delete_folder(s3instance, self.files_path)
            return super(FurnitureComponentModel, self).delete()
        except FileNotFoundError:
            raise Exception('Cant delete folder')


class FurniturePartList(models.Model):
    name = models.CharField(max_length=100)
    part_id = models.IntegerField(verbose_name='Хэш код детали')
    alter_prop = models.JSONField(verbose_name='Part\'s altering data', blank=True, null=True)
    file_path = models.FileField(
        blank=True,
        null=True,
        verbose_name='Полный путь к файлу',
        storage=PrivateMediaStorage())
    area = models.CharField(verbose_name='Площать поверхности', max_length=20, blank=True)
    parent_id = models.ForeignKey('FurnitureComponentModel', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Общий список деталей'

    def __str__(self):
        return self.name


class TaskData(models.Model):
    cart_id = models.PositiveIntegerField(blank=True)
    cartitem_id = models.PositiveIntegerField(blank=True)
    task_id = models.CharField(max_length=50, blank=True)
    task_status = models.CharField(max_length=10, blank=True)
    extra = models.JSONField(verbose_name='Extra task\'s data', blank=True, null=True)

    def __str__(self):
        return f'Task id: {self.task_id}'


class RawMaterial(models.Model):
    name = models.CharField(max_length=20)
    width = models.PositiveSmallIntegerField(verbose_name='Material width')
    height = models.PositiveSmallIntegerField(verbose_name='Material height')
    thick = models.PositiveSmallIntegerField(verbose_name='Material thick')
    is_texture = models.BooleanField(default=False)

    @property
    def volume_m3(self):
        return (self.width * self.height * self.thick) / 1000000

    @property
    def area_m2(self):
        return (self.width * self.height) / 1000000

    class Meta:
        verbose_name = _('Raw\'s material list')

    def __str__(self):
        return self.name
