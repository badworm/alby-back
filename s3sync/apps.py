from django.apps import AppConfig


class S3SyncConfig(AppConfig):
    name = 's3sync'
