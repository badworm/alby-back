import os
from django.utils.crypto import get_random_string
from io import BytesIO


def get_base_path(filepath: str, isdraft=True) -> str:
    path_only = os.path.split(filepath)[0]
    if isdraft:
        path_only = f'{path_only}\\draft'
    return path_only


def s3_delete_folder(s3instance, folder_path: str):
    path_list = folder_path.replace('\\', '/').split('/')
    if len(path_list) > 1 and path_list[0] == 'files':
        path_list = '/'.join(path_list[1:])
        folder_files = s3instance.listdir(path_list)
        for f_file in folder_files[1]:
            f_file = os.path.join(path_list, '', f_file)
            s3instance.delete(f_file)
        s3instance.delete(os.path.join(path_list))
        return True


def s3_upload_files(s3instance, file_path: str, files: list):
    base_remote_path = get_base_path(file_path)
    for file in files:
        with open(file, 'rb') as f:
            base_filename = os.path.basename(f.name)
            remote_path = os.path.join(base_remote_path, base_filename)
            s3instance.save(remote_path, f)
    return True


def s3_io_get_file(s3instance, rem_filepath: str) -> dict:
    try:
        filename = os.path.basename(rem_filepath)
        with s3instance.open(rem_filepath, 'rb') as remote_file:
            return {'name': filename, 'io': remote_file.read()}
    except (FileNotFoundError, ConnectionError) as e:
        raise Exception(e)
