from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage
from tempfile import NamedTemporaryFile
# class StaticStorage(S3Boto3Storage):
#     location = settings.AWS_STATIC_LOCATION


class PublicMediaStorage(S3Boto3Storage):
    location = settings.AWS_PUBLIC_MEDIA_LOCATION
    default_acl = 'public-read'
    file_overwrite = False


class PrivateMediaStorage(S3Boto3Storage):
    location = settings.AWS_PRIVATE_MEDIA_LOCATION
    bucket_name = 'alby-files'
    private_media = 'media'
    default_acl = 'private'
    file_overwrite = False
    custom_domain = False
    access_key_names = settings.AWS_ACCESS_KEY_ID
    secret_key_names = settings.AWS_SECRET_ACCESS_KEY


class UsersPrivateStorage(PrivateMediaStorage):
    default_acl = 'public-read'
    location = 'users-data'
