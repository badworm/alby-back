from rest_framework.serializers import ModelSerializer
from cms.models.titlemodels import Title


class MenuSerializer(ModelSerializer):
    class Meta:
        model = Title
        fields = '__all__'
