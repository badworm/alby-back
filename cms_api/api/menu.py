import cms.cms_menus
from rest_framework.views import APIView
from cms.models.titlemodels import Title
from cms_api.serializers.menu import MenuSerializer
from rest_framework.response import Response
from menus.menu_pool import MenuPool, MenuRenderer
from django.test import RequestFactory
from cms.cms_menus import CMSMenu, get_visible_nodes, get_menu_node_for_page
from django.contrib.auth.models import AnonymousUser
from cms.models.pagemodel import Page


class MenuApi(APIView):

    def get(self, request):
        menu = Title.objects.filter(published=True, publisher_is_draft=True)
        serializer = MenuSerializer(menu, many=True)
        return Response(serializer.data)


def gen_data_by_node(data):
    return {'text': data.title, 'href': data.path}


def get_children_items(parent_id, nodes):
    items = []
    for node in nodes:
        if node.parent_id == parent_id:
            items.append(gen_data_by_node(node))
    return items if items else None


class MApi(APIView):

    def get(self, request):
        menu_pool = MenuPool()
        rend = menu_pool.get_renderer(request)
        a = CMSMenu(rend)
        # pages = Page.objects.filter(in_navigation=True, publisher_is_draft=False, languages='ru')
        # b = get_visible_nodes(request, pages, 'ru')
        res = a.get_nodes(request)
        id_nodes = menu_pool.get_nodes_by_attribute(res, "reverse_id", 1)
        # menu = Title.objects.filter(published=True, publisher_is_draft=True)
        # serializer = MenuSerializer(menu, many=True)
        # return Response(serializer.data)
        menu = {'items': []}
        for node in res:
            if node.attr['is_home']:
                menu['home'] = gen_data_by_node(node)
                continue
            if not node.parent_id:
                parent = gen_data_by_node(node)
                child = get_children_items(node.id, res)
                if child:
                    parent['items'] = child
                    parent['expanded'] = 'true'
                menu['items'].append(parent)
        return Response(menu)
