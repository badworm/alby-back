from django.urls import path
from cms_api.api.menu import MenuApi, MApi
from shop.views.catalog import AddToCartView, ProductRetrieveView, ProductListView

app_name = 'cms_api'

urlpatterns = [
    # path('menu/', MenuApi.as_view(), name='menu'),
    path('menu/', MApi.as_view(), name='menu'),
    # path('xxx/', ProductListView.as_view()),
]
