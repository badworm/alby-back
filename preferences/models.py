from django.db import models
from django.utils.translation import gettext_lazy as _
from alby.settings import AUTH_USER_MODEL as USER_PREF


class Preferences(models.Model):
    user = models.OneToOneField(related_name='user_pref', to=USER_PREF, on_delete=models.CASCADE)
    material = models.JSONField(verbose_name=_('Material\'s preferences'), blank=True, null=True)
    cad = models.JSONField(verbose_name=_('Cad\'s preferences'), blank=True, null=True)
    product = models.JSONField(verbose_name=_('Product\'s preferences'), blank=True, null=True)
    extra = models.JSONField(verbose_name=_('Extra\'s preferences'), blank=True, null=True)

    def __str__(self):
        return f'{self.user.name}'
