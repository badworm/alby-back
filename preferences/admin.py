from django.contrib import admin
from .models import Preferences
from .forms import PreferencesAdminForm


class PreferencesAdmin(admin.ModelAdmin):
    form = PreferencesAdminForm


admin.site.register(Preferences, PreferencesAdmin)
