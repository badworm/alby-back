from django import forms
from .models import Preferences
from django_json_widget.widgets import JSONEditorWidget


class PreferencesAdminForm(forms.ModelForm):
    class Meta:
        model = Preferences
        fields = ('material', 'cad', 'product', 'extra')
        widgets = {
            'material': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'}),
            'cad': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'}),
            'product': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'}),
            'extra': JSONEditorWidget(width='100%', height='300px', options={'mode': 'form'})
        }
